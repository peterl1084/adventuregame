package fi.peterlehto.model.game.event;

import fi.peterlehto.eventbus.EventbusEvent;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;

public class PickupItemsEvent implements EventbusEvent {
	private final GameCharacter character;
	private final Class<? extends InventoryItem> itemType;

	public PickupItemsEvent(GameCharacter character,
			Class<? extends InventoryItem> itemType) {
		this.character = character;
		this.itemType = itemType;
	}

	public GameCharacter getCharacter() {
		return character;
	}

	public Class<? extends InventoryItem> getItemType() {
		return itemType;
	}
}
