package fi.peterlehto.model.game.world;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.Vector;

public enum MoveDirection {
	NORTH(0, -1),
	NORTH_EAST(1, -1),
	EAST(1, 0),
	SOUTH_EAST(1, 1),
	SOUTH(0, 1),
	SOUTH_WEST(-1, 1),
	WEST(-1, 0),
	NORT_WEST(-1, -1);

	private final int x;
	private final int y;
	private final float angle;

	private MoveDirection(int x, int y) {
		this.x = x;
		this.y = y;
		angle = new Vector(x, y).getDegreeAngle();
	}

	public Point apply(Point point) {
		return Point.valueOf(point.getX() + x, point.getY() + y);
	}

	public float getDirection() {
		return angle;
	}
}
