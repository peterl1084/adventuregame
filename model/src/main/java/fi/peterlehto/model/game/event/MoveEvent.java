package fi.peterlehto.model.game.event;

import fi.peterlehto.eventbus.EventbusEvent;
import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.character.GameCharacter;

public class MoveEvent implements EventbusEvent {
	private final GameCharacter character;
	private final MoveDirection direction;

	public MoveEvent(GameCharacter character, MoveDirection direction) {
		this.character = character;
		this.direction = direction;
	}

	public GameCharacter getCharacter() {
		return character;
	}

	public MoveDirection getDirection() {
		return direction;
	}
}
