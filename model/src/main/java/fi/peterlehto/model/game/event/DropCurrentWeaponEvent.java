package fi.peterlehto.model.game.event;

import fi.peterlehto.eventbus.EventbusEvent;
import fi.peterlehto.model.gameitem.character.GameCharacter;

public class DropCurrentWeaponEvent implements EventbusEvent {
	private final GameCharacter character;

	public DropCurrentWeaponEvent(GameCharacter character) {
		this.character = character;
	}

	public GameCharacter getCharacter() {
		return character;
	}
}
