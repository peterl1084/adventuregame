package fi.peterlehto.model.game.world;

import java.util.Optional;
import java.util.Set;

import model.util.ModelUtils;
import fi.peterlehto.common.point.Point;
import fi.peterlehto.eventbus.EventBusListener;
import fi.peterlehto.model.game.Player;
import fi.peterlehto.model.game.event.DropCurrentWeaponEvent;
import fi.peterlehto.model.game.event.DropItemEvent;
import fi.peterlehto.model.game.event.MoveEvent;
import fi.peterlehto.model.game.event.PickupItemsEvent;
import fi.peterlehto.model.gameitem.GameItem;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.model.gameitem.tile.Wall;
import fi.peterlehto.model.map.GameItemMap;

public class GameWorld {
	private GameItemMap itemMap;
	private Player currentPlayerInTurn;

	public GameWorld() {
		this(new GameItemMap());
	}

	public GameWorld(GameItemMap itemMap) {
		ModelUtils.notNullOrThrow(itemMap);
		this.itemMap = itemMap;

	}

	public void addGameItem(GameItem item, Point point) {
		itemMap.put(item, point);
	}

	/**
	 * Picks up all the items from the location of given character adding them
	 * to character's inventory and removes them from the map.
	 * 
	 * @param character
	 */
	@EventBusListener
	public void onPickUpItems(PickupItemsEvent event) {
		onPickUpItems(event.getCharacter(), event.getItemType());
	}

	/**
	 * Picks up items of defined type from the location of given character
	 * adding them to character's inventory and removes them from the map. As
	 * GameItems are hierarchical depending on which level of hierarchy is given
	 * as parameter more than one kinds of items may be picked up (like weapons
	 * AND food).
	 * 
	 * @param character
	 * @param typeToPickUp
	 */
	protected void onPickUpItems(GameCharacter character,
			Class<? extends InventoryItem> typeToPickUp) {
		ModelUtils.notNullOrThrow(character);

		Optional<Point> characterLocation = itemMap.whereIs(character);

		if (characterLocation.isPresent())
			character.pickupItems(itemMap.removeAll(itemMap.getItemsAt(
					characterLocation.get(), typeToPickUp)));
	}

	@EventBusListener
	public void onDropItem(DropItemEvent event) {
		onDropItem(event.getCharacter(), event.getItem());
	}

	@EventBusListener
	public void onDropCurrentWeapon(DropCurrentWeaponEvent event) {
		GameCharacter character = event.getCharacter();

		if (character.hasCurrentWeapon()) {
			onDropItem(character, character.currentWeapon().get());
		}
	}

	/**
	 * Drops given item from given character's inventory to the current position
	 * of character.
	 * 
	 * @param character
	 * @param item
	 */
	protected void onDropItem(GameCharacter character, InventoryItem item) {
		ModelUtils.notNullOrThrow(character, item);

		Optional<Point> characterLocation = itemMap.whereIs(character);

		if (characterLocation.isPresent())
			if (character.dropItem(item))
				itemMap.put(item, characterLocation.get());
	}

	@EventBusListener
	public void onMove(MoveEvent event) {
		onMove(event.getCharacter(), event.getDirection());
	}

	/**
	 * Moves given character by one to given direction. Given character must be
	 * on the map to be able to move.
	 * 
	 * @param character
	 * @param direction
	 * @return true if character moved, false if it did not.
	 */
	protected boolean onMove(GameCharacter character, MoveDirection direction) {
		ModelUtils.notNullOrThrow(character, direction);
		Optional<Point> characterLocation = itemMap.whereIs(character);

		if (characterLocation.isPresent()) {
			Point targetPoint = direction.apply(characterLocation.get());
			character.face(direction);
			if (isFreeToMove(targetPoint)) {
				return itemMap.move(character, targetPoint);
			}

		}
		return false;
	}

	/**
	 * Given character attacks from it's current position to given direction.
	 * 
	 * @param character
	 * @param direction
	 */
	public void onAttack(GameCharacter character, MoveDirection direction) {
		ModelUtils.notNullOrThrow(character, direction);
		Optional<Point> characterLocation = itemMap.whereIs(character);

		if (characterLocation.isPresent()) {
			Point targetPoint = direction.apply(characterLocation.get());

			Optional<GameCharacter> possibleTarget = getCharacterAt(targetPoint);
			if (possibleTarget.isPresent()) {
				doAttack(character, possibleTarget.get());
			}
		}
	}

	private void doAttack(GameCharacter attacker, GameCharacter attackee) {

	}

	private boolean isFreeToMove(Point point) {
		return itemMap.getItemsAt(point, GameCharacter.class).isEmpty()
				&& itemMap.getItemsAt(point, Wall.class).isEmpty();
	}

	private Optional<GameCharacter> getCharacterAt(Point point) {
		Set<GameCharacter> itemsAt = itemMap.getItemsAt(point,
				GameCharacter.class);
		if (itemsAt.size() == 1) {
			return Optional.of(itemsAt.iterator().next());
		}

		return Optional.empty();
	}
}
