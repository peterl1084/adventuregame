package fi.peterlehto.model.game;

import java.math.BigDecimal;
import java.math.RoundingMode;

import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.value.CharacterAttribute;
import fi.peterlehto.model.gameitem.value.CharacterStat;
import fi.peterlehto.model.gameitem.value.InventoryItemAttribute;

public class Calculator {

	/**
	 * For half of the max strength returns 1. For max strength returns 2.
	 * Always returns at least 0.5.
	 * 
	 * @param attacker
	 * @return BigDecimal describing the strength bonus.
	 */
	public BigDecimal calculateStrengthBonus(GameCharacter attacker) {
		BigDecimal currentStrength = BigDecimal.valueOf(attacker.attributes()
				.getValue(CharacterAttribute.STRENGTH));
		BigDecimal halfOfMaxStrength = BigDecimal.valueOf(
				CharacterAttribute.STRENGTH.maxValue()).divide(
				BigDecimal.valueOf(2));
		if (currentStrength.compareTo(halfOfMaxStrength) < 0) {
			BigDecimal percentageOfCurrentToHalfOfMax = currentStrength
					.divide(halfOfMaxStrength);
			BigDecimal multiply = percentageOfCurrentToHalfOfMax
					.multiply(BigDecimal.valueOf(0.5));
			return multiply.add(BigDecimal.valueOf(0.5));
		} else {
			return currentStrength.divide(halfOfMaxStrength);
		}
	}

	public BigDecimal calculateTotalDamage(GameCharacter attacker) {
		BigDecimal damage = BigDecimal
				.valueOf(attacker.hasCurrentWeapon() ? attacker.currentWeapon()
						.get().getAttribute(InventoryItemAttribute.DAMAGE) : 0);

		BigDecimal durability = attacker.hasCurrentWeapon() ? attacker
				.currentWeapon().get()
				.getPercentualAttribute(InventoryItemAttribute.DURABILITY)
				: BigDecimal.ZERO;

		return damage.multiply(calculateStrengthBonus(attacker))
				.multiply(durability).setScale(2, RoundingMode.CEILING);
	}

	/**
	 * Calculates the current attack endurance between 0 to 1 (0 - 100%) based
	 * on currentWeapon's max damage endurance and attackers current endurance.
	 * If attacker has higher current endurance than what is the max damage
	 * endurance of attacker's current weapon 1 is returned. Otherwise a number
	 * between 0 and 1 is returned describing the current percentual of
	 * available endurance.
	 * 
	 * @param attacker
	 * @return BigDecimal value between 0 to 1.
	 */
	public BigDecimal calculateEndurance(GameCharacter attacker) {
		BigDecimal endurance = BigDecimal.valueOf(attacker.statistics()
				.getValue(CharacterStat.ENDURANCE));

		BigDecimal maxDamageEndurance = BigDecimal.valueOf(attacker
				.hasCurrentWeapon() ? attacker.currentWeapon().get()
				.getAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE) : 0);

		if (endurance.compareTo(maxDamageEndurance) < 0) {
			return endurance.divide(maxDamageEndurance).setScale(2,
					RoundingMode.CEILING);
		}

		return BigDecimal.ONE;
	}
}
