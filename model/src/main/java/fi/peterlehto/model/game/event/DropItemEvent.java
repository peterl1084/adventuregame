package fi.peterlehto.model.game.event;

import fi.peterlehto.eventbus.EventbusEvent;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;

public class DropItemEvent implements EventbusEvent {
	private final GameCharacter character;
	private final InventoryItem item;

	public DropItemEvent(GameCharacter character, InventoryItem itemToDrop) {
		this.character = character;
		item = itemToDrop;
	}

	public GameCharacter getCharacter() {
		return character;
	}

	public InventoryItem getItem() {
		return item;
	}
}
