package fi.peterlehto.model.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * PlayerTurnManager will take care of players of the game and determining who's
 * turn it is.
 */
public class PlayerTurnManager {
	private int currentPlayerInTurn;
	private List<Player> players;

	public PlayerTurnManager() {
		this(new HumanPlayer(), new AIPlayer());
	}

	public PlayerTurnManager(Player... players) {
		this.players = new ArrayList<>();
		this.players.addAll(Arrays.asList(players));
	}

	/**
	 * Called when turn of current player is node. Next player's turn will
	 * begin.
	 */
	public void onTurnDone() {
		getNextPlayer().beginTurn();
		currentPlayerInTurn += 1;

		if (currentPlayerInTurn > players.size() - 1) {
			currentPlayerInTurn = 0;
		}
	}

	/**
	 * @return Player that is currently having turn
	 */
	public Player getPlayerInTurn() {
		return players.get(currentPlayerInTurn);
	}

	/**
	 * Calling this method will not mutate the state of PlayerTurnManager and
	 * getPlayerInTurn will still return same current player as before this
	 * call.
	 * 
	 * @return Player who will have next turn.
	 */
	public Player getNextPlayer() {
		int nextPlayerIndex = currentPlayerInTurn + 1;
		if (nextPlayerIndex > players.size() - 1)
			nextPlayerIndex = 0;
		return players.get(nextPlayerIndex);
	}
}
