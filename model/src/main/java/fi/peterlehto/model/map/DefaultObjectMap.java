package fi.peterlehto.model.map;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import model.util.ModelUtils;
import fi.peterlehto.common.point.Point;

public class DefaultObjectMap<T> implements ObjectMap<T> {
	private static final int MIN_WIDTH = 10;
	private static final int MIN_HEIGHT = 10;

	private static final int DEFAULT_WIDTH = 100;
	private static final int DEFAULT_HEIGHT = 100;

	protected final Map<Point, Set<T>> itemPositions;
	protected final Map<T, Point> gameItems;

	private final int width;
	private final int height;

	public DefaultObjectMap() {
		this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	public DefaultObjectMap(int width, int height) {
		assertDimensionsOrThrow(width, height);

		this.width = width;
		this.height = height;

		itemPositions = new HashMap<>();
		gameItems = new HashMap<>();
	}

	@Override
	public boolean put(T item, Point point) {
		ModelUtils.notNullOrThrow(item, point);

		// No duplicates
		if (this.contains(item))
			return false;

		if (isValid(point)) {
			if (!itemPositions.containsKey(point)) {
				itemPositions.put(point, new HashSet<>());
			}

			itemPositions.get(point).add(item);
			gameItems.put(item, point);

			return true;
		}

		return false;
	}

	@Override
	public Set<T> getItemsAt(Point point) {
		ModelUtils.notNullOrThrow(point);

		if (!itemPositions.containsKey(point))
			return Collections.emptySet();

		return Collections.unmodifiableSet(itemPositions.get(point));
	}

	@Override
	public boolean move(T item, Point point) {
		ModelUtils.notNullOrThrow(item, point);

		if (isValid(point)) {
			if (!this.contains(item))
				return false;

			if (remove(item))
				return put(item, point);

		}

		return false;
	}

	@Override
	public boolean remove(Object item) {
		ModelUtils.notNullOrThrow(item);

		if (!gameItems.containsKey(item))
			return false;

		Point itemLocation = gameItems.remove(item);
		Set<T> itemsAtLocation = itemPositions.get(itemLocation);

		boolean wasRemoved = itemsAtLocation.remove(item);

		// Remove empty set
		if (itemsAtLocation.isEmpty())
			itemPositions.remove(itemLocation);

		return wasRemoved;
	}

	@Override
	public <U> Set<U> removeAll(Set<U> items) {
		ModelUtils.notNullOrThrow(items);

		Iterator<U> iterator = items.iterator();
		while (iterator.hasNext()) {
			if (!remove(iterator.next())) {
				iterator.remove();
			}
		}

		return Collections.unmodifiableSet(items);
	}

	public Set<? extends T> removeAll(T... items) {
		ModelUtils.notNullOrThrow(items);
		return removeAll(new HashSet<>(Arrays.asList(items)));
	}

	@Override
	public Set<T> removeItemsAt(Point point) {
		ModelUtils.notNullOrThrow(point);

		if (!itemPositions.containsKey(point))
			return Collections.emptySet();

		Set<T> set = itemPositions.remove(point);
		set.forEach(i -> gameItems.remove(i));

		Set<T> returnSet = new HashSet<>(set);
		set.clear();

		return returnSet;
	}

	@Override
	public Optional<Point> whereIs(T item) {
		ModelUtils.notNullOrThrow(item);

		if (this.contains(item))
			return Optional.of(gameItems.get(item));

		return Optional.empty();
	}

	@Override
	public boolean contains(T item) {
		ModelUtils.notNullOrThrow(item);
		return gameItems.containsKey(item);
	}

	@Override
	public int width() {
		return width;
	}

	@Override
	public int height() {
		return height;
	}

	@Override
	public Point bottomRightPoint() {
		return Point.valueOf(width - 1, height - 1);
	}

	private void assertDimensionsOrThrow(int width, int height) {
		if (width < MIN_WIDTH)
			throw new IllegalArgumentException("Map width must be at least "
					+ MIN_WIDTH);

		if (height < MIN_HEIGHT)
			throw new IllegalArgumentException("Map height must be at least "
					+ MIN_HEIGHT);
	}

	/**
	 * @param point
	 * @return true if given point remains inside the bounds of this map, false
	 *         if point is invalid and outside of map bounds.
	 * @throws IllegalArgumentException
	 *             if point is null
	 */
	private boolean isValid(Point point) {
		ModelUtils.notNullOrThrow(point);
		return point.getX() >= 0 && point.getX() < width & point.getY() >= 0
				&& point.getY() <= height;
	}

	@Override
	public Iterator<T> iterator() {
		return Collections.unmodifiableSet(gameItems.keySet()).iterator();
	}
}
