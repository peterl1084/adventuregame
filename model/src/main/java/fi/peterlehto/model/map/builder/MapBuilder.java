package fi.peterlehto.model.map.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import model.util.ModelUtils;
import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.gameitem.GameItem;
import fi.peterlehto.model.map.GameItemMap;

public class MapBuilder {
	private Integer width;
	private Integer height;

	private List<Fill> fills;

	public MapBuilder() {
		fills = new ArrayList<>();
	}

	public GameItemMap build() {
		getLogger().fine("Starting to build map");
		if (width == null || height == null)
			throw new IllegalStateException(
					"Width and height must be set before building map");

		GameItemMap map = new GameItemMap(width, height);
		fills.forEach(f -> f.populate(map));
		map.setWallTypes();
		return map;
	}

	public MapBuilder width(int width) {
		getLogger().fine("Setting map width " + width);
		this.width = width;
		return this;
	}

	public MapBuilder height(int height) {
		getLogger().fine("Setting map height " + width);
		this.height = height;
		return this;
	}

	public MapBuilder fill(Point topLeft, Point bottomRight,
			Class<? extends GameItem> itemType) {
		ModelUtils.notNullOrThrow(topLeft, bottomRight, itemType);
		fills.add(new Fill(topLeft, bottomRight, itemType));
		return this;
	}

	public MapBuilder clear(Point topLeft, Point bottomRight) {
		ModelUtils.notNullOrThrow(topLeft, bottomRight);
		fills.add(new Fill(topLeft, bottomRight, null));
		return this;
	}

	private static class Fill {
		private final Point topLeft;
		private final Point bottomRight;
		private final Class<? extends GameItem> itemType;

		public Fill(Point topLeft, Point bottomRight,
				Class<? extends GameItem> itemType) {
			this.topLeft = topLeft;
			this.bottomRight = bottomRight;
			this.itemType = itemType;
		}

		public Point getTopLeft() {
			return topLeft;
		}

		public Point getBottomRight() {
			return bottomRight;
		}

		public GameItem getItem() {
			try {
				return itemType.newInstance();
			} catch (Exception e) {
				throw new RuntimeException("Failed to instantiate "
						+ itemType.getSimpleName(), e);
			}
		}

		public boolean isClear() {
			return itemType == null;
		}

		protected void populate(GameItemMap map) {
			int fillWidth = Math.min(map.width(),
					Math.max(1, getBottomRight().getX() - getTopLeft().getX()));
			int fillHeight = Math.min(map.height(),
					Math.max(1, getBottomRight().getY() - getTopLeft().getY()));

			int fillXStart = getTopLeft().getX();
			int fillYStart = getTopLeft().getY();

			for (int i = fillXStart; i <= (fillXStart + fillWidth); i++) {
				for (int j = fillYStart; j <= (fillYStart + fillHeight); j++) {
					if (isClear()) {
						map.removeItemsAt(Point.valueOf(i, j));
					} else {
						Point point = Point.valueOf(i, j);
						System.out.println("Putting " + getItem() + " to "
								+ point);
						map.put(getItem(), point);
					}
				}
			}
		}
	}

	private Logger getLogger() {
		return Logger.getLogger(MapBuilder.class.getSimpleName());
	}
}
