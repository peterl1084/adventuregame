package fi.peterlehto.model.map;

import java.util.Optional;
import java.util.Set;

import fi.peterlehto.common.point.Point;

/**
 * ObjectMap allows storing objects to a map like structure where each added
 * object is targeted to a Point. ObjectMap does not allow same item to be added
 * more than once.
 * 
 * <T> type of items used in Map.
 */
public interface ObjectMap<T> extends Iterable<T> {

	/**
	 * Adds given item to given point in this map.
	 * 
	 * @param item
	 * @param point
	 * @return true if given item did not exist in the map and was added
	 *         successfully. False if item was not added.
	 * @throws IllegalArgumentException
	 *             if item is null
	 * @throws IllegalArgumentException
	 *             if point is null
	 */
	boolean put(T item, Point point);

	/**
	 * Moves given item to new position in this map.
	 * 
	 * @param item
	 * @param point
	 * @return true if item was successfully moved, false if map did not contain
	 *         item or if it was not moved.
	 * @throws IllegalArgumentException
	 *             if item is null
	 * @throws IllegalArgumentException
	 *             if point is null
	 */
	boolean move(T item, Point point);

	/**
	 * Removes given item from this map.
	 * 
	 * @param item
	 * @return true if item was removed, false otherwise. False will occur if
	 *         map does not contain given item. otherwise.
	 * @throws IllegalArgumentException
	 *             if item is null
	 */
	boolean remove(Object item);

	/**
	 * Removes all given items from the map. If one of the given items does not
	 * exist in the map it's not removed.
	 * 
	 * @param items
	 * @return Set containing the items which were removed. The size of the set
	 *         is always <= items.size() depending on if the given items
	 *         actually existed in the map.
	 */
	<U> Set<U> removeAll(Set<U> items);

	Set<? extends T> removeAll(T... items);

	/**
	 * Removes all items positioned at given point.
	 * 
	 * @param point
	 * @return Set of items which were at given point before and which were now
	 *         removed. Returned set will be empty if there were no items at
	 *         given point.
	 * @throws IllegalArgumentException
	 *             if point is null
	 */
	Set<T> removeItemsAt(Point point);

	/**
	 * @param point
	 * @return set of items at given point. If there are no items in the given
	 *         point empty set is returned.
	 */
	Set<T> getItemsAt(Point point);

	/**
	 * @param item
	 * @return Optional of Point where given item is. Optional will be empty if
	 *         item is not on the map.
	 * @throws IllegalArgumentException
	 *             if point is null
	 */
	Optional<Point> whereIs(T item);

	/**
	 * @param item
	 * @return true if this map contains given item, false otherwise.
	 * @throws IllegalArgumentException
	 *             if point is null
	 */
	boolean contains(T item);

	/**
	 * @return width of the map as given when created
	 */
	int width();

	/**
	 * @return height of the map as given when created
	 */
	int height();

	/**
	 * @return Point that is at the bottom right corner of the map.
	 */
	Point bottomRightPoint();
}
