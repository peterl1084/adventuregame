package fi.peterlehto.model.map;

import java.util.Set;
import java.util.stream.Collectors;

import model.util.ModelUtils;
import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.GameItem;
import fi.peterlehto.model.gameitem.tile.Wall;
import fi.peterlehto.model.gameitem.tile.Wall.WallType;

public class GameItemMap extends DefaultObjectMap<GameItem> {

	public GameItemMap() {
		super();
	}

	public GameItemMap(int width, int height) {
		super(width, height);
	}

	@SuppressWarnings("unchecked")
	public <T extends GameItem> Set<T> getItemsAt(Point point,
			Class<T> gameItemType) {
		ModelUtils.notNullOrThrow(point, gameItemType);

		Set<GameItem> items = super.getItemsAt(point);

		return (Set<T>) items.stream()
				.filter(i -> gameItemType.isAssignableFrom(i.getClass()))
				.collect(Collectors.toSet());
	}

	public void setWallTypes() {
		for (GameItem item : this) {
			if (item instanceof Wall) {
				updateWallType((Wall) item);
			}
		}
	}

	protected void updateWallType(Wall wall) {
		Point point = whereIs(wall).get();
		WallType type = WallType.HORIZONTAL;

		// North <-> South wall == VERTICAL
		if (hasAdjacentWall(point, MoveDirection.NORTH)
				|| hasAdjacentWall(point, MoveDirection.SOUTH)) {
			type = WallType.VERTICAL;
		}

		// West <-> East wall == Horizontal
		if (hasAdjacentWall(point, MoveDirection.WEST)
				|| hasAdjacentWall(point, MoveDirection.EAST)) {
			type = WallType.HORIZONTAL;
		}

		// West, North == BOTTOM_RIGHT
		if (hasAdjacentWall(point, MoveDirection.WEST)
				&& hasAdjacentWall(point, MoveDirection.NORTH)) {
			type = WallType.BOTTOM_RIGHT;
		}

		// North, East == BOTTOM_LEFT
		if (hasAdjacentWall(point, MoveDirection.NORTH)
				&& hasAdjacentWall(point, MoveDirection.EAST)) {
			type = WallType.BOTTOM_LEFT;
		}

		// West, South == TOP_RIGHT
		if (hasAdjacentWall(point, MoveDirection.WEST)
				&& hasAdjacentWall(point, MoveDirection.SOUTH)) {
			type = WallType.TOP_RIGHT;
		}

		// East, South == TOP_LEFT
		if (hasAdjacentWall(point, MoveDirection.EAST)
				&& hasAdjacentWall(point, MoveDirection.SOUTH)) {
			type = WallType.TOP_LEFT;
		}

		if (hasAdjacentWall(point, MoveDirection.WEST)
				&& hasAdjacentWall(point, MoveDirection.EAST)
				&& hasAdjacentWall(point, MoveDirection.SOUTH)) {
			type = WallType.INTERSECTION_TOP_DOWN;
		}

		if (hasAdjacentWall(point, MoveDirection.WEST)
				&& hasAdjacentWall(point, MoveDirection.EAST)
				&& hasAdjacentWall(point, MoveDirection.NORTH)) {
			type = WallType.INTERSECTION_BOTTOM_UP;
		}

		if (hasAdjacentWall(point, MoveDirection.NORTH)
				&& hasAdjacentWall(point, MoveDirection.SOUTH)
				&& hasAdjacentWall(point, MoveDirection.EAST)) {
			type = WallType.INTERSECTION_LEFT_RIGHT;
		}

		if (hasAdjacentWall(point, MoveDirection.NORTH)
				&& hasAdjacentWall(point, MoveDirection.SOUTH)
				&& hasAdjacentWall(point, MoveDirection.WEST)) {
			type = WallType.INTERSECTION_RIGHT_LEFT;
		}

		wall.setType(type);
	}

	private boolean hasAdjacentWall(Point point, MoveDirection direction) {
		return !getItemsAt(direction.apply(point), Wall.class).isEmpty();
	}
}
