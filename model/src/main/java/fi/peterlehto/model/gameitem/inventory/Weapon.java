package fi.peterlehto.model.gameitem.inventory;

public class Weapon extends InventoryItem {

	public Weapon(String name) {
		super(name);
	}

	@Override
	public String itemId() {
		return "gun1";
	}
}
