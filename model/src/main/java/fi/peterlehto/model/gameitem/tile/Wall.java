package fi.peterlehto.model.gameitem.tile;

import fi.peterlehto.model.gameitem.AbstractGameItem;
import model.util.ModelUtils;

public class Wall extends AbstractGameItem {

	public enum WallType {
		HORIZONTAL("wall_horizontal"),
		VERTICAL("wall_vertical"),
		TOP_LEFT("wall_top_left"),
		TOP_RIGHT("wall_top_right"),
		BOTTOM_LEFT("wall_bottom_left"),
		BOTTOM_RIGHT("wall_bottom_right"),
		INTERSECTION_LEFT_RIGHT("wall_left_intersection_right"),
		INTERSECTION_RIGHT_LEFT("wall_right_intersection_left"),
		INTERSECTION_TOP_DOWN("wall_top_intersection_down"),
		INTERSECTION_BOTTOM_UP("wall_bottom_intersection_up");

		private final String id;

		private WallType(String id) {
			this.id = id;
		}

		public String getId() {
			return id;
		}
	}

	private WallType type;

	public Wall() {
		this.type = WallType.HORIZONTAL;
	}

	public Wall(WallType wallType) {
		setType(wallType);
	}

	public WallType getType() {
		return type;
	}

	public void setType(WallType type) {
		ModelUtils.notNullOrThrow(type);
		this.type = type;
	}

	@Override
	public String itemId() {
		return getType().getId();
	}
}
