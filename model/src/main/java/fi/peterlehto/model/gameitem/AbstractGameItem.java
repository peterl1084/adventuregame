package fi.peterlehto.model.gameitem;

/**
 * Abstract base class for all GameItems that implements equality checks and
 * hashcoding.
 */
public abstract class AbstractGameItem implements GameItem {
	private static long indexCounter;
	private Long id;

	public AbstractGameItem() {
		this.id = ++indexCounter;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj instanceof AbstractGameItem) {
			return this.id.equals(((AbstractGameItem) obj).id);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
