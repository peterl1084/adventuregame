package fi.peterlehto.model.gameitem.value;

import java.util.EnumMap;
import java.util.Map;

/**
 * ValueContainer manages one particular Enum<VALUE>
 *
 * @param <VALUE>
 *            type of Value as Enum
 */
public class ValueContainer<VALUE extends Enum<VALUE> & Value> {
	private Map<VALUE, Integer> statistics;

	public ValueContainer(Class<VALUE> valueType) {
		statistics = new EnumMap<VALUE, Integer>(valueType);

		for (VALUE value : valueType.getEnumConstants()) {
			setValue(value, value.defaultValue());
		}
	}

	/**
	 * @param value
	 * @return current numberic value of given VALUE type
	 */
	public int getValue(VALUE value) {
		return statistics.get(value);
	}

	/**
	 * Sets the current numeric value of given VALUE type
	 * 
	 * @param value
	 * @param number
	 */
	public void setValue(VALUE value, int number) {
		statistics.put(value,
				Math.max(Math.min(number, value.maxValue()), value.minValue()));
	}
}
