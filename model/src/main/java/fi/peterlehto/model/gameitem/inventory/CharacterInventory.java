package fi.peterlehto.model.gameitem.inventory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import fi.peterlehto.model.gameitem.character.GameCharacter.Inventory;
import model.util.ModelUtils;

public class CharacterInventory implements Inventory {
	private Set<InventoryItem> items;

	public CharacterInventory() {
		items = new HashSet<>();
	}

	@Override
	public boolean add(InventoryItem item) {
		ModelUtils.notNullOrThrow(item);
		System.out.println("Adding " + item + " to inventory");
		return items.add(item);
	}

	@Override
	public boolean remove(InventoryItem item) {
		ModelUtils.notNullOrThrow(item);
		return items.remove(item);
	}

	@Override
	public int removeAll(Set<InventoryItem> items) {
		ModelUtils.notNullOrThrow(items);
		int removedItems = 0;

		for (InventoryItem item : items) {
			removedItems += remove(item) ? 1 : 0;
		}

		return removedItems;
	}

	@Override
	public int removeAll(InventoryItem... items) {
		return removeAll(new HashSet<>(Arrays.asList(items)));
	}

	@Override
	@SuppressWarnings("unchecked")
	public <I extends InventoryItem> Set<I> getItemsOfType(Class<I> type) {
		ModelUtils.notNullOrThrow(type);
		return (Set<I>) items.stream()
				.filter(i -> type.isAssignableFrom(i.getClass()))
				.collect(Collectors.toSet());
	}

	@Override
	public int size() {
		return items.size();
	}

	@Override
	public boolean hasItem(InventoryItem item) {
		if (item == null)
			return false;

		return items.contains(item);
	}

	@Override
	public int addAll(Set<? extends InventoryItem> items) {
		ModelUtils.notNullOrThrow(items);
		int addedItems = 0;

		for (InventoryItem item : items) {
			addedItems += add(item) ? 1 : 0;
		}

		return addedItems;
	}

	@Override
	public int addAll(InventoryItem... items) {
		return addAll(new HashSet<>(Arrays.asList(items)));
	}
}
