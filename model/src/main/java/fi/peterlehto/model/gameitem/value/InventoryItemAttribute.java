package fi.peterlehto.model.gameitem.value;

public enum InventoryItemAttribute implements Value {
	/**
	 * Weight of the item in gramms.
	 */
	WEIGHT(1, 100000, 0),

	/**
	 * Sales price of the item.
	 */
	PRICE(0, 10000, 0),

	/**
	 * Durability of the item. Item is considered broken when durability is
	 * zero.
	 */
	DURABILITY(0, 100, 100),

	/**
	 * Maximum damage that can be produced
	 */
	DAMAGE(0, 1000, 0),

	/**
	 * Amount of endurance required to produce theoretical maximum damage
	 */
	MAX_DAMAGE_ENDURANCE(0, 100, 0);

	private final int minValue;
	private final int maxValue;
	private final int defaultValue;

	private InventoryItemAttribute(int minValue, int maxValue, int defaultValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
	}

	@Override
	public int defaultValue() {
		return defaultValue;
	}

	@Override
	public int maxValue() {
		return maxValue;
	}

	@Override
	public int minValue() {
		return minValue;
	}
}
