package fi.peterlehto.model.gameitem.character;

import java.util.Optional;
import java.util.Set;

import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.GameItem;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.model.gameitem.inventory.Weapon;
import fi.peterlehto.model.gameitem.value.CharacterAttribute;
import fi.peterlehto.model.gameitem.value.CharacterStat;
import fi.peterlehto.model.gameitem.value.ValueContainer;

public interface GameCharacter extends GameItem {

	/**
	 * Inventory is place where character holds his items.
	 */
	public interface Inventory {
		/**
		 * Adds given item to inventory
		 * 
		 * @param item
		 * @return true if item was added, false otherwise
		 */
		boolean add(InventoryItem item);

		/**
		 * Adds given set of items into the inventory. If some of the items in
		 * given set was already in the inventory it will not be added.
		 * 
		 * @param items
		 * @return number of items added to inventory. Number is always <=
		 *         items.size()
		 */
		int addAll(Set<? extends InventoryItem> items);

		int addAll(InventoryItem... items);

		/**
		 * Removes given item from inventory
		 * 
		 * @param item
		 * @return true if item was removed, false otherwise. False is caused by
		 *         item not being in the inventory to start with.
		 */
		boolean remove(InventoryItem item);

		/**
		 * @param type
		 * @return Set of inventory items which are of given type
		 */
		<I extends InventoryItem> Set<I> getItemsOfType(Class<I> type);

		/**
		 * @return number of items in this characters' inventory
		 */
		int size();

		/**
		 * @param item
		 * @return true if this inventory contains given item, false if not or
		 *         if null.
		 */
		boolean hasItem(InventoryItem item);

		/**
		 * Removes given set of items from this inventory. If some of the items
		 * in given set was not in the inventory it doesn't affect the removal
		 * of other items.
		 * 
		 * @param itemsAt
		 * @return number of items removed. Number is always <= items.size()
		 */
		int removeAll(Set<InventoryItem> items);

		int removeAll(InventoryItem... items);

	}

	ValueContainer<CharacterStat> statistics();

	ValueContainer<CharacterAttribute> attributes();

	/**
	 * @return Optional of Weapon used as current weapon or empty optional if no
	 *         current weapon is selected
	 */
	Optional<Weapon> currentWeapon();

	/**
	 * Sets given weapon as current weapon. Will remove the currently set
	 * current weapon if null is passed. The weapon passed to this method must
	 * exist in the inventory of this character, otherwise an exception will be
	 * thrown.
	 * 
	 * @param weapon
	 * @throws IllegalStateException
	 *             if given weapon does not appear in the inventory of this
	 *             character
	 */
	void setCurrentWeapon(Weapon weapon);

	/**
	 * @return true if this character has current weapon selected, false if no
	 *         weapon is selected.
	 */
	boolean hasCurrentWeapon();

	boolean pickupItem(InventoryItem item);

	int pickupItems(Set<? extends InventoryItem> items);

	boolean dropItem(InventoryItem item);

	/**
	 * Turns characters facing to given direction
	 * 
	 * @param direction
	 */
	void face(MoveDirection direction);

	MoveDirection getFacing();
}
