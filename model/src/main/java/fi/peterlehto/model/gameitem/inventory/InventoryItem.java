package fi.peterlehto.model.gameitem.inventory;

import java.math.BigDecimal;
import java.math.RoundingMode;

import fi.peterlehto.model.gameitem.AbstractGameItem;
import fi.peterlehto.model.gameitem.value.InventoryItemAttribute;
import fi.peterlehto.model.gameitem.value.ValueContainer;

public abstract class InventoryItem extends AbstractGameItem {
	private ValueContainer<InventoryItemAttribute> attributes;

	private final String name;

	public InventoryItem(String name) {
		attributes = new ValueContainer<>(InventoryItemAttribute.class);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getAttribute(InventoryItemAttribute attribute) {
		return attributes.getValue(attribute);
	}

	public BigDecimal getPercentualAttribute(InventoryItemAttribute attribute) {
		BigDecimal currentValue = BigDecimal.valueOf(attributes
				.getValue(attribute));
		BigDecimal maxValue = BigDecimal.valueOf(attribute.maxValue());

		return currentValue.divide(maxValue).setScale(2, RoundingMode.CEILING);
	}

	public void setAttribute(InventoryItemAttribute attribute, int value) {
		attributes.setValue(attribute, value);
	}
}
