package fi.peterlehto.model.gameitem.value;

/**
 * Attributes are Values which change unfrequently
 */
public enum CharacterAttribute implements Value {
	STRENGTH(1, 10, 1),
	WEIGHT(0, 1000, 50);

	private final int minValue;
	private final int maxValue;
	private final int defaultValue;

	private CharacterAttribute(int minValue, int maxValue, int defaultValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
	}

	@Override
	public int defaultValue() {
		return defaultValue;
	}

	@Override
	public int maxValue() {
		return maxValue;
	}

	@Override
	public int minValue() {
		return minValue;
	}
}
