package fi.peterlehto.model.gameitem.value;

/**
 * Statistics are Values which change often, usually in every cycle.
 */
public enum CharacterStat implements Value {
	VITALITY(0, 1000, 100),
	ENDURANCE(0, 1000, 1000),
	HUNGER(0, 100, 0),
	THIRST(0, 100, 0),
	BLADDER(0, 100, 0);

	private final int minValue;
	private final int maxValue;
	private final int defaultValue;

	private CharacterStat(int minValue, int maxValue, int defaultValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
	}

	@Override
	public int defaultValue() {
		return defaultValue;
	}

	@Override
	public int maxValue() {
		return maxValue;
	}

	@Override
	public int minValue() {
		return minValue;
	}
}
