package fi.peterlehto.model.gameitem.value;

/**
 * Value defines limits and default for numerical enum value.
 */
public interface Value {

	int defaultValue();

	int maxValue();

	int minValue();
}
