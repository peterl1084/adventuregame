package fi.peterlehto.model.gameitem.character;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import model.util.ModelUtils;
import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.AbstractGameItem;
import fi.peterlehto.model.gameitem.inventory.CharacterInventory;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.model.gameitem.inventory.Weapon;
import fi.peterlehto.model.gameitem.value.CharacterAttribute;
import fi.peterlehto.model.gameitem.value.CharacterStat;
import fi.peterlehto.model.gameitem.value.ValueContainer;

public class Character extends AbstractGameItem implements GameCharacter {
	private ValueContainer<CharacterStat> statistics;
	private ValueContainer<CharacterAttribute> attributes;

	private Inventory inventory;
	private Weapon currentWeapon;
	private MoveDirection facing;

	public Character() {
		this(new CharacterInventory());
	}

	public Character(Inventory inventory) {
		ModelUtils.notNullOrThrow(inventory);
		statistics = new ValueContainer<CharacterStat>(CharacterStat.class);
		attributes = new ValueContainer<CharacterAttribute>(
				CharacterAttribute.class);

		this.inventory = inventory;
		facing = MoveDirection.EAST;
	}

	public Inventory inventory() {
		return inventory;
	}

	@Override
	public Optional<Weapon> currentWeapon() {
		return Optional.ofNullable(currentWeapon);
	}

	@Override
	public void setCurrentWeapon(Weapon weapon) {
		if (weapon == null) {
			System.out.println("Removing currently set weapon from " + this);
			this.currentWeapon = null;
		} else {
			if (inventory().hasItem(weapon)) {
				System.out.println("Setting " + weapon
						+ " as current weapon of " + this);
				this.currentWeapon = weapon;
			} else {
				throw new IllegalStateException(
						"Could not set "
								+ weapon
								+ " as "
								+ this
								+ "'s current weapon as it does not appear in the inventory");
			}
		}
	}

	@Override
	public boolean hasCurrentWeapon() {
		return currentWeapon().isPresent();
	}

	@Override
	public ValueContainer<CharacterStat> statistics() {
		return statistics;
	}

	@Override
	public ValueContainer<CharacterAttribute> attributes() {
		return attributes;
	}

	@Override
	public String itemId() {
		return "character_body";
	}

	@Override
	public void face(MoveDirection direction) {
		ModelUtils.notNullOrThrow(direction);
		this.facing = direction;
	}

	@Override
	public MoveDirection getFacing() {
		return facing;
	}

	@Override
	public boolean pickupItem(InventoryItem item) {
		ModelUtils.notNullOrThrow(item);
		boolean pickedUp = inventory().add(item);
		if (pickedUp)
			if (item instanceof Weapon) {
				if (!hasCurrentWeapon()) {
					setCurrentWeapon((Weapon) item);
				}
			}

		return pickedUp;
	}

	@Override
	public int pickupItems(Set<? extends InventoryItem> items) {
		ModelUtils.notNullOrThrow(items);
		int pickedUp = inventory().addAll(items);
		Set<? extends InventoryItem> weapons = items.stream()
				.filter(i -> Weapon.class.isAssignableFrom(i.getClass()))
				.collect(Collectors.toSet());

		if (!weapons.isEmpty()) {
			if (!hasCurrentWeapon()) {
				setCurrentWeapon((Weapon) weapons.iterator().next());
			}
		}

		return pickedUp;
	}

	@Override
	public boolean dropItem(InventoryItem item) {
		ModelUtils.notNullOrThrow(item);
		if (inventory().remove(item)) {
			if (item.equals(currentWeapon)) {
				setCurrentWeapon(null);
			}

			return true;
		}

		return false;
	}
}
