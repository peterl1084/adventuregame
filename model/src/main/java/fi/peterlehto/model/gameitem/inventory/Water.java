package fi.peterlehto.model.gameitem.inventory;

public class Water extends InventoryItem {

	public Water() {
		super("Water");
	}

	@Override
	public String itemId() {
		return "drop";
	}
}
