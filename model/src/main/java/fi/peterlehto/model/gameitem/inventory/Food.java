package fi.peterlehto.model.gameitem.inventory;

public class Food extends InventoryItem {

	public Food(String name) {
		super(name);
	}

	@Override
	public String itemId() {
		return "food";
	}
}
