package model.util;

public class ModelUtils {

	/**
	 * Tests that none of the given values is null
	 * 
	 * @param value
	 * @throws IllegalArgumentException
	 *             if null is passed, if nothing is passed or if any one of the
	 *             items passed is null.
	 */
	public static void notNullOrThrow(Object... values)
			throws IllegalArgumentException {
		if (values == null) {
			throw new IllegalArgumentException(
					"Null passed where array expected");
		}

		if (values != null && values.length == 0) {
			throw new IllegalArgumentException("No arguments passed");
		}

		for (Object value : values) {
			if (value == null) {
				throw new IllegalArgumentException(
						"One of the passed arguements was null");
			}
		}
	}
}
