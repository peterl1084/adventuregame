package fi.peterlehto.model.gameitem.character;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.character.GameCharacter.Inventory;
import fi.peterlehto.model.gameitem.inventory.CharacterInventory;
import fi.peterlehto.model.gameitem.inventory.Weapon;

public class CharacterTests {

	@Test
	public void testSetCurrentWeapon_CurrentWeaponSet_HasCurrentWeapon() {
		GameCharacter character = new Character();
		Weapon weapon = new Weapon("Sword");

		Assert.assertFalse(
				"Character should not have a current weapon as it's not set yet",
				character.hasCurrentWeapon());

		Assert.assertEquals(Optional.empty(), character.currentWeapon());

		// Pickup sets current weapon
		character.pickupItem(weapon);

		Assert.assertTrue(character.hasCurrentWeapon());
		Assert.assertEquals(weapon, character.currentWeapon().get());

		// Remove current weapon
		character.setCurrentWeapon(null);

		Assert.assertFalse(
				"Character should not have a current weapon as it's not set yet",
				character.hasCurrentWeapon());

		Assert.assertEquals(Optional.empty(), character.currentWeapon());
	}

	@Test
	public void testPickUpWeapon_NoCurrentWeaponSet_ItemGetsSetAsCurrentWeaponAndAppearsInInventory() {
		Inventory inventory = new CharacterInventory();
		GameCharacter character = new Character(inventory);

		Weapon weapon = new Weapon("Sword");
		Assert.assertFalse(character.hasCurrentWeapon());
		Assert.assertEquals(0, inventory.size());

		character.pickupItem(weapon);

		Assert.assertTrue(character.hasCurrentWeapon());
		Assert.assertEquals(weapon, character.currentWeapon().get());
		Assert.assertEquals(weapon, inventory.getItemsOfType(Weapon.class)
				.iterator().next());
		Assert.assertEquals(1, inventory.size());
	}

	@Test
	public void testDropWeapon_WeaponIsCurrentWeapon_CurrentWeaponNotSetItemRemovedFromInventory() {
		Inventory inventory = new CharacterInventory();
		GameCharacter character = new Character(inventory);
		Weapon weapon = new Weapon("Sword");

		character.pickupItem(weapon);
		Assert.assertTrue(character.dropItem(character.currentWeapon().get()));
		Assert.assertEquals(0, inventory.size());
		Assert.assertFalse(character.hasCurrentWeapon());
		Assert.assertEquals(Optional.empty(), character.currentWeapon());
	}

	@Test(expected = IllegalStateException.class)
	public void testSetCurrentWeapon_WeaponDoesNotAppearInInventory_ExceptionThrown() {
		GameCharacter character = new Character();
		Weapon weapon = new Weapon("Sword");
		character.setCurrentWeapon(weapon);
		Assert.fail("Exception should've been thrown as weapon was not in character's inventory");
	}

	@Test
	public void testRemovingCurrentWeaponButNotDroppingIt_ItemRemainsInInventory_CurrentWeaponNotSet() {
		Inventory inventory = new CharacterInventory();
		GameCharacter character = new Character(inventory);
		Weapon weapon = new Weapon("Sword");
		character.pickupItem(weapon);

		Assert.assertTrue(character.hasCurrentWeapon());
		Assert.assertEquals(weapon, character.currentWeapon().get());
		Assert.assertEquals(weapon, inventory.getItemsOfType(Weapon.class)
				.iterator().next());

		character.setCurrentWeapon(null);

		Assert.assertFalse("Character doesn't have current weapon anymore",
				character.hasCurrentWeapon());
		Assert.assertEquals("Current weapon should no longer be set",
				Optional.empty(), character.currentWeapon());
		Assert.assertEquals("Weapon should still appear in inventory", weapon,
				inventory.getItemsOfType(Weapon.class).iterator().next());
	}

	@Test
	public void testPickUpWeapon_CurrentWeaponAlreadySet_CurrentWeaponDoesNotChange() {
		Inventory inventory = new CharacterInventory();
		GameCharacter character = new Character(inventory);
		Weapon weaponA = new Weapon("Sword");
		character.pickupItem(weaponA);

		Assert.assertTrue(character.hasCurrentWeapon());
		Assert.assertEquals(weaponA, character.currentWeapon().get());

		Weapon weaponB = new Weapon("Gun");

		character.pickupItem(weaponB);

		Assert.assertTrue(character.hasCurrentWeapon());
		Assert.assertEquals(
				"WeaponA should still be the selected weapon as selected weapon should not chagen when picking up another weapon",
				weaponA, character.currentWeapon().get());
	}

	@Test
	public void testPickingUpSameItemTwice_SecondAttemptIgnored() {
		Inventory inventory = new CharacterInventory();
		GameCharacter character = new Character(inventory);
		Weapon weaponA = new Weapon("Sword");
		Assert.assertTrue(character.pickupItem(weaponA));

		Assert.assertEquals(1, inventory.size());
		Assert.assertEquals(weaponA, inventory.getItemsOfType(Weapon.class)
				.iterator().next());

		Assert.assertFalse(
				"Picking up same item twice should fail on second attempt",
				character.pickupItem(weaponA));
		Assert.assertEquals(
				"Only one item should exist in inventory as same item was picked up twice",
				1, inventory.size());
		Assert.assertEquals(weaponA, inventory.getItemsOfType(Weapon.class)
				.iterator().next());
	}
}
