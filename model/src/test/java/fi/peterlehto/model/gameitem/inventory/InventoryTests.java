package fi.peterlehto.model.gameitem.inventory;

import junit.framework.Assert;

import org.junit.Test;

import fi.peterlehto.model.gameitem.character.GameCharacter.Inventory;
import fi.peterlehto.model.gameitem.inventory.CharacterInventory;
import fi.peterlehto.model.gameitem.inventory.Food;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.model.gameitem.inventory.Weapon;

public class InventoryTests {

	@Test
	public void testAddItemsToCharactersInventory_ItemsInInventory_NoProblems() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		Assert.assertTrue(inventory.add(new Food("Pear")));
		Assert.assertTrue(inventory.add(new Food("Apple")));

		Assert.assertEquals("There should be two items in the inventory", 2,
				inventory.size());
	}

	@Test
	public void testAddItemsToCharactersInventory_SameItemAttemptedToBeAddedTwice_ItemAddedOnlyOnce() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		InventoryItem thePear = new Food("Pear");

		Assert.assertTrue(inventory.add(thePear));
		Assert.assertFalse(inventory.add(thePear));

		Assert.assertEquals("There should be only one item in the inventory",
				1, inventory.size());
	}

	@Test
	public void testAddAllItemsToCharactersInventory_DifferentItems_AllAddedOnce() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		InventoryItem thePear = new Food("Pear");

		InventoryItem theApple = new Food("Apple");

		Assert.assertEquals("Two items should've been added to the inventory",
				2, inventory.addAll(thePear, theApple));

		Assert.assertEquals("Inventory should have two items", 2,
				inventory.size());
	}

	@Test
	public void testAddAllItemsToCharactersInventory_SameItemTwice_AllAddedOnce() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		InventoryItem thePear = new Food("Pear");

		InventoryItem theApple = new Food("Apple");

		Assert.assertEquals("Two items should've been added to the inventory",
				2, inventory.addAll(thePear, theApple, theApple));

		Assert.assertEquals("Inventory should have two items", 2,
				inventory.size());
	}

	@Test
	public void testRemoveItemFromInventory_ItemWasInInventory_NoProblems() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		InventoryItem thePear = new Food("Pear");

		InventoryItem theApple = new Food("Apple");

		Assert.assertEquals("Two items should've been added to the inventory",
				2, inventory.addAll(thePear, theApple));

		Assert.assertTrue("Pear should've been removed",
				inventory.remove(thePear));
		Assert.assertFalse("Pear should've already be removed",
				inventory.remove(thePear));

		Assert.assertEquals("Only one item should remain in inventory", 1,
				inventory.size());

		Assert.assertTrue("Apple should be in inventory",
				inventory.hasItem(theApple));
		Assert.assertFalse("Pear should not be in inventory",
				inventory.hasItem(thePear));
	}

	@Test
	public void testRemoveAllFromInventory_GivenItemsWereInInventory_NoProblems() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		InventoryItem thePear = new Food("Pear");

		InventoryItem theApple = new Food("Apple");

		InventoryItem theOrange = new Food("Orange");

		Assert.assertEquals(
				"Three items should've been added to the inventory", 3,
				inventory.addAll(thePear, theApple, theOrange));

		Assert.assertEquals("Two items should've been removed", 2,
				inventory.removeAll(thePear, theApple));
		Assert.assertEquals("One item should remain in inventory", 1,
				inventory.size());
	}

	@Test
	public void testRemoveAllFromInventory_NotAllGivenItemsWereInInventory_OnlyCorrectItemsRemoved() {
		Inventory inventory = new CharacterInventory();
		Assert.assertTrue(inventory.size() == 0);

		InventoryItem thePear = new Food("Pear");

		InventoryItem theApple = new Food("Apple");

		InventoryItem theOrange = new Food("Orange");

		InventoryItem sword = new Weapon("Sword");

		Assert.assertEquals(
				"Three items should've been added to the inventory", 3,
				inventory.addAll(thePear, theApple, theOrange));

		Assert.assertEquals("Two items should've been removed", 2,
				inventory.removeAll(thePear, theApple, sword));
		Assert.assertEquals("One item should remain in inventory", 1,
				inventory.size());
		Assert.assertFalse("Inventory should not have the pear",
				inventory.hasItem(thePear));
		Assert.assertFalse("Inventory should not have the apple",
				inventory.hasItem(theApple));
		Assert.assertTrue("Inventory should still have the orange",
				inventory.hasItem(theOrange));
		Assert.assertFalse("Inventory should not have the sword",
				inventory.hasItem(sword));
	}
}
