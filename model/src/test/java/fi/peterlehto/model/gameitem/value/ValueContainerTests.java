package fi.peterlehto.model.gameitem.value;

import junit.framework.Assert;

import org.junit.Test;

import fi.peterlehto.model.gameitem.value.CharacterAttribute;
import fi.peterlehto.model.gameitem.value.ValueContainer;

public class ValueContainerTests {

	@Test
	public void testValueContainerInitialized_DefaultValuesSet() {
		ValueContainer<CharacterAttribute> attributeContainer = new ValueContainer<>(
				CharacterAttribute.class);

		for (CharacterAttribute attribute : CharacterAttribute.values()) {
			Assert.assertEquals(attribute.defaultValue(),
					attributeContainer.getValue(attribute));
		}
	}

	@Test
	public void testValueContainerSettingValues_ValuesInRange_ValuesCorrectlySet() {
		ValueContainer<CharacterAttribute> attributeContainer = new ValueContainer<>(
				CharacterAttribute.class);

		for (CharacterAttribute attribute : CharacterAttribute.values()) {
			// Assert default value start
			Assert.assertEquals(attribute.defaultValue(),
					attributeContainer.getValue(attribute));

			// Decrement max value by one
			attributeContainer.setValue(attribute, attribute.maxValue() - 1);

			// Assert given value set
			Assert.assertEquals(attribute.maxValue() - 1,
					attributeContainer.getValue(attribute));
		}
	}

	@Test
	public void testValueContainerSettingValues_ValuesAboveAllowedRange_MaxValuesSet() {
		ValueContainer<CharacterAttribute> attributeContainer = new ValueContainer<>(
				CharacterAttribute.class);

		for (CharacterAttribute attribute : CharacterAttribute.values()) {
			// Assert default value start
			Assert.assertEquals(attribute.defaultValue(),
					attributeContainer.getValue(attribute));

			// Increment 10 above max.
			attributeContainer.setValue(attribute, attribute.maxValue() + 10);

			// Assert max value set
			Assert.assertEquals(attribute.maxValue(),
					attributeContainer.getValue(attribute));
		}
	}

	@Test
	public void testValueContainerSettingValues_ValuesBelowAllowedRange_MinValuesSet() {
		ValueContainer<CharacterAttribute> attributeContainer = new ValueContainer<>(
				CharacterAttribute.class);

		for (CharacterAttribute attribute : CharacterAttribute.values()) {
			// Assert default value start
			Assert.assertEquals(attribute.defaultValue(),
					attributeContainer.getValue(attribute));

			// Decrement 10 below min.
			attributeContainer.setValue(attribute, attribute.minValue() - 10);

			// Assert min value set
			Assert.assertEquals(attribute.minValue(),
					attributeContainer.getValue(attribute));
		}
	}
}
