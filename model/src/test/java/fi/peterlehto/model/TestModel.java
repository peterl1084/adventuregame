package fi.peterlehto.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fi.peterlehto.model.game.CalculatorTests;
import fi.peterlehto.model.game.PlayerTurnManagerTests;
import fi.peterlehto.model.game.world.GameWorldItemTests;
import fi.peterlehto.model.game.world.GameWorldMoveTests;
import fi.peterlehto.model.gameitem.character.CharacterTests;
import fi.peterlehto.model.gameitem.inventory.InventoryTests;
import fi.peterlehto.model.gameitem.value.ValueContainerTests;
import fi.peterlehto.model.map.DefaultGameMapTests;
import fi.peterlehto.model.map.builder.MapBuilderTests;
import fi.peterlehto.model.util.ModelUtilsTests;

@RunWith(Suite.class)
@SuiteClasses({ DefaultGameMapTests.class, ModelUtilsTests.class,
		ValueContainerTests.class, GameWorldItemTests.class,
		InventoryTests.class, GameWorldMoveTests.class, CalculatorTests.class,
		MapBuilderTests.class, CharacterTests.class,
		PlayerTurnManagerTests.class })
public class TestModel {

}
