package fi.peterlehto.model.util;

import junit.framework.Assert;
import model.util.ModelUtils;

import org.junit.Test;

public class ModelUtilsTests {

	@Test(expected = IllegalArgumentException.class)
	public void testNotNullOrThrowUtilMethod_NoArgsPassed_ExceptionThrown() {
		ModelUtils.notNullOrThrow();
		Assert.fail("Exception should've been thrown");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotNullOrThrowUtilMethod_NullPassed_ExceptionThrown() {
		ModelUtils.notNullOrThrow(null);
		Assert.fail("Exception should've been thrown");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotNullOrThrowUtilMethod_NullArgumentPassedAsFirst_ExceptionThrown() {
		ModelUtils.notNullOrThrow(null, 1, 2, 3);
		Assert.fail("Exception should've been thrown");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotNullOrThrowUtilMethod_NullArgumentPassedAsLast_ExceptionThrown() {
		ModelUtils.notNullOrThrow(1, 2, 3, null);
		Assert.fail("Exception should've been thrown");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotNullOrThrowUtilMethod_NullArgumentPassedInTheMiddle_ExceptionThrown() {
		ModelUtils.notNullOrThrow(1, 2, null, 4);
		Assert.fail("Exception should've been thrown");
	}
}
