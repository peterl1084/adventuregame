package fi.peterlehto.model.game.world;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.game.world.GameWorld;
import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.gameitem.inventory.Food;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.model.gameitem.inventory.Weapon;
import fi.peterlehto.model.map.GameItemMap;

public class GameWorldItemTests {

	@Test
	public void testItemPickup_CharacterAndItemsInSamePoint_ItemsAddedToInventoryAndRemovedFromTheMapSuccessfully() {
		Character characterA = new Character();
		InventoryItem pear = new Food("Pear");
		InventoryItem apple = new Food("Apple");

		GameItemMap map = new GameItemMap();

		GameWorld world = new GameWorld(map);

		world.addGameItem(characterA, Point.valueOf(10, 10));
		world.addGameItem(pear, Point.valueOf(10, 10));
		world.addGameItem(apple, Point.valueOf(10, 10));

		Assert.assertEquals(
				"Character should not yet have any items in inventory", 0,
				characterA.inventory().size());

		Assert.assertTrue(map.getItemsAt(Point.valueOf(10, 10)).containsAll(
				Arrays.asList(characterA, pear, apple)));

		world.onPickUpItems(characterA, InventoryItem.class);

		Assert.assertEquals("Character should have two items in inventory", 2,
				characterA.inventory().size());

		Assert.assertTrue("Character should have 'pear'", characterA
				.inventory().hasItem(pear));
		Assert.assertTrue("Character should have 'apple'", characterA
				.inventory().hasItem(apple));

		Assert.assertTrue("Character should still be in the map location", map
				.getItemsAt(Point.valueOf(10, 10)).contains(characterA));
		Assert.assertFalse(
				"Pear should be removed from the map as character picked it up",
				map.getItemsAt(Point.valueOf(10, 10)).contains(pear));
		Assert.assertFalse(
				"Apple should be removed from the map as character picked it up",
				map.getItemsAt(Point.valueOf(10, 10)).contains(apple));
	}

	@Test
	public void testItemPickup_CharacterAndItemsInSamePoint_PickUpOnlyFoodItems_RightsItemsPickedUp() {
		Character characterA = new Character();
		InventoryItem pear = new Food("Pear");
		InventoryItem apple = new Food("Apple");
		InventoryItem sword = new Weapon("Sword");

		GameItemMap map = new GameItemMap();
		GameWorld world = new GameWorld(map);

		world.addGameItem(characterA, Point.valueOf(10, 10));
		world.addGameItem(pear, Point.valueOf(10, 10));
		world.addGameItem(apple, Point.valueOf(10, 10));
		world.addGameItem(sword, Point.valueOf(10, 10));

		Assert.assertEquals(
				"Character should not yet have any items in inventory", 0,
				characterA.inventory().size());

		Assert.assertTrue(map.getItemsAt(Point.valueOf(10, 10)).containsAll(
				Arrays.asList(characterA, pear, apple, sword)));

		world.onPickUpItems(characterA, Food.class);

		Assert.assertEquals("Character should have two items in inventory", 2,
				characterA.inventory().size());

		Assert.assertTrue("Character should have 'pear'", characterA
				.inventory().hasItem(pear));
		Assert.assertTrue("Character should have 'apple'", characterA
				.inventory().hasItem(apple));
		Assert.assertFalse("Character should NOT have 'sword'", characterA
				.inventory().hasItem(sword));

		Assert.assertTrue("Character should still be in the map location", map
				.getItemsAt(Point.valueOf(10, 10)).contains(characterA));
		Assert.assertFalse(
				"Pear should be removed from the map as character picked it up",
				map.getItemsAt(Point.valueOf(10, 10)).contains(pear));
		Assert.assertFalse(
				"Apple should be removed from the map as character picked it up",
				map.getItemsAt(Point.valueOf(10, 10)).contains(apple));
		Assert.assertTrue(
				"Sword should still be in the map and character should not have picked it up",
				map.getItemsAt(Point.valueOf(10, 10)).contains(sword));
	}

	@Test
	public void testCharacterHasAnItem_CharacterDropsTheItem_ItemAppearsOnMapAndIsRemovedFromCharacter() {
		Character character = new Character();
		InventoryItem weapon = new Weapon("Sword");

		GameItemMap map = new GameItemMap();
		GameWorld world = new GameWorld(map);

		character.inventory().add(weapon);
		Assert.assertTrue(character.inventory().hasItem(weapon));
		Assert.assertFalse(map.getItemsAt(Point.valueOf(1, 1)).contains(weapon));

		world.addGameItem(character, Point.valueOf(1, 1));
		world.onDropItem(character, weapon);

		Assert.assertFalse(character.inventory().hasItem(weapon));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).contains(weapon));
	}
}
