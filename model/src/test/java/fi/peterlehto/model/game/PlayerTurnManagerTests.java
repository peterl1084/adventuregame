package fi.peterlehto.model.game;

import junit.framework.Assert;

import org.junit.Test;

import fi.peterlehto.model.game.AIPlayer;
import fi.peterlehto.model.game.HumanPlayer;
import fi.peterlehto.model.game.Player;
import fi.peterlehto.model.game.PlayerTurnManager;

public class PlayerTurnManagerTests {

	@Test
	public void testPlayerTurnDone_NextPlayersTurnBegins_NoProblems() {
		Player a = new HumanPlayer();
		Player b = new AIPlayer();

		PlayerTurnManager manager = new PlayerTurnManager(a, b);

		for (int i = 0; i < 100; i++) {
			Assert.assertEquals(
					"Player 'a' should begin as he was added first", a,
					manager.getPlayerInTurn());
			Assert.assertEquals("Player 'b' should be next in turn", b,
					manager.getNextPlayer());
			Assert.assertEquals(
					"Player 'a' should still have turn as getNextPlayer should not change the state of turn manager",
					a, manager.getPlayerInTurn());

			// B's turn
			manager.onTurnDone();

			Assert.assertEquals(
					"Player 'b' should have turn as player a's turn was done",
					b, manager.getPlayerInTurn());

			Assert.assertEquals("Player 'a' should be next in turn", a,
					manager.getNextPlayer());

			// A's turn
			manager.onTurnDone();
		}
	}
}
