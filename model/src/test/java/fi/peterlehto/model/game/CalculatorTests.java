package fi.peterlehto.model.game;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;

import fi.peterlehto.model.game.Calculator;
import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.gameitem.inventory.Weapon;
import fi.peterlehto.model.gameitem.value.CharacterAttribute;
import fi.peterlehto.model.gameitem.value.CharacterStat;
import fi.peterlehto.model.gameitem.value.InventoryItemAttribute;

public class CalculatorTests {

	@Test
	public void testCalculateAttackEndurance_NonFatiguedCharacter_FullAttackEndurance() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		Weapon sword = new Weapon("Sword");
		character.pickupItem(sword);
		sword.setAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE, 20);
		character.statistics().setValue(CharacterStat.ENDURANCE, 100);

		Assert.assertEquals(BigDecimal.ONE,
				calculator.calculateEndurance(character));
	}

	@Test
	public void testCalculateAttackEndurance_SameEnduranceAsMaxDamageEndurance_FullAttackEndurance() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		Weapon sword = new Weapon("Sword");
		character.pickupItem(sword);
		sword.setAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE, 20);
		character.statistics().setValue(CharacterStat.ENDURANCE, 20);

		Assert.assertEquals(BigDecimal.ONE,
				calculator.calculateEndurance(character));
	}

	@Test
	public void testCalculateAttackEndurance_75PercentOfMaxDamageEndurance_75PercentAttackEndurance() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		Weapon sword = new Weapon("Sword");
		character.pickupItem(sword);
		sword.setAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE, 20);
		character.statistics().setValue(CharacterStat.ENDURANCE, 15);

		Assert.assertEquals(BigDecimal.valueOf(0.75).setScale(2),
				calculator.calculateEndurance(character));
	}

	@Test
	public void testCalculateAttackEndurance_25PercentOfMaxDamageEndurance_25PercentAttackEndurance() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		Weapon sword = new Weapon("Sword");
		character.pickupItem(sword);
		sword.setAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE, 20);
		character.statistics().setValue(CharacterStat.ENDURANCE, 5);

		Assert.assertEquals(BigDecimal.valueOf(0.25).setScale(2),
				calculator.calculateEndurance(character));
	}

	@Test
	public void testCalculateAttackEndurance_FatiguedCharacter_0PercentAttackEndurance() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		Weapon sword = new Weapon("Sword");
		character.pickupItem(sword);
		sword.setAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE, 20);
		character.statistics().setValue(CharacterStat.ENDURANCE, 0);

		Assert.assertEquals(BigDecimal.valueOf(0.00).setScale(2),
				calculator.calculateEndurance(character));
	}

	@Test
	public void testCalculateStrengthBonus_FullOfMaxStrenght_1Returned() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		character.attributes().setValue(CharacterAttribute.STRENGTH, 5);

		Assert.assertEquals(BigDecimal.valueOf(1),
				calculator.calculateStrengthBonus(character));
	}

	@Test
	public void testCalculateStrengthBonus_MinimumStrength_05Returned() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		for (int i = 1; i <= CharacterAttribute.STRENGTH.maxValue(); i++) {
			character.attributes().setValue(CharacterAttribute.STRENGTH, i);
			System.out
					.println("Strength: "
							+ i
							+ " bonus: "
							+ calculator.calculateStrengthBonus(character)
									.floatValue());
		}
	}

	@Test
	public void testCalculateTotalDamage() {
		Calculator calculator = new Calculator();

		Character character = new Character();
		Weapon sword = new Weapon("Sword");
		character.pickupItem(sword);
		sword.setAttribute(InventoryItemAttribute.MAX_DAMAGE_ENDURANCE, 20);
		sword.setAttribute(InventoryItemAttribute.DAMAGE, 10);
		sword.setAttribute(InventoryItemAttribute.DURABILITY, 100);
		character.statistics().setValue(CharacterStat.ENDURANCE,
				CharacterStat.ENDURANCE.maxValue());

		for (int i = 1; i <= CharacterAttribute.STRENGTH.maxValue(); i++) {
			character.attributes().setValue(CharacterAttribute.STRENGTH, i);
			System.out.println("Strength: " + i + " bonus: "
					+ calculator.calculateStrengthBonus(character).floatValue()
					+ "damage: " + calculator.calculateTotalDamage(character));
		}
	}
}
