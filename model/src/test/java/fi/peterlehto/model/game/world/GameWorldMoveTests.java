package fi.peterlehto.model.game.world;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.game.world.GameWorld;
import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.map.GameItemMap;

public class GameWorldMoveTests {

	@Test
	public void testMoveCharacterFullCircle_MoveOneByOne_CharacterMovedCorrectly() {
		GameItemMap map = new GameItemMap();
		GameWorld world = new GameWorld(map);

		Character a = new Character();
		world.addGameItem(a, Point.valueOf(5, 5));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(5, 5)).contains(a));

		world.onMove(a, MoveDirection.EAST);
		Assert.assertFalse(map.getItemsAt(Point.ZERO).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(6, 5)).contains(a));

		world.onMove(a, MoveDirection.SOUTH_EAST);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(6, 5)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(7, 6)).contains(a));

		world.onMove(a, MoveDirection.SOUTH);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(7, 6)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(7, 7)).contains(a));

		world.onMove(a, MoveDirection.SOUTH_WEST);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(7, 7)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(6, 8)).contains(a));

		world.onMove(a, MoveDirection.WEST);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(6, 8)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(5, 8)).contains(a));

		world.onMove(a, MoveDirection.NORT_WEST);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(5, 8)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(4, 7)).contains(a));

		world.onMove(a, MoveDirection.NORTH);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(4, 7)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(4, 6)).contains(a));

		world.onMove(a, MoveDirection.NORTH_EAST);
		Assert.assertFalse(map.getItemsAt(Point.valueOf(4, 6)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(5, 5)).contains(a));
	}

	@Test
	public void testMovingCharacter_SpaceOccupied_MoveNotAllowed() {
		GameItemMap map = new GameItemMap();
		GameWorld world = new GameWorld(map);

		Character a = new Character();
		Character b = new Character();

		world.addGameItem(a, Point.ZERO);
		world.addGameItem(b, Point.valueOf(1, 0));

		Assert.assertTrue(map.getItemsAt(Point.ZERO).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 0)).contains(b));

		Assert.assertFalse(
				"Character 'a' should not have moved as (1:0) is occupied",
				world.onMove(a, MoveDirection.EAST));

		Assert.assertTrue(map.getItemsAt(Point.ZERO).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 0)).contains(b));

		Assert.assertTrue(
				"Character 'a' should be able to move to (1:1) as it's free",
				world.onMove(a, MoveDirection.SOUTH_EAST));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).contains(a));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 0)).contains(b));
	}
}
