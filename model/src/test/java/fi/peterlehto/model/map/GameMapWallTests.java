package fi.peterlehto.model.map;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.gameitem.tile.Wall;
import fi.peterlehto.model.gameitem.tile.Wall.WallType;
import fi.peterlehto.model.map.GameItemMap;
import fi.peterlehto.model.map.builder.MapBuilder;

public class GameMapWallTests {

	@Test
	public void testSettingWalls_CorrectWallTypesSet() {
		MapBuilder builder = new MapBuilder();
		GameItemMap map = builder.width(100).height(100)
				.fill(Point.ZERO, Point.valueOf(2, 1), Wall.class).build();

		// 0, 0, 0
		// 0, 0, 0

		Assert.assertEquals(1, map.getItemsAt(Point.valueOf(0, 0)).size());
		Assert.assertEquals(1, map.getItemsAt(Point.valueOf(1, 0)).size());
		Assert.assertEquals(1, map.getItemsAt(Point.valueOf(2, 0)).size());
		Assert.assertEquals(1, map.getItemsAt(Point.valueOf(0, 0)).size());
		Assert.assertEquals(1, map.getItemsAt(Point.valueOf(1, 1)).size());
		Assert.assertEquals(1, map.getItemsAt(Point.valueOf(2, 1)).size());

		Assert.assertEquals(WallType.TOP_LEFT,
				map.getItemsAt(Point.valueOf(0, 0), Wall.class).iterator()
						.next().getType());
		Assert.assertEquals(WallType.INTERSECTION_TOP_DOWN,
				map.getItemsAt(Point.valueOf(1, 0), Wall.class).iterator()
						.next().getType());
		Assert.assertEquals(WallType.TOP_RIGHT,
				map.getItemsAt(Point.valueOf(2, 0), Wall.class).iterator()
						.next().getType());
		Assert.assertEquals(WallType.BOTTOM_LEFT,
				map.getItemsAt(Point.valueOf(0, 1), Wall.class).iterator()
						.next().getType());
		Assert.assertEquals(WallType.INTERSECTION_BOTTOM_UP,
				map.getItemsAt(Point.valueOf(1, 1), Wall.class).iterator()
						.next().getType());
		Assert.assertEquals(WallType.BOTTOM_RIGHT,
				map.getItemsAt(Point.valueOf(2, 1), Wall.class).iterator()
						.next().getType());
	}
}
