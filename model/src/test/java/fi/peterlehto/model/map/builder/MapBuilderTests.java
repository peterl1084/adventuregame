package fi.peterlehto.model.map.builder;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.gameitem.tile.Wall;
import fi.peterlehto.model.map.GameItemMap;
import fi.peterlehto.model.map.builder.MapBuilder;

public class MapBuilderTests {

	@Test
	public void testMapBuilder_NormalMap_MapBuiltSuccessfully() {
		MapBuilder builder = new MapBuilder();
		GameItemMap map = builder.width(20).height(20).build();

		Assert.assertEquals(20, map.width());
		Assert.assertEquals(20, map.height());
	}

	@Test
	public void testMapBuilder_UseFillAndClear_ExpectedPointsFilledAndCleared() {
		MapBuilder builder = new MapBuilder();
		builder.width(20).height(20)
				.fill(Point.valueOf(1, 1), Point.valueOf(7, 3), Wall.class);

		GameItemMap map = builder.build();

		for (int x = 1; x <= 7; x++) {
			for (int y = 1; y <= 3; y++) {
				Assert.assertTrue("No item at " + x + ";" + y,
						map.getItemsAt(Point.valueOf(x, y)).size() == 1);
			}
		}

		for (int x = 0; x <= 10; x++) {
			Assert.assertTrue(map.getItemsAt(Point.valueOf(x, 0)).isEmpty());
		}

		for (int y = 0; y <= 10; y++) {
			Assert.assertTrue(map.getItemsAt(Point.valueOf(0, y)).isEmpty());
		}

		for (int x = 0; x <= 10; x++) {
			Assert.assertTrue(map.getItemsAt(Point.valueOf(x, 4)).isEmpty());
		}

		for (int y = 0; y <= 10; y++) {
			Assert.assertTrue(map.getItemsAt(Point.valueOf(8, y)).isEmpty());
		}

		builder.clear(Point.valueOf(5, 2), Point.valueOf(5, 2));
		map = builder.build();

		Assert.assertTrue(map.getItemsAt(Point.valueOf(5, 2)).isEmpty());

		builder.clear(Point.ZERO, map.bottomRightPoint());
		map = builder.build();

		for (int x = 0; x <= map.width(); x++) {
			for (int y = 0; y <= map.height(); y++) {
				Assert.assertTrue(map.getItemsAt(Point.valueOf(x, y)).isEmpty());
			}
		}
	}
}
