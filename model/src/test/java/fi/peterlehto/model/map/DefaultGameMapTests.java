package fi.peterlehto.model.map;

import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.inventory.Food;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.model.map.DefaultObjectMap;
import fi.peterlehto.model.map.GameItemMap;
import fi.peterlehto.model.map.ObjectMap;

public class DefaultGameMapTests {

	@Test
	public void testAddingItemsToMap_ItemAddedAndFound_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(
				"False was returned even though item was supposed to be added",
				map.put("a", Point.valueOf(1, 1)));

		Assert.assertTrue(map.contains("a"));

		Set<String> itemsAt = map.getItemsAt(Point.valueOf(1, 1));
		Assert.assertTrue("Map had more than one item at 1:1",
				itemsAt.size() == 1);
		Assert.assertTrue("Expected item wasn't 'a'", itemsAt.iterator().next()
				.equals("a"));

		Assert.assertTrue("Adjacent point should be empty",
				map.getItemsAt(Point.valueOf(0, 0)).isEmpty());
	}

	@Test
	public void testAddItem_ItemAddedOutsideMapBounds_ItemNotAdded() {
		ObjectMap<String> map = new DefaultObjectMap<>(10, 10);
		Assert.assertFalse(map.put("a", Point.valueOf(11, 11)));
	}

	@Test
	public void testMoveItem_ItemMovedOutsideOfMapBounds_ItemNotMoved() {
		ObjectMap<String> map = new DefaultObjectMap<>(10, 10);

		Assert.assertTrue(map.put("a", Point.valueOf(9, 9)));

		// Move outside of bounds
		Assert.assertFalse(map.move("a", Point.valueOf(10, 10)));

		// Should remain where was
		Assert.assertTrue(map.getItemsAt(Point.valueOf(9, 9)).contains("a"));
		Assert.assertFalse(map.getItemsAt(Point.valueOf(10, 10)).contains("a"));

		// Move inside bounds
		Assert.assertTrue(map.move("a", Point.valueOf(8, 8)));

		// Should have moved
		Assert.assertFalse(map.getItemsAt(Point.valueOf(9, 9)).contains("a"));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(8, 8)).contains("a"));
	}

	@Test
	public void testGettingItems_GivenPointOutOfMapBounds_EmptySetReturned() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(
				"False was returned even though item was supposed to be added",
				map.put("a", Point.valueOf(1, 1)));

		Assert.assertTrue(map.contains("a"));

		Assert.assertTrue("15000:15000 point should be empty",
				map.getItemsAt(Point.valueOf(15000, 15000)).isEmpty());
	}

	@Test
	public void testAddingItemAndRemovingByItem_ItemAddedAndRemoved_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(
				"False was returned even though item was supposed to be added",
				map.put("a", Point.valueOf(1, 1)));

		Assert.assertTrue(map.contains("a"));

		Assert.assertTrue(
				"Remove item returned false when item was supposed to be found",
				map.remove("a"));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).isEmpty());

		Assert.assertFalse(
				"Remove item returned true when no more items expected",
				map.remove("a"));
		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).isEmpty());

		Assert.assertFalse(map.contains("a"));
	}

	@Test
	public void testAddingItemAndRemovingByItem_MultipleItemsAtSamePoint_CorrectItemAddedAndRemoved_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(
				"False was returned even though item was supposed to be added",
				map.put("a", Point.valueOf(1, 1)));

		Assert.assertTrue(
				"False was returned even though item was supposed to be added",
				map.put("b", Point.valueOf(1, 1)));

		Assert.assertTrue(map.contains("a"));
		Assert.assertTrue(map.contains("b"));

		Assert.assertTrue("Map should have two items at 1:1",
				map.getItemsAt(Point.valueOf(1, 1)).size() == 2);

		Assert.assertTrue(
				"Remove item returned false when item was supposed to be found",
				map.remove("a"));
		Assert.assertFalse(
				"Remove item returned truen when item was not supposed to be found",
				map.remove("a"));
		Assert.assertTrue("Map should have two items at 1:1",
				map.getItemsAt(Point.valueOf(1, 1)).size() == 1);

		Assert.assertFalse(map.contains("a"));
		Assert.assertTrue(map.contains("b"));

		Assert.assertEquals(Optional.empty(), map.whereIs("a"));
		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs("b").get());

		Assert.assertTrue(
				"Remove item returned false when item was supposed to be found",
				map.remove("b"));

		Assert.assertEquals(Optional.empty(), map.whereIs("b"));

		Assert.assertTrue("Map should have two items at 1:1",
				map.getItemsAt(Point.valueOf(1, 1)).isEmpty());
	}

	@Test
	public void testGetItemsByItemType_TypeProvided_CorrectItemsReturned() {
		GameItemMap map = new GameItemMap();

		Character charA = new Character();
		Character charB = new Character();

		InventoryItem invItemA = new Food("pear");

		map.put(charA, Point.valueOf(1, 1));
		map.put(charB, Point.valueOf(1, 1));
		map.put(invItemA, Point.valueOf(1, 1));

		Set<GameCharacter> characters = map.getItemsAt(Point.valueOf(1, 1),
				GameCharacter.class);
		Set<InventoryItem> inventoryItems = map.getItemsAt(Point.valueOf(1, 1),
				InventoryItem.class);

		Assert.assertEquals("Number of characters should be 2", 2,
				characters.size());
		Assert.assertEquals("Number of inv items should be 1", 1,
				inventoryItems.size());

		Assert.assertTrue(characters.contains(charA));
		Assert.assertTrue(characters.contains(charB));
		Assert.assertTrue(inventoryItems.contains(invItemA));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetItemsByItemType_NullParameterProvided_ExceptionThrown() {
		GameItemMap map = new GameItemMap();

		Character charA = new Character();
		Character charB = new Character();

		InventoryItem invItemA = new Food("pear");

		map.put(charA, Point.valueOf(1, 1));
		map.put(charB, Point.valueOf(1, 1));
		map.put(invItemA, Point.valueOf(1, 1));

		map.getItemsAt(Point.valueOf(1, 1), null);
		Assert.fail("Exception should've been thrown");
	}

	@Test
	public void testAddingItemAndRemovingByPoint_ItemAddedAndRemoved_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(map.put("a", Point.valueOf(1, 1)));
		Assert.assertTrue(map.put("b", Point.valueOf(1, 1)));

		Assert.assertTrue(map.contains("a"));
		Assert.assertTrue(map.contains("b"));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).size() == 2);
		Set<String> removeItemsAt = map.removeItemsAt(Point.valueOf(1, 1));
		Assert.assertTrue(removeItemsAt.contains("a"));
		Assert.assertTrue(removeItemsAt.contains("b"));

		Assert.assertTrue(map.removeItemsAt(Point.valueOf(1, 1)).isEmpty());

		Assert.assertFalse(map.remove("a"));
		Assert.assertFalse(map.remove("b"));

		Assert.assertFalse(map.contains("a"));
		Assert.assertFalse(map.contains("b"));
	}

	@Test
	public void testRemovingMultipleItems_AllItemsExistInTheMap_AllGivenItemsRemoved() {
		ObjectMap<String> map = new DefaultObjectMap<>();

		map.put("a", Point.valueOf(1, 1));
		map.put("b", Point.valueOf(1, 1));
		map.put("c", Point.valueOf(1, 1));

		Assert.assertTrue(map.contains("a"));
		Assert.assertTrue(map.contains("b"));
		Assert.assertTrue(map.contains("c"));

		Set<? extends String> removeAll = map.removeAll("a", "b");
		Assert.assertEquals(2, removeAll.size());

		Assert.assertTrue(removeAll.contains("a"));
		Assert.assertTrue(removeAll.contains("b"));
		Assert.assertFalse(removeAll.contains("c"));

		Assert.assertFalse(map.contains("a"));
		Assert.assertFalse(map.contains("b"));
		Assert.assertTrue(map.contains("c"));
	}

	@Test
	public void testRemovingMultipleItems_NotAllItemsExistInTheMap_AllGivenItemsRemoved_NonExistentItemsCauseNoTrouble() {
		ObjectMap<String> map = new DefaultObjectMap<>();

		map.put("a", Point.valueOf(1, 1));
		map.put("b", Point.valueOf(1, 1));
		map.put("c", Point.valueOf(1, 1));
		map.put("d", Point.valueOf(1, 1));

		Assert.assertTrue(map.contains("a"));
		Assert.assertTrue(map.contains("b"));
		Assert.assertTrue(map.contains("c"));
		Assert.assertTrue(map.contains("d"));

		Set<? extends String> removeAll = map.removeAll("c", "d", "e");
		Assert.assertEquals(2, removeAll.size());
		Assert.assertFalse(removeAll.contains("a"));
		Assert.assertFalse(removeAll.contains("b"));
		Assert.assertTrue(removeAll.contains("c"));
		Assert.assertTrue(removeAll.contains("d"));
		Assert.assertFalse(removeAll.contains("e"));

		Assert.assertTrue(map.contains("a"));
		Assert.assertTrue(map.contains("b"));
		Assert.assertFalse(map.contains("c"));
		Assert.assertFalse(map.contains("d"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemovingMultipleItems_NullParameterGivenInArray_ExceptionThrown() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		map.removeAll("a", "b", null, "d");
		Assert.fail("Exception should've been thrown");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemovingMultipleItems_NoParameterGiven_ExceptionThrown() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		map.removeAll();
		Assert.fail("Exception should've been thrown");
	}

	@Test
	public void testAddingItemTwiceToSameLocation_ItemAddedOnlyOnce_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(map.put("a", Point.valueOf(1, 1)));
		Assert.assertFalse(map.put("a", Point.valueOf(1, 1)));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).size() == 1);

		Assert.assertTrue(map.contains("a"));
	}

	@Test
	public void testAddingItemTwiceToDifferentLocations_ItemAddedOnlyOnce_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(map.put("a", Point.valueOf(1, 1)));
		Assert.assertFalse(map.put("a", Point.valueOf(2, 2)));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).size() == 1);
		Assert.assertTrue(map.getItemsAt(Point.valueOf(2, 2)).isEmpty());

		Assert.assertTrue(map.contains("a"));
	}

	@Test
	public void testAddingItemWithNoEqualsImplementationToSameLocationTwice_ItemAddedOnlyOnce_NoProblems() {
		ObjectMap<Object> map = new DefaultObjectMap<>();
		Object item = new Object();
		Assert.assertTrue(map.put(item, Point.valueOf(1, 1)));
		Assert.assertFalse(map.put(item, Point.valueOf(1, 1)));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).size() == 1);

		Assert.assertTrue(map.contains(item));
		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs(item).get());
	}

	@Test
	public void testAddingItemWithNoEqualsImplementationToDifferentLocationsTwice_ItemAddedOnlyOnce_NoProblems() {
		ObjectMap<Object> map = new DefaultObjectMap<>();
		Object item = new Object();
		Assert.assertTrue(map.put(item, Point.valueOf(1, 1)));
		Assert.assertFalse(map.put(item, Point.valueOf(2, 2)));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).size() == 1);
		Assert.assertTrue(map.getItemsAt(Point.valueOf(2, 2)).isEmpty());

		Assert.assertTrue(map.contains(item));
		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs(item).get());
	}

	@Test
	public void testAddingTwoDifferentItemsWithNoEqualsImplementationToSameLocationTwice_ItemAddeTwice_NoProblems() {
		ObjectMap<Object> map = new DefaultObjectMap<>();
		Object a = new Object();
		Object b = new Object();
		Assert.assertTrue(map.put(a, Point.valueOf(1, 1)));
		Assert.assertTrue(map.put(b, Point.valueOf(1, 1)));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).size() == 2);

		Assert.assertTrue(map.contains(a));
		Assert.assertTrue(map.contains(b));
		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs(a).get());
		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs(b).get());
	}

	@Test
	public void testUsingWhereIsForSuccessfullyAddedItem_ItemFoundFromRightPoint_NoProblems() {
		ObjectMap<Integer> map = new DefaultObjectMap<>();
		Assert.assertTrue(map.put(1, Point.valueOf(1, 1)));
		Assert.assertTrue(map.put(2, Point.valueOf(1, 1)));
		Assert.assertTrue(map.put(3, Point.valueOf(2, 2)));

		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs(1).get());
		Assert.assertEquals(Point.valueOf(1, 1), map.whereIs(2).get());
		Assert.assertEquals(Point.valueOf(2, 2), map.whereIs(3).get());
	}

	@Test
	public void testUsingContainsForSuccessfullyAddedItem_ItemFound_NoProblems() {
		ObjectMap<Integer> map = new DefaultObjectMap<>();
		Assert.assertTrue(map.put(1, Point.valueOf(1, 1)));
		Assert.assertTrue(map.put(2, Point.valueOf(1, 1)));
		Assert.assertTrue(map.put(3, Point.valueOf(2, 2)));

		Assert.assertTrue(map.contains(1));
		Assert.assertTrue(map.contains(2));
		Assert.assertTrue(map.contains(3));
	}

	@Test
	public void testMovingExistingItemToNewPosition_MovedSuccessfully_NoProblems() {
		ObjectMap<String> map = new DefaultObjectMap<>();
		Assert.assertTrue(map.put("a", Point.valueOf(1, 1)));

		Assert.assertTrue(map.contains("a"));

		Set<String> itemsAt = map.getItemsAt(Point.valueOf(1, 1));
		Assert.assertTrue("Map had more than one item at 1:1",
				itemsAt.size() == 1);
		Assert.assertTrue("Expected item wasn't 'a'", itemsAt.iterator().next()
				.equals("a"));

		Assert.assertTrue("Adjacent point should be empty",
				map.getItemsAt(Point.valueOf(0, 0)).isEmpty());

		map.move("a", Point.valueOf(0, 0));

		Assert.assertTrue(map.contains("a"));

		Assert.assertTrue(map.getItemsAt(Point.valueOf(1, 1)).isEmpty());
		Assert.assertTrue(map.getItemsAt(Point.valueOf(0, 0)).size() == 1);
	}

	@Test
	public void testMapBottomRightPoint_PointFoundCorrectly() {
		DefaultObjectMap<String> map = new DefaultObjectMap<>(20, 20);
		Assert.assertEquals(Point.valueOf(19, 19), map.bottomRightPoint());
	}
}
