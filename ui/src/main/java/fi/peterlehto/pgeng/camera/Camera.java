package fi.peterlehto.pgeng.camera;

import java.math.BigDecimal;
import java.math.RoundingMode;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;

public class Camera {

	public enum Pan {
		NORTH(0, -1),
		NORTH_EAST(1, -1),
		EAST(1, 0),
		SOUTH_EAST(1, 1),
		SOUTH(0, 1),
		SOUTH_WEST(-1, 1),
		WEST(-1, 0),
		NORTH_WEST(-1, -1);

		private final int x;
		private final int y;

		private Pan(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public Point apply(Point target, int scale) {
			scale = Math.min(1, scale);

			return Point.valueOf(target.getX() + (x * scale), target.getY()
					+ (y * scale));
		}
	}

	private final static int DEFAULT_VIEW_AREA_WIDTH = 1024;
	private final static int DEFAULT_VIEW_AREA_HEIGHT = 768;

	private final static float DEFAULT_MIN_ZOOM_FACTOR = 0.25f;
	private final static float DEFAULT_MAX_ZOOM_FACTOR = 2.00f;
	private final static float DEFAULT_ZOOM_FACTOR = 1.00f;

	private Point viewPortCenterPoint;
	private BigDecimal zoomFactor;

	private final BigDecimal viewPortUnscaledWidth;
	private final BigDecimal viewPortUnscaledHeight;

	private final float minZoomFactor;
	private final float maxZoomFactor;
	private Integer orthoLeft;
	private Integer orthoRight;
	private Integer orthoBottom;
	private Integer orthoTop;
	private BigDecimal viewPortHeight;
	private BigDecimal viewPortWidth;
	private Point topLeft;
	private Point bottomRight;

	public Camera() {
		this(DEFAULT_VIEW_AREA_WIDTH, DEFAULT_VIEW_AREA_HEIGHT,
				DEFAULT_MIN_ZOOM_FACTOR, DEFAULT_MAX_ZOOM_FACTOR);
	}

	public Camera(int viewAreaWidth, int viewAreaHeight) {
		this(viewAreaWidth, viewAreaHeight, DEFAULT_MIN_ZOOM_FACTOR,
				DEFAULT_MAX_ZOOM_FACTOR);
	}

	public Camera(int viewAreaWidth, int viewAreaHeight, float minZoomFactor,
			float maxZoomFactor) {
		if (viewAreaWidth % 2 != 0)
			throw new IllegalArgumentException(
					"View area width must be an even number, given "
							+ viewAreaWidth);
		if (viewAreaHeight % 2 != 0)
			throw new IllegalArgumentException(
					"View area height must be an even number, given "
							+ viewAreaHeight);

		if (minZoomFactor > maxZoomFactor)
			throw new IllegalArgumentException(
					"Minimum zoom factor must be smaller value than max zoom factor");

		if (minZoomFactor < 0.1)
			minZoomFactor = 0.1f;

		if (maxZoomFactor > 4)
			maxZoomFactor = 4;

		viewPortUnscaledWidth = BigDecimal.valueOf(Math.max(0, viewAreaWidth));
		viewPortUnscaledHeight = BigDecimal
				.valueOf(Math.max(0, viewAreaHeight));

		this.minZoomFactor = minZoomFactor;
		this.maxZoomFactor = maxZoomFactor;

		viewPortCenterPoint = Point.valueOf(viewAreaWidth / 2,
				viewAreaHeight / 2);
		zoomFactor = BigDecimal.valueOf(DEFAULT_ZOOM_FACTOR);
	}

	public Point getTopLeftCornerPoint() {
		if (topLeft == null) {
			BigDecimal viewPortScaledWidth = calculateViewPortWidth();
			BigDecimal viewPortScaledHeight = calculateViewPortHeight();

			BigDecimal halfScaledWidth = viewPortScaledWidth.divide(BigDecimal
					.valueOf(2));
			BigDecimal halfScaledHeight = viewPortScaledHeight
					.divide(BigDecimal.valueOf(2));

			BigDecimal centerPointX = BigDecimal.valueOf(getCenterPoint()
					.getX());
			BigDecimal centerPointY = BigDecimal.valueOf(getCenterPoint()
					.getY());

			topLeft = Point.valueOf(
					centerPointX.subtract(halfScaledWidth)
							.setScale(0, RoundingMode.HALF_UP).intValue(),
					centerPointY.subtract(halfScaledHeight)
							.setScale(0, RoundingMode.HALF_UP).intValue());
		}

		return topLeft;
	}

	public Point getBottomRightCornerPoint() {
		if (bottomRight == null) {
			Point topLeft = getTopLeftCornerPoint();

			bottomRight = Point.valueOf(topLeft.getX()
					+ calculateViewPortWidthAsInteger(), topLeft.getY()
					+ calculateViewPortHeightAsInteger());
		}
		return bottomRight;
	}

	public PositionalRectangle getViewArea() {
		return new PositionalRectangle(getCenterPoint(),
				calculateViewPortWidthAsInteger(),
				calculateViewPortHeightAsInteger(), Align.CENTER_POINT);
	}

	public void setCenterPointTo(Point point) {
		topLeft = null;
		bottomRight = null;
		viewPortCenterPoint = new Point(point);
	}

	public void pan(Pan pan, int amount) {
		setCenterPointTo(pan.apply(viewPortCenterPoint, amount));
	}

	public Point getCenterPoint() {
		return viewPortCenterPoint;
	}

	public void zoomTo(float zoomFactor) {
		orthoLeft = null;
		orthoRight = null;
		orthoTop = null;
		orthoBottom = null;
		viewPortWidth = null;
		viewPortHeight = null;
		topLeft = null;
		bottomRight = null;

		this.zoomFactor = BigDecimal.valueOf(Math.min(maxZoomFactor,
				Math.max(minZoomFactor, zoomFactor)));
	}

	private BigDecimal calculateViewPortWidth() {
		if (viewPortWidth == null)
			viewPortWidth = viewPortUnscaledWidth.divide(zoomFactor, 2,
					RoundingMode.HALF_UP);

		return viewPortWidth;
	}

	private int calculateViewPortWidthAsInteger() {
		return calculateViewPortWidth().intValue();
	}

	private BigDecimal calculateViewPortHeight() {
		if (viewPortHeight == null)
			viewPortHeight = viewPortUnscaledHeight.divide(zoomFactor, 2,
					RoundingMode.HALF_UP);

		return viewPortHeight;
	}

	private int calculateViewPortHeightAsInteger() {
		return calculateViewPortHeight().intValue();
	}

	public int getCenterOrthoLeft() {
		if (orthoLeft == null)
			orthoLeft = viewPortUnscaledWidth.divide(BigDecimal.valueOf(2))
					.negate().divide(zoomFactor, 2, RoundingMode.HALF_UP)
					.intValue();

		return orthoLeft;
	}

	public int getCenterOrthoRight() {
		if (orthoRight == null)
			orthoRight = viewPortUnscaledWidth.divide(BigDecimal.valueOf(2))
					.divide(zoomFactor, 2, RoundingMode.HALF_UP).intValue();

		return orthoRight;
	}

	public int getCenterOrthoBottom() {
		if (orthoBottom == null)
			orthoBottom = viewPortUnscaledHeight.divide(BigDecimal.valueOf(2))
					.divide(zoomFactor, 2, RoundingMode.HALF_UP).intValue();

		return orthoBottom;
	}

	public int getCenterOrthoTop() {
		if (orthoTop == null)
			orthoTop = viewPortUnscaledHeight.divide(BigDecimal.valueOf(2))
					.negate().divide(zoomFactor, 2, RoundingMode.HALF_UP)
					.intValue();

		return orthoTop;
	}
}
