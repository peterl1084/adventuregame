package fi.peterlehto.pgeng.command.exception;

import fi.peterlehto.pgeng.command.interpreter.CommandInterpreterException;

/**
 * CommandNotFoundException is thrown if interpreter fails to find suitable
 * command for request.
 * 
 * @author Peter
 */
public class CommandNotFoundException extends CommandInterpreterException {
    private static final long serialVersionUID = 3916087842905376094L;

    public CommandNotFoundException(String message) {
        super(message);
    }

    public CommandNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
