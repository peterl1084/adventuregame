package fi.peterlehto.pgeng.command.interpreter;

import fi.peterlehto.pgeng.command.CommandExecutionTarget;

/**
 * CommandInterpreter is used to interpret commands from incoming command
 * String. Commands are always interpreted on some execution target.
 * 
 * @param <TARGET>
 *            execution target of this interpreter
 * 
 * @author Peter
 */
public interface CommandInterpreter<TARGET extends CommandExecutionTarget> {
    /**
     * Attempts to interpret given command
     * 
     * @param command
     *            as String
     * @return command exit message as String
     * @throws CommandInterpreterException
     *             if interpreting command fails.
     */
    String interpretCommand(String command) throws CommandInterpreterException;
}
