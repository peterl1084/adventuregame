package fi.peterlehto.pgeng.command.interpreter;

import fi.peterlehto.pgeng.command.Command;
import fi.peterlehto.pgeng.command.CommandExecutionTarget;
import fi.peterlehto.pgeng.command.exception.CommandInvalidArgumentsException;
import fi.peterlehto.pgeng.command.exception.CommandNotFoundException;
import fi.peterlehto.pgeng.command.executor.CommandExecutor;
import fi.peterlehto.pgeng.command.executor.CommandExecutorException;

/**
 * AbstractCommandInterpreter is an abstract implementation of interpreter which
 * handles the command string, finding actual command executor and providing
 * parameters for it.
 * 
 * @param <TARGET>
 *            CommandExecutionTarget
 * 
 * @author Peter
 */
public abstract class AbstractCommandInterpreter<TARGET extends CommandExecutionTarget> implements
        CommandInterpreter<TARGET> {

    private static final String SPACE_CHARACTER_SPLIT = "\\s+";
    private final TARGET target;

    public AbstractCommandInterpreter(TARGET target) {
        this.target = target;
    }

    @Override
    public String interpretCommand(String commandString) throws CommandInterpreterException {
        if (commandString == null) {
            throw new CommandInterpreterException("Command was null");
        }
        if (commandString.isEmpty()) {
            throw new CommandInterpreterException("Command was empty");
        }

        String[] parameters = commandString.split(SPACE_CHARACTER_SPLIT);
        Command<TARGET> command = getCommandForName(parameters[0]);

        CommandExecutor<TARGET> executor = getParametrizedExecutorForCommand(command, parameters);

        return executor.execute(target);
    }

    /**
     * Fetches the executor from the command and parametrizes it with given
     * parameters.
     * 
     * @param command
     * @param parameters
     * @return CommandExecutor on execution target that is ready to be executed
     * @throws CommandInvalidArgumentsException
     * @throws CommandExecutorException
     */
    protected CommandExecutor<TARGET> getParametrizedExecutorForCommand(Command<TARGET> command, String[] parameters)
            throws CommandInvalidArgumentsException, CommandExecutorException {
        CommandExecutor<TARGET> executor = command.getExecutor();

        int numberOfParams = Math.max(0, parameters.length - 1);
        String[] commandParams = new String[numberOfParams];

        for (int i = 1; i < parameters.length; i++) {
            commandParams[i - 1] = parameters[i];
        }

        executor.setParameters(commandParams);

        return executor;
    }

    /**
     * @param commandName
     * @return command for given name.
     * @throws CommandNotFoundException
     *             if no command for given name is found
     */
    protected abstract Command<TARGET> getCommandForName(String commandName) throws CommandNotFoundException;
}
