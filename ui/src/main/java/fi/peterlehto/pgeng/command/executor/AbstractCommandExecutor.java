package fi.peterlehto.pgeng.command.executor;

import fi.peterlehto.pgeng.command.CommandExecutionTarget;
import fi.peterlehto.pgeng.command.exception.CommandInvalidArgumentsException;

/**
 * AbstractCommandExecutor is an abstract implementation of CommandExecutor that
 * takes care of parameter type handling and parameter assigning.
 * 
 * @param <TARGET>
 *            CommandExecutionTarget of this executor.
 * 
 * @author Peter
 */
public abstract class AbstractCommandExecutor<TARGET extends CommandExecutionTarget> implements CommandExecutor<TARGET> {
    private String[] parameters;

    @Override
    public void setParameters(String[] commandParams) throws CommandInvalidArgumentsException {
        if (getNumberOfExpectedParameters() > 0) {
            if (commandParams == null) {
                throw new CommandInvalidArgumentsException("No parameters provided for command expecting "
                        + getNumberOfExpectedParameters() + " parameters");
            }

            if (commandParams.length < getNumberOfExpectedParameters()) {
                throw new CommandInvalidArgumentsException("Too few parameters specified for command, expecting "
                        + getNumberOfExpectedParameters() + " parameters");
            }
        }

        this.parameters = commandParams;
    }

    protected String getParameterValueAsString(int paramIndex) throws CommandExecutorException {
        return getParameterValue(paramIndex).toString();
    }

    protected int getParameterValueAsInteger(int paramIndex) throws CommandExecutorException {
        try {
            return Integer.parseInt(getParameterValue(paramIndex).toString());
        } catch (NumberFormatException e) {
            throw new CommandExecutorException("Parameter " + paramIndex + " is not a valid integer", e);
        }
    }

    protected float getParameterValueAsFloat(int paramIndex) throws CommandExecutorException {
        try {
            return Float.parseFloat(getParameterValue(paramIndex).toString());
        } catch (NumberFormatException e) {
            throw new CommandExecutorException("Parameter " + paramIndex + " is not a valid float", e);
        }
    }

    protected double getParameterValueAsDouble(int paramIndex) throws CommandExecutorException {
        try {
            return Double.parseDouble(getParameterValue(paramIndex).toString());
        } catch (NumberFormatException e) {
            throw new CommandExecutorException("Parameter " + paramIndex + " is not a valid double", e);
        }
    }

    protected long getParameterValueAsLong(int paramIndex) throws CommandExecutorException {
        try {
            return Long.parseLong(getParameterValue(paramIndex).toString());
        } catch (NumberFormatException e) {
            throw new CommandExecutorException("Parameter " + paramIndex + " is not a valid long", e);
        }
    }

    protected boolean getParameterValueABoolean(int paramIndex) throws CommandExecutorException {
        try {
            return Boolean.parseBoolean(getParameterValue(paramIndex).toString());
        } catch (NumberFormatException e) {
            throw new CommandExecutorException("Parameter " + paramIndex + " is not a valid long", e);
        }
    }

    private Object getParameterValue(int paramIndex) throws CommandExecutorException {
        if (parameters.length - 1 < paramIndex) {
            throw new CommandExecutorException("No parameter for index " + paramIndex);
        }

        return parameters[paramIndex];
    }

    /**
     * @return number of parameters this command expects to have
     */
    protected abstract int getNumberOfExpectedParameters();
}
