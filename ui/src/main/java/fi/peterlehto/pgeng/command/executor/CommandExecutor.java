package fi.peterlehto.pgeng.command.executor;

import fi.peterlehto.pgeng.command.CommandExecutionTarget;
import fi.peterlehto.pgeng.command.exception.CommandInvalidArgumentsException;

/**
 * CommandExecutor is used to execute the command on CommandExecutionTarget.
 * Executor can be provided with parameters.
 * 
 * 
 * 
 * @param <TARGET>
 *            CommandExecutionTarget
 * 
 * @author Peter
 */
public interface CommandExecutor<TARGET extends CommandExecutionTarget> {

    /**
     * Executes the command on given target
     * 
     * @param target
     * @return command exit message
     * @throws CommandExecutorException
     *             if execution of command fails
     */
    String execute(TARGET target) throws CommandExecutorException;

    /**
     * Sets the command parameters
     * 
     * @param commandParams
     * @throws CommandInvalidArgumentsException
     *             if parameters are invalid. Invalidity is evaluated by the
     *             specific executor.
     */
    void setParameters(String[] commandParams) throws CommandInvalidArgumentsException;
}
