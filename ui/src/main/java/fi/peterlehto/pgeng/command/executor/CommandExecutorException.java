package fi.peterlehto.pgeng.command.executor;

import fi.peterlehto.pgeng.command.interpreter.CommandInterpreterException;

/**
 * CommandExecutorException is thrown if there is an exception during command
 * execution.
 * 
 * @author Peter
 */
public class CommandExecutorException extends CommandInterpreterException {
    private static final long serialVersionUID = 8896326563904255284L;

    public CommandExecutorException(String message) {
        super(message);
    }

    public CommandExecutorException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
