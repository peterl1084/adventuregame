package fi.peterlehto.pgeng.command.exception;

import fi.peterlehto.pgeng.command.interpreter.CommandInterpreterException;

/**
 * CommandInvalidArgumentsException is thrown if command interpreter detect
 * invalid arguments in the command.
 * 
 * @author Peter
 */
public class CommandInvalidArgumentsException extends CommandInterpreterException {
    private static final long serialVersionUID = 873254336105152375L;

    public CommandInvalidArgumentsException(String message) {
        super(message);
    }

    public CommandInvalidArgumentsException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
