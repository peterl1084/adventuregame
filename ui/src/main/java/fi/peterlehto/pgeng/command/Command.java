package fi.peterlehto.pgeng.command;

import fi.peterlehto.pgeng.command.executor.CommandExecutor;
import fi.peterlehto.pgeng.command.executor.CommandExecutorException;

/**
 * Command is an interface that maps a command name with command executor on
 * certain execution target. The actual implementation can be anything, for
 * example a map or enum constant.
 * 
 * @param <TARGET>
 *            CommandExecutionTarget where the command executor will perform.
 * 
 * @author Peter
 */
public interface Command<TARGET extends CommandExecutionTarget> {

    /**
     * @return name of the command
     */
    String getCommandName();

    /**
     * @return Executor instance for this command
     * @throws CommandExecutorException
     *             if getting the executor instance fails.
     */
    CommandExecutor<TARGET> getExecutor() throws CommandExecutorException;
}
