package fi.peterlehto.pgeng.command.interpreter;

/**
 * CommandInterpreterException is a super exception for command interpreter.
 * 
 * @author Peter
 */
public class CommandInterpreterException extends Exception {
    private static final long serialVersionUID = -7330252854110879110L;

    public CommandInterpreterException(String message) {
        super(message);
    }

    public CommandInterpreterException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
