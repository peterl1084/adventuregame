package fi.peterlehto.pgeng.command;

/**
 * CommandExecutionTarget is a marker interface for software components which
 * can execute commands.
 * 
 * @author Peter
 */
public interface CommandExecutionTarget {

}
