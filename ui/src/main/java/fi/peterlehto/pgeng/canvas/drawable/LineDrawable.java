package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;

public class LineDrawable extends AbstractDrawable {
	private final Point a;
	private final Point b;

	private final int lineWidth;

	public LineDrawable(Point a, Point b, int lineWidth) {
		this(a, b, lineWidth, RGBColor.WHITE);
	}

	public LineDrawable(Point a, Point b, int lineWidth, RGBColor color) {
		super(color);

		this.a = new Point(a);
		this.b = new Point(b);

		this.lineWidth = lineWidth;
	}

	public int getLineWidth() {
		return lineWidth;
	}

	@Override
	protected void drawContent() {
		GL11.glLineWidth(lineWidth);

		GL11.glBegin(GL11.GL_LINES);

		GL11.glVertex2i(a.getX(), a.getY());
		GL11.glVertex2i(b.getX(), b.getY());

		GL11.glEnd();
	}
}
