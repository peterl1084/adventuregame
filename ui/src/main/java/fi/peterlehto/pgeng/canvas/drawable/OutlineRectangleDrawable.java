package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;

public class OutlineRectangleDrawable extends RectangleDrawable {
	private final int lineWidth;

	public OutlineRectangleDrawable(PositionalRectangle rectangle,
			float direction, int lineWidth) {
		this(rectangle, direction, RGBColor.WHITE, lineWidth);
	}

	public OutlineRectangleDrawable(PositionalRectangle rectangle,
			float direction, RGBColor color, int lineWidth) {
		super(rectangle, direction, Point.ZERO, color);

		this.lineWidth = lineWidth;
	}

	@Override
	protected void drawContent() {
		GL11.glDisable(GL11.GL_TEXTURE_2D);

		GL11.glBegin(GL11.GL_LINE_LOOP);

		GL11.glLineWidth(lineWidth);

		GL11.glVertex2f(0, 0);
		GL11.glVertex2f(getWidth(), 0);
		GL11.glVertex2f(getWidth(), getHeight());
		GL11.glVertex2f(0, getHeight());

		GL11.glEnd();
	}
}
