package fi.peterlehto.pgeng.canvas.drawable;

public interface HasDirection extends Drawable {
    float getDirection();
}
