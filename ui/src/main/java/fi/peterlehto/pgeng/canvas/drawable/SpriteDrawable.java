package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.Rectangle;
import fi.peterlehto.pgeng.display.Texture;

public class SpriteDrawable extends RectangleDrawable {
	private final Texture texture;

	public SpriteDrawable(PositionalRectangle rectangle, Texture texture) {
		this(rectangle, 0, texture, RGBColor.WHITE);
	}

	public SpriteDrawable(PositionalRectangle rectangle, float direction,
			Texture texture, RGBColor color) {
		this(rectangle, direction, Point.valueOf(rectangle.getWidth() / 2,
				rectangle.getHeight() / 2), texture, color);
	}

	public SpriteDrawable(PositionalRectangle rectangle, float direction,
			Point pivot, Texture texture, RGBColor color) {
		this(rectangle, direction, pivot, texture, Rectangle.valueOf(
				texture.getWidth(), texture.getHeight()), color);
	}

	public SpriteDrawable(PositionalRectangle centerPoint, float direction,
			Point pivot, Texture texture, Rectangle textureSize, RGBColor color) {
		super(centerPoint, direction, pivot, color);
		this.texture = texture;
	}

	@Override
	protected void drawContent() {
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureId());
		super.drawContent();
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}
}
