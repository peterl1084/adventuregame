package fi.peterlehto.pgeng.canvas.drawable;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.pgeng.display.PGESpritePool;
import fi.peterlehto.pgeng.display.Texture;

public class TextDrawable extends AbstractPositionalDrawable {
	private Integer maxWidth;
	private int lineHeight;

	private String[] words;
	private final Map<String, Integer> wordWidths = new HashMap<>();

	private final static int SPACING_BETWEEN_CHARACTER = 10;

	public TextDrawable(String text, PositionalRectangle centerPoint,
			RGBColor color) {
		super(centerPoint, color);
		setText(text);
	}

	public TextDrawable(String text, PositionalRectangle centerPoint,
			RGBColor color, int maxWidth) {
		this(text, centerPoint, color);
		setMaxWidth(maxWidth);
	}

	@Override
	protected void drawContent() {
		GL11.glEnable(GL11.GL_TEXTURE_2D);

		int position = 0;

		for (String word : words) {
			int wordWidth = wordWidths.get(word);

			position += wordWidth;

			if (hasMaxWidth() && position >= maxWidth
					&& (position - wordWidth != 0)) {
				GL11.glTranslatef(-position + wordWidth, lineHeight, 0);
				position = wordWidth;
			}

			for (Character character : word.toCharArray()) {
				Texture sprite = PGESpritePool.getInstance().getSprite(
						String.valueOf(character));
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, sprite.getTextureId());
				drawCharacter(sprite);
				GL11.glTranslatef(sprite.getWidth(), 0, 0);
			}

			GL11.glTranslatef(SPACING_BETWEEN_CHARACTER, 0, 0);
			position += SPACING_BETWEEN_CHARACTER;
		}

		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}

	private void drawCharacter(Texture sprite) {
		GL11.glBegin(GL11.GL_QUADS);
		{
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(0, 0);

			GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f(sprite.getWidth(), 0);

			GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f(sprite.getWidth(), sprite.getHeight());

			GL11.glTexCoord2f(0, 1);
			GL11.glVertex2f(0, sprite.getHeight());
		}

		GL11.glEnd();
	}

	public int getWidth() {
		return hasMaxWidth() ? maxWidth : getUnwrappedTextWidth();
	}

	public int getHeight() {
		return hasMaxWidth() ? getTextHeight() : lineHeight;
	}

	public boolean hasMaxWidth() {
		return maxWidth != null && maxWidth > 0;
	}

	public void setMaxWidth(Integer maxWidth) {
		if (maxWidth < 1) {
			throw new IllegalArgumentException("Max width cannot be negative");
		}

		this.maxWidth = maxWidth;
	}

	private int getUnwrappedTextWidth() {
		int totalWidth = 0;

		for (String word : words) {
			totalWidth += wordWidths.get(word);
			totalWidth += SPACING_BETWEEN_CHARACTER;
		}

		return totalWidth;
	}

	private int getTextHeight() {
		int numberOfLines = 1;
		int position = 0;

		for (String word : words) {
			int wordWidth = wordWidths.get(word);

			position += wordWidth;

			if (hasMaxWidth() && position >= maxWidth
					&& (position - wordWidth != 0)) {
				position = wordWidth;
				numberOfLines += 1;
			}

			position += SPACING_BETWEEN_CHARACTER;
		}

		return numberOfLines * lineHeight;
	}

	public void setText(String text) {
		if (text == null)
			text = "";
		words = text.split("\\s+");
		wordWidths.clear();

		int lineHeight = 0;

		for (String word : words) {
			int wordWidth = 0;

			for (Character character : word.toCharArray()) {
				Texture sprite = PGESpritePool.getInstance().getSprite(
						String.valueOf(character));
				wordWidth += sprite.getWidth();

				if (lineHeight < sprite.getHeight()) {
					lineHeight = sprite.getHeight();
				}
			}

			wordWidths.put(word, wordWidth);
		}

		this.lineHeight = lineHeight;
	}
}
