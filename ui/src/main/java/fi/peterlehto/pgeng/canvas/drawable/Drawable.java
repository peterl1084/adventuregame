package fi.peterlehto.pgeng.canvas.drawable;

public interface Drawable extends HasColor {
    void draw();

    int getZIndex();
}
