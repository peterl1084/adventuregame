package fi.peterlehto.pgeng.canvas.drawable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;

public abstract class AbstractContainerDrawable extends
		AbstractPositionalDrawable implements HasChildren {
	private final List<Drawable> drawables;

	public AbstractContainerDrawable(PositionalRectangle representingRectangle) {
		this(representingRectangle, RGBColor.WHITE);
	}

	public AbstractContainerDrawable(PositionalRectangle representingRectangle,
			RGBColor color) {
		super(representingRectangle, color);

		drawables = new LinkedList<>();
	}

	@Override
	public void draw() {
		GL11.glPushMatrix();

		applyColor();

		translatePosition();

		drawContent();
		drawChildren();

		GL11.glPopMatrix();
	}

	@Override
	public void addDrawable(AbstractPositionalDrawable drawable,
			Point centerOffset) {
		drawable.setCenterPoint(centerOffset);
		drawables.add(drawable);
	}

	protected void drawChildren() {
		for (Drawable drawable : getChildren()) {
			GL11.glPushMatrix();
			drawable.draw();
			GL11.glPopMatrix();
		}
	}

	protected Collection<Drawable> getChildren() {
		return drawables;
	}
}
