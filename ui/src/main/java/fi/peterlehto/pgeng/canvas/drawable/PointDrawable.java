package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.Rectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;

public class PointDrawable extends AbstractPositionalDrawable {

	private final int pointSize;

	public PointDrawable(Point centerPoint, int pointSize) {
		this(centerPoint, pointSize, RGBColor.WHITE);
	}

	public PointDrawable(Point centerPoint, int pointSize, RGBColor color) {
		super(new PositionalRectangle(centerPoint, Rectangle.valueOf(1, 1),
				Align.CENTER_POINT), color);

		this.pointSize = pointSize;
	}

	@Override
	protected void drawContent() {
		GL11.glPointSize(pointSize);
		GL11.glBegin(GL11.GL_POINTS);

		// Draw to 0;0 because super class takes care of position translation.
		GL11.glVertex2i(0, 0);

		GL11.glEnd();
	}
}
