package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;

public abstract class AbstractPositionalDrawable extends AbstractDrawable {
	private PositionalRectangle representingRectangle;

	public AbstractPositionalDrawable(PositionalRectangle centerPoint) {
		this(centerPoint, RGBAColor.WHITE);
	}

	public AbstractPositionalDrawable(PositionalRectangle centerPoint,
			RGBColor color) {
		super(color);

		this.representingRectangle = new PositionalRectangle(centerPoint);
	}

	@Override
	public void draw() {
		GL11.glPushMatrix();

		applyColor();
		translatePosition();
		drawContent();

		GL11.glPopMatrix();
	}

	protected void translatePosition() {
		GL11.glTranslatef(representingRectangle.getTopLeftCorner().getX(),
				representingRectangle.getTopLeftCorner().getY(), 0);
	}

	protected PositionalRectangle getRepresentingRectangle() {
		return representingRectangle;
	}

	public void setCenterPoint(Point centerOffset) {
		representingRectangle = new PositionalRectangle(centerOffset,
				representingRectangle.getWidth(),
				representingRectangle.getHeight(),
				representingRectangle.getAlign());
	}
}
