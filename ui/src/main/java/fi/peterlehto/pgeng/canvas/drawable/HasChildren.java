package fi.peterlehto.pgeng.canvas.drawable;

import fi.peterlehto.common.point.Point;

public interface HasChildren {
	public void addDrawable(AbstractPositionalDrawable drawable,
			Point centerOffset);
}
