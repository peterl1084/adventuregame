package fi.peterlehto.pgeng.canvas;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.pgeng.camera.Camera;
import fi.peterlehto.pgeng.ui.component.AbstractComponent;
import fi.peterlehto.pgeng.ui.component.HasProjections;

public abstract class AbstractCanvas extends AbstractComponent implements
		HasProjections {

	private Camera camera;

	private boolean enabled;

	private final static Camera defaultCamera = new Camera();

	public void enableProjections() {
		if (enabled) {
			throw new IllegalStateException("Projections were already enabled");
		}
		enabled = true;
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();

		GL11.glOrtho(camera.getCenterOrthoLeft(), camera.getCenterOrthoRight(),
				camera.getCenterOrthoBottom(), camera.getCenterOrthoTop(), -1,
				1);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		GL11.glPushMatrix();

		GL11.glTranslatef(-camera.getTopLeftCornerPoint().getX(), -camera
				.getTopLeftCornerPoint().getY(), 0);
	}

	@Override
	public void disableProjections() {
		if (!enabled) {
			throw new IllegalStateException(
					"Projections were enabled and attempt to disable");
		}

		GL11.glTranslatef(camera.getTopLeftCornerPoint().getX(), camera
				.getTopLeftCornerPoint().getY(), 0);

		GL11.glPopMatrix();

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPopMatrix();

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		enabled = false;
	}

	public void apply(Camera camera) {
		this.camera = camera == null ? defaultCamera : camera;
	}
}
