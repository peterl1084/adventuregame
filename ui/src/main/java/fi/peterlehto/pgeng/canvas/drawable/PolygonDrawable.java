package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;

public class PolygonDrawable extends AbstractDrawable {

	private final Point[] vertices;

	public PolygonDrawable(Point[] vertices) {
		this(vertices, RGBColor.WHITE);
	}

	public PolygonDrawable(Point[] vertices, RGBColor color) {
		super(color);

		this.vertices = vertices;
	}

	@Override
	protected void drawContent() {
		boolean textureEnabled = GL11.glIsEnabled(GL11.GL_TEXTURE_2D);

		GL11.glDisable(GL11.GL_TEXTURE_2D);

		GL11.glBegin(GL11.GL_LINE_STRIP);

		for (Point point : vertices) {
			GL11.glVertex2f(point.getX(), point.getY());
		}

		GL11.glEnd();

		if (textureEnabled) {
			GL11.glEnable(GL11.GL_TEXTURE_2D);
		}
	}
}
