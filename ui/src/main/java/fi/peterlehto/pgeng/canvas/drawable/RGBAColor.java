package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

/**
 * RGBAColor stores 3 color components and alpha component
 * 
 * @author Peter Lehto
 * @version 17.7.2008
 * 
 */

public class RGBAColor extends RGBColor {

    public static RGBAColor RED = RGBAColor.valueOf(255, 0, 0, 255);
    public static RGBAColor GREEN = RGBAColor.valueOf(0, 255, 0, 255);
    public static RGBAColor BLUE = RGBAColor.valueOf(0, 0, 255, 255);
    public static RGBAColor WHITE = RGBAColor.valueOf(255, 255, 255, 255);
    public static RGBAColor BLACK = RGBAColor.valueOf(0, 0, 0, 255);

    private final int a;

    public RGBAColor(RGBColor color, int alpha) {
        this(color.getR(), color.getG(), color.getB(), alpha);
    }

    public RGBAColor(RGBAColor color) {
        this(color.getR(), color.getG(), color.getB(), color.getA());
    }

    public RGBAColor(int r, int g, int b, int a) {
        super(r, g, b);

        this.a = a;
    }

    public int getA() {
        return a;
    }

    public float getFloatA() {
        return a / 255f;
    }

    @Override
    public String toString() {
        return getFloatR() + " " + getFloatG() + " " + getFloatB() + " " + getFloatA();
    }

    @Override
    public void applyColor() {
        GL11.glColor4f(getFloatR(), getFloatG(), getFloatB(), getFloatA());
    }

    public static RGBAColor valueOf(int r, int g, int b, int a) {
        return new RGBAColor(r, g, b, a);
    }
}
