package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;

public abstract class AbstractDirectedDrawable extends
		AbstractContainerDrawable implements HasDirection {
	private final Point pivot;

	private final float direction;

	public AbstractDirectedDrawable(PositionalRectangle centerPoint,
			float direction) {
		this(centerPoint, direction, RGBAColor.WHITE);
	}

	public AbstractDirectedDrawable(PositionalRectangle centerPoint,
			float direction, RGBColor color) {
		this(centerPoint, direction, Point.ZERO, color);
	}

	public AbstractDirectedDrawable(PositionalRectangle centerPoint,
			float direction, Point pivot) {
		this(centerPoint, direction, pivot, RGBAColor.WHITE);
	}

	public AbstractDirectedDrawable(PositionalRectangle centerPoint,
			float direction, Point pivot, RGBColor color) {
		super(centerPoint, color);

		this.direction = direction;
		this.pivot = pivot;
	}

	@Override
	public void draw() {
		GL11.glPushMatrix();

		applyColor();

		translatePosition();
		translateRotation();

		drawContent();

		drawChildren();

		GL11.glPopMatrix();
	}

	protected void translateRotation() {
		GL11.glTranslatef(pivot.getX(), pivot.getY(), 0);
		GL11.glRotatef(direction, 0, 0, 1);
		GL11.glTranslatef(-pivot.getX(), -pivot.getY(), 0);
	}

	@Override
	public float getDirection() {
		return direction;
	}
}
