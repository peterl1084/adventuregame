package fi.peterlehto.pgeng.canvas.drawable;

public interface HasColor {
    RGBColor getColor();

    void setColor(RGBColor color);
}
