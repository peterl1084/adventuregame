package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

/**
 * RGBAColor stores 3 color components and alpha component
 * 
 * @author Peter Lehto
 * @version 17.7.2008
 * 
 */

public class RGBColor {
    public static RGBColor RED = RGBColor.valueOf(255, 0, 0);
    public static RGBColor GREEN = RGBColor.valueOf(0, 255, 0);
    public static RGBColor BLUE = RGBColor.valueOf(0, 0, 255);
    public static RGBColor WHITE = RGBColor.valueOf(255, 255, 255);
    public static RGBColor BLACK = RGBColor.valueOf(0, 0, 0);

    private final int r;
    private final int g;
    private final int b;

    public RGBColor(RGBColor color) {
        this(color.getR(), color.getG(), color.getB());
    }

    public RGBColor(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int getR() {
        return r;
    }

    public float getFloatR() {
        return r / 255f;
    }

    public int getG() {
        return g;
    }

    public float getFloatG() {
        return g / 255f;
    }

    public int getB() {
        return b;
    }

    public float getFloatB() {
        return b / 255f;
    }

    public void applyColor() {
        GL11.glColor3f(getFloatR(), getFloatG(), getFloatB());
    }

    @Override
    public String toString() {
        return getFloatR() + " " + getFloatG() + " " + getFloatB();
    }

    public static RGBColor valueOf(int r, int g, int b) {
        return new RGBColor(r, g, b);
    }
}
