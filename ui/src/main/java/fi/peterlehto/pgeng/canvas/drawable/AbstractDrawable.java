package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

public abstract class AbstractDrawable implements Drawable {
    private RGBColor color;

    private int zIndex;

    public AbstractDrawable() {
        this(RGBColor.WHITE);
    }

    public AbstractDrawable(RGBColor color) {
        this.color = color;

        if (color == null) {
            throw new IllegalArgumentException("Color cannot be null");
        }
    }

    protected abstract void drawContent();

    @Override
    public void draw() {
        GL11.glPushMatrix();
        applyColor();
        drawContent();
        GL11.glPopMatrix();
    }

    @Override
    public RGBColor getColor() {
        return color;
    }

    @Override
    public void setColor(RGBColor color) {
        this.color = color;
    }

    protected void applyColor() {
        if (color != null) {
            color.applyColor();
        }
    }

    @Override
    public int getZIndex() {
        return zIndex;
    }

    public void setZIndex(int zIndex) {
        this.zIndex = zIndex;
    }
}
