package fi.peterlehto.pgeng.canvas.drawable;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;

public class RectangleDrawable extends AbstractDirectedDrawable {

	public RectangleDrawable(PositionalRectangle rectangle) {
		this(rectangle, RGBColor.WHITE);
	}

	public RectangleDrawable(PositionalRectangle rectangle, RGBColor color) {
		this(rectangle, 0, color);
	}

	public RectangleDrawable(PositionalRectangle rectangle, float direction) {
		this(rectangle, direction, Point.valueOf(rectangle.getWidth() / 2,
				rectangle.getHeight() / 2));
	}

	public RectangleDrawable(PositionalRectangle rectangle, float direction,
			RGBColor color) {
		this(rectangle, direction, Point.valueOf(rectangle.getWidth() / 2,
				rectangle.getHeight() / 2), color);
	}

	public RectangleDrawable(PositionalRectangle rectangle, float direction,
			Point pivot) {
		this(rectangle, direction, pivot, RGBColor.WHITE);
	}

	public RectangleDrawable(PositionalRectangle rectangle, float direction,
			Point pivot, RGBColor color) {
		super(rectangle, direction, pivot, color);
	}

	@Override
	protected void drawContent() {
		GL11.glBegin(GL11.GL_QUADS);
		{
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(0, 0);

			GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f(getWidth(), 0);

			GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f(getWidth(), getHeight());

			GL11.glTexCoord2f(0, 1);
			GL11.glVertex2f(0, getHeight());
		}

		GL11.glEnd();
	}

	protected int getWidth() {
		return getRepresentingRectangle().getWidth();
	}

	protected int getHeight() {
		return getRepresentingRectangle().getHeight();
	}
}
