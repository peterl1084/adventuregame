package fi.peterlehto.pgeng;

import java.util.HashSet;
import java.util.Set;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.pgeng.display.DisplayProvider;
import fi.peterlehto.pgeng.ui.component.AbstractSingleComponentContainer;
import fi.peterlehto.pgeng.ui.component.Component;
import fi.peterlehto.pgeng.ui.component.HasComponents;

public abstract class UI extends AbstractSingleComponentContainer {
	private UIControl control;

	public UI() {
		control = new UIControl(this);
	}

	/**
	 * @param x
	 * @param y
	 * @return topmost component (component that has highest nesting level)
	 *         starting from root component that is in given coordinates. Null
	 *         if there are no components in given location.
	 */
	public Component getTopmostComponentAt(int x, int y) {
		Set<Component> components = getComponentsAt(this, x, y);

		Component highestNestingLevel = null;
		for (Component component : components) {
			if (highestNestingLevel == null
					|| (highestNestingLevel != null && component
							.getNestingLevel() > highestNestingLevel
							.getNestingLevel())) {
				highestNestingLevel = component;
			}
		}

		return highestNestingLevel;
	}

	/**
	 * @param componentContainer
	 * @param x
	 * @param y
	 * @return Will return all nested components in given container which occupy
	 *         given location.
	 */
	private Set<Component> getComponentsAt(HasComponents componentContainer,
			int x, int y) {
		Set<Component> components = new HashSet<>();

		if (componentContainer.getAreaAsRectangle().containsPoint(
				Point.valueOf(x, y))) {
			components.add(componentContainer);
		}

		for (Component child : componentContainer) {
			if (child.getAreaAsRectangle().containsPoint(Point.valueOf(x, y))) {
				components.add(child);

				if (child instanceof HasComponents) {
					components.addAll(getComponentsAt((HasComponents) child, x,
							y));
				}
			}
		}

		return components;
	}

	@Override
	protected int calculateComponentWidth() {
		return DisplayProvider.getInstance().provideDisplay().getWidth();
	}

	@Override
	protected int calculateComponentHeight() {
		return DisplayProvider.getInstance().provideDisplay().getHeight();
	}
}
