package fi.peterlehto.pgeng.font;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;

public class FontImageBuilder {
	private static ComponentColorModel rgbaColorModel = new ComponentColorModel(
			ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 8, 8, 8, 8 }, true, false,
			ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);

	public static BufferedImage getCharacterImage(Font font, char c)
			throws IOException {
		String characterString = String.valueOf(c);

		int width = getCharacterWidth(font, c);
		int height = getHeightOfTallestCharacter(font);

		WritableRaster raster = Raster.createInterleavedRaster(
				DataBuffer.TYPE_BYTE, width, height, 4, null);
		BufferedImage texture = new BufferedImage(rgbaColorModel, raster,
				false, null);

		Graphics2D graphics = texture.createGraphics();
		graphics.setColor(Color.WHITE);
		graphics.setFont(font);
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.drawString(characterString, 0, height - getBaselineY(font));
		graphics.dispose();

		return texture;
	}

	private static int getHeightOfTallestCharacter(Font font) {
		BufferedImage image = new BufferedImage(1, 1,
				BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D graphics = image.createGraphics();
		graphics.setFont(font);
		FontMetrics fontMetrics = graphics.getFontMetrics();

		graphics.dispose();
		return fontMetrics.getHeight();
	}

	private static int getBaselineY(Font font) {
		BufferedImage image = new BufferedImage(1, 1,
				BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D graphics = image.createGraphics();
		graphics.setFont(font);
		FontMetrics fontMetrics = graphics.getFontMetrics();

		graphics.dispose();
		return fontMetrics.getHeight() - fontMetrics.getMaxAscent();
	}

	private static int getCharacterWidth(Font font, char c) {
		BufferedImage image = new BufferedImage(1, 1,
				BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D graphics = image.createGraphics();
		graphics.setFont(font);
		FontMetrics fontMetrics = graphics.getFontMetrics();

		graphics.dispose();
		return fontMetrics.charWidth(c);
	}
}
