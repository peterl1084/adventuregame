package fi.peterlehto.pgeng.font;

public enum FontFamily {

    TIMES_NEW_ROMAN, ARIAL;
}
