package fi.peterlehto.pgeng.display;

/**
 * Texture stores OpenGL texture id and it can be bound to renderer. Texture's
 * width and height are to closest power of two from the original texture
 * (resource) size.
 * 
 * @author Peter
 * @version 20.10.2009
 * 
 */

public class Texture {
    private final int width;
    private final int height;

    private final Integer textureId;

    Texture(int textureId, int width, int height) {
        this.textureId = textureId;

        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getTextureId() {
        return textureId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof Texture) {
            return textureId.equals(((Texture) other).textureId);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.textureId.hashCode();
    }
}
