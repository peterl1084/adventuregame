package fi.peterlehto.pgeng.display;

public class PGEException extends RuntimeException {

    private static final long serialVersionUID = -2185451738248621593L;

    public PGEException(String message) {
        super(message);
    }

    public PGEException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
