package fi.peterlehto.pgeng.display;

import fi.peterlehto.pgeng.ui.component.HasProjections;
import fi.peterlehto.pgeng.ui.component.SingleComponentContainer;

public interface PGEDisplay extends HasProjections, SingleComponentContainer {

	void start();
}
