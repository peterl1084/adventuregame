package fi.peterlehto.pgeng.display;

public class DisplayProvider {

    private static DisplayProvider provider;

    private final PGEDisplay display;

    private DisplayProvider(PGEDisplay display) {
        this.display = display;
    }

    public static void initialize(PGEDisplay display) {
        provider = new DisplayProvider(display);
    }

    public static DisplayProvider getInstance() {
        return provider;
    }

    public PGEDisplay provideDisplay() {
        return display;
    }
}
