package fi.peterlehto.pgeng.display;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import fi.peterlehto.pgeng.font.FontImageBuilder;
import fi.peterlehto.pgeng.input.PGEKeyboardManager;
import fi.peterlehto.pgeng.input.PGEMouseManager;
import fi.peterlehto.pgeng.ui.component.AbstractSingleComponentContainer;

public class DefaultPGEDisplay extends AbstractSingleComponentContainer
		implements PGEDisplay {
	private long lastFrameTime;

	private long fps;
	private long lastFps;
	private int delta;

	private final Set<Character> additionalCharacters = new HashSet<>(
			Arrays.asList('.', ',', ';', ':', '-', '_', '(', ')', '[', ']',
					'{', '}', '!', '?'));

	public DefaultPGEDisplay() {
		PGESpritePool spritePool = PGESpritePool.getInstance();
		Font font = new Font("Arial", Font.PLAIN, 18);

		for (int i = 0; i < 255; i++) {
			char character = (char) i;
			if (Character.isLetterOrDigit(character)
					|| additionalCharacters.contains(character)) {
				try {

					BufferedImage image = FontImageBuilder.getCharacterImage(
							font, character);
					spritePool.loadSprite(String.valueOf(character), image);
				} catch (IOException e) {
					throw new RuntimeException("Failed to initialize font", e);
				}
			}
		}
	}

	@Override
	public void start() {
		fps = getSystemTimeInMilliseconds();
		getTimeSinceLastFrame();
		lastFps = getSystemTimeInMilliseconds();
		Display.sync(60);

		while (!Display.isCloseRequested()) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			delta = getTimeSinceLastFrame();
			updateCaptionInfo();

			render();

			Display.update();

			PGEKeyboardManager.getInstance().update();
			PGEMouseManager.getInstance().update();
		}

		Display.destroy();
		getLogger().info("Exiting game window update loop");
	}

	@Override
	public int getWidth() {
		return Display.getDisplayMode().getWidth();
	}

	@Override
	public int getHeight() {
		return Display.getDisplayMode().getHeight();
	}

	@Override
	public int getX() {
		return 0;
	}

	@Override
	public int getY() {
		return 0;
	}

	@Override
	public void setCaption(String caption) {
		Display.setTitle(caption);
	}

	@Override
	public String getCaption() {
		return Display.getTitle();
	}

	@Override
	public void enableProjections() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();

		GL11.glOrtho(0, getWidth(), getHeight(), 0, -1, 1);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
	}

	@Override
	public void disableProjections() {
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
		GL11.glPopMatrix();
	}

	@Override
	protected int calculateComponentWidth() {
		return getWidth();
	}

	@Override
	protected int calculateComponentHeight() {
		return getHeight();
	}

	private long getSystemTimeInMilliseconds() {
		return System.nanoTime() / 1000000;
	}

	private int getTimeSinceLastFrame() {
		long time = getSystemTimeInMilliseconds();
		int delta = (int) (time - lastFrameTime);
		lastFrameTime = time;

		return delta;
	}

	private void updateCaptionInfo() {
		if (getSystemTimeInMilliseconds() - lastFps > 1000) {
			setCaption("FPS " + fps + " delta " + delta);
			fps = 0;
			lastFps += 1000;
		}

		fps++;
	}

	private static DisplayMode discoverSuitableDisplayModeFor(int width,
			int height, boolean fullscreen) {
		DisplayMode desktopMode = Display.getDesktopDisplayMode();

		int desktopBitsPerPixel = desktopMode.getBitsPerPixel();
		int desktopFrequency = desktopMode.getFrequency();

		try {
			for (DisplayMode availableMode : Display.getAvailableDisplayModes()) {
				if (availableMode.getWidth() == width
						&& availableMode.getHeight() == height) {
					if (availableMode.getBitsPerPixel() == desktopBitsPerPixel
							&& availableMode.getFrequency() == desktopFrequency) {
						getLogger().info("Found display mode " + availableMode);
						return availableMode;
					}
				}
			}
		} catch (LWJGLException e) {
			throw new PGEException("Error getting display modes", e);
		}

		throw new PGEException("Could not find suitable display mode");
	}

	private static Logger getLogger() {
		return Logger.getLogger(DefaultPGEDisplay.class.getSimpleName());
	}

	public static PGEDisplay init(int width, int height, boolean fullscreen)
			throws PGEException {
		getLogger().info(
				"Initializing PGENG window with " + width + " " + height);

		DisplayMode targetMode = discoverSuitableDisplayModeFor(width, height,
				fullscreen);

		try {
			getLogger().info("Setting target display mode " + targetMode);
			Display.setDisplayMode(targetMode);

			try {
				if (!Display.isCreated()) {
					getLogger().info(
							"Creating display as it is not yet created");
					Display.create();

					GL11.glEnable(GL11.GL_LINE_SMOOTH);
					GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);

					GL11.glDisable(GL11.GL_DEPTH_TEST);
					GL11.glDepthFunc(GL11.GL_NEVER);

					GL11.glEnable(GL11.GL_BLEND);
					GL11.glBlendFunc(GL11.GL_SRC_ALPHA,
							GL11.GL_ONE_MINUS_SRC_ALPHA);

					GL11.glShadeModel(GL11.GL_FLAT);

					GL11.glViewport(0, 0, width, height);

					GL11.glMatrixMode(GL11.GL_PROJECTION);

					GL11.glOrtho(0, width, height, 0, 1, -1);
					GL11.glMatrixMode(GL11.GL_MODELVIEW);
					GL11.glLoadIdentity();
				}
			} catch (LWJGLException e) {
				throw new PGEException("Error creating display", e);
			}

			DisplayProvider.initialize(new DefaultPGEDisplay());
			return DisplayProvider.getInstance().provideDisplay();
		} catch (LWJGLException e) {
			throw new PGEException("Error setting display mode", e);
		}
	}

}
