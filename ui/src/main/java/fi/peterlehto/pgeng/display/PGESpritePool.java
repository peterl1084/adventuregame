package fi.peterlehto.pgeng.display;

import java.awt.image.BufferedImage;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import fi.peterlehto.pgeng.PGEBufferUtils;

/**
 * PGESpritePool is a container for sprites.
 * 
 * @author Peter Lehto
 * @version 20.10.2009
 */

public class PGESpritePool {
	private static PGESpritePool spritePool;

	private final Map<String, Texture> sprites;

	/**
	 * Instantiates an empty sprite pool
	 */

	private PGESpritePool() {
		this.sprites = new HashMap<String, Texture>();
	}

	/**
	 * @param identifier
	 * @return Sprite with given identifier
	 * @throws PGEViewException
	 *             if sprite with given identifier cannot be found
	 */
	public Texture getSprite(String identifier) {
		if (sprites.containsKey(identifier)) {
			return sprites.get(identifier);
		}

		throw new RuntimeException("Could not find sprire with identifier "
				+ identifier);
	}

	/**
	 * Loads an image from given resource and stores it as a sprite with given
	 * identifier.
	 * 
	 * @param identifier
	 * @param resource
	 * @return
	 * @throws PGEViewException
	 */
	public Texture loadSprite(String identifier, BufferedImage image) {
		getLogger().fine("Loading sprite '" + identifier + "' from " + image);
		if (this.sprites.containsKey(identifier)) {
			return this.sprites.get(identifier);
		}

		int textureId = generateTextureId();
		getLogger().fine("Generated texture id " + textureId);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_LINEAR);

		int width = image.getWidth();
		int height = image.getHeight();

		Texture texture = new Texture(textureId, width, height);

		int sourceFormat = image.getColorModel().hasAlpha() ? GL11.GL_RGBA
				: GL11.GL_RGB;

		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height,
				0, sourceFormat, GL11.GL_UNSIGNED_BYTE,
				PGEBufferUtils.convertToByteBuffer(image));

		sprites.put(identifier, texture);

		return texture;
	}

	private int generateTextureId() {
		IntBuffer textureIds = BufferUtils.createIntBuffer(1);
		GL11.glGenTextures(textureIds);

		return textureIds.get(0);
	}

	public static PGESpritePool getInstance() {
		if (spritePool == null) {
			spritePool = new PGESpritePool();
		}

		return spritePool;
	}

	private Logger getLogger() {
		return Logger.getLogger(PGESpritePool.class.getSimpleName());
	}
}
