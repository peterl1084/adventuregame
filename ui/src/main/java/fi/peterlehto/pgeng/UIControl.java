package fi.peterlehto.pgeng;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.input.Keyboard;

import fi.peterlehto.pgeng.input.PGEKeyboardManager;
import fi.peterlehto.pgeng.input.PGEKeyboardManager.KeyPressListener;
import fi.peterlehto.pgeng.input.PGEMouseManager;
import fi.peterlehto.pgeng.input.PGEMouseManager.MousePressListener;
import fi.peterlehto.pgeng.input.PGEMouseManager.MouseWheelListener;
import fi.peterlehto.pgeng.ui.component.Component;
import fi.peterlehto.pgeng.ui.component.Focusable;
import fi.peterlehto.pgeng.ui.component.KeyboardFocusable;
import fi.peterlehto.pgeng.ui.component.Scrollable;

public class UIControl implements MousePressListener, KeyPressListener,
		MouseWheelListener {

	private static Map<Integer, Character> characterCodes = new HashMap<>();

	private Focusable focusedComponent;

	private UI ui;

	static {
		characterCodes.put(Keyboard.KEY_0, '0');
		characterCodes.put(Keyboard.KEY_1, '1');
		characterCodes.put(Keyboard.KEY_2, '2');
		characterCodes.put(Keyboard.KEY_3, '3');
		characterCodes.put(Keyboard.KEY_4, '4');
		characterCodes.put(Keyboard.KEY_5, '5');
		characterCodes.put(Keyboard.KEY_6, '6');
		characterCodes.put(Keyboard.KEY_7, '7');
		characterCodes.put(Keyboard.KEY_8, '8');
		characterCodes.put(Keyboard.KEY_9, '9');
		characterCodes.put(Keyboard.KEY_A, 'a');
		characterCodes.put(Keyboard.KEY_B, 'b');
		characterCodes.put(Keyboard.KEY_C, 'c');
		characterCodes.put(Keyboard.KEY_D, 'd');
		characterCodes.put(Keyboard.KEY_E, 'e');
		characterCodes.put(Keyboard.KEY_F, 'f');
		characterCodes.put(Keyboard.KEY_G, 'g');
		characterCodes.put(Keyboard.KEY_H, 'h');
		characterCodes.put(Keyboard.KEY_I, 'i');
		characterCodes.put(Keyboard.KEY_J, 'j');
		characterCodes.put(Keyboard.KEY_K, 'k');
		characterCodes.put(Keyboard.KEY_L, 'l');
		characterCodes.put(Keyboard.KEY_M, 'm');
		characterCodes.put(Keyboard.KEY_N, 'n');
		characterCodes.put(Keyboard.KEY_O, 'o');
		characterCodes.put(Keyboard.KEY_P, 'p');
		characterCodes.put(Keyboard.KEY_Q, 'q');
		characterCodes.put(Keyboard.KEY_R, 'r');
		characterCodes.put(Keyboard.KEY_S, 's');
		characterCodes.put(Keyboard.KEY_T, 't');
		characterCodes.put(Keyboard.KEY_U, 'u');
		characterCodes.put(Keyboard.KEY_V, 'v');
		characterCodes.put(Keyboard.KEY_W, 'w');
		characterCodes.put(Keyboard.KEY_X, 'x');
		characterCodes.put(Keyboard.KEY_Y, 'y');
		characterCodes.put(Keyboard.KEY_Z, 'z');
		characterCodes.put(Keyboard.KEY_COMMA, ',');
		characterCodes.put(Keyboard.KEY_COLON, ':');
		characterCodes.put(Keyboard.KEY_PERIOD, '.');
	}

	public UIControl(UI ui) {
		this.ui = ui;

		PGEMouseManager.getInstance().addButtonPressListener(this);
		PGEMouseManager.getInstance().addWheelListener(this);
		PGEKeyboardManager.getInstance().addKeyPressListener(this);
	}

	@Override
	public void onKeyPressed(int keyCode) {

	}

	@Override
	public void onKeyReleased(int keyCode) {
		if (focusedComponent instanceof KeyboardFocusable) {
			KeyboardFocusable keyboardTarget = (KeyboardFocusable) focusedComponent;
			if (Keyboard.KEY_SPACE == keyCode) {
				keyboardTarget.onSpaceKey();
			} else if (Keyboard.KEY_RETURN == keyCode) {
				keyboardTarget.onEnterKey();
			} else if (Keyboard.KEY_BACK == keyCode) {
				keyboardTarget.onBackspaceKey();
			} else if (characterCodes.containsKey(keyCode)) {
				keyboardTarget.onKeyboardCharacter(characterCodes.get(keyCode));
			}
		}
	}

	@Override
	public void onMouseButtonPressed(int button, int eventX, int eventY) {

	}

	@Override
	public void onMouseButtonReleased(int button, int eventX, int eventY) {
		Component component = ui.getTopmostComponentAt(eventX, eventY);

		if (component == null) {
			if (focusedComponent != null) {
				focusedComponent.onBlur();
			}

			focusedComponent = null;
			return;
		} else {

			if (component instanceof Focusable) {
				if (focusedComponent != null) {
					focusedComponent.onBlur();
				}

				focusedComponent = (Focusable) component;
				focusedComponent.onFocus();
			} else {
				if (focusedComponent != null) {
					focusedComponent.onBlur();
					focusedComponent = null;
				}
			}
		}
	}

	@Override
	public void onMouseWheelScrolled(int wheel) {
		if (focusedComponent != null) {
			if (focusedComponent instanceof Scrollable) {
				Scrollable scrollable = (Scrollable) focusedComponent;
				scrollable.scroll((int) ((wheel / 100f) * 1));
			}
		}
	}
}
