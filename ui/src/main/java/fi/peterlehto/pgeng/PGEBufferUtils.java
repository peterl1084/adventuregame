package fi.peterlehto.pgeng;

import java.awt.Graphics;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class PGEBufferUtils {

    private static final ColorModel rgbaColorModel = new ComponentColorModel(
            ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 8, 8, 8, 8 }, true, false,
            ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);

    private static final ColorModel rgbColorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB),
            new int[] { 8, 8, 8, 0 }, false, false, ComponentColorModel.OPAQUE, DataBuffer.TYPE_BYTE);

    public static ByteBuffer convertToByteBuffer(BufferedImage image) {
        ByteBuffer buffer = null;
        WritableRaster raster = null;
        BufferedImage textureImage = null;

        int width = image.getWidth();
        int height = image.getHeight();

        if (image.getColorModel().hasAlpha()) {
            raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 4, null);
            textureImage = new BufferedImage(rgbaColorModel, raster, false, null);
        } else {
            raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 3, null);
            textureImage = new BufferedImage(rgbColorModel, raster, false, null);
        }

        Graphics g = textureImage.getGraphics();
        g.drawImage(image, 0, 0, null);

        byte[] data = ((DataBufferByte) textureImage.getRaster().getDataBuffer()).getData();
        buffer = ByteBuffer.allocateDirect(data.length);
        buffer.order(ByteOrder.nativeOrder());
        buffer.put(data, 0, data.length);
        buffer.flip();

        return buffer;
    }

    public static FloatBuffer getAsDirectFloatBuffer(float... floats) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(floats.length * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        FloatBuffer buffer = allocateDirect.asFloatBuffer();
        buffer.put(floats);
        buffer.position(0);

        return buffer;
    }
}
