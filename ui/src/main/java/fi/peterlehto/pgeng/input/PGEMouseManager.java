package fi.peterlehto.pgeng.input;

import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class PGEMouseManager {

    private static PGEMouseManager instance;

    private final Set<MousePressListener> buttonPressListeners;
    private final Set<MouseMoveListener> mouseMoveListeners;
    private final Set<MouseWheelListener> mouseWheelListeners;

    private PGEMouseManager() {
        buttonPressListeners = new HashSet<>();
        mouseMoveListeners = new HashSet<>();
        mouseWheelListeners = new HashSet<>();
    }

    public void update() {
        while (Mouse.next()) {
            int eventButton = Mouse.getEventButton();
            int eventX = Mouse.getEventX();
            int eventY = Display.getHeight() - Mouse.getEventY();
            boolean pressed = Mouse.getEventButtonState();
            int wheel = Mouse.getEventDWheel();

            if (eventButton >= 0) {
                if (pressed) {
                    firePressListeners(eventButton, eventX, eventY);
                } else {
                    fireReleaseListeners(eventButton, eventX, eventY);
                }
            } else if (wheel != 0) {
                fireWheelListeners(wheel);
            } else {
                fireMoveListeners(eventX, eventY);
            }
        }
    }

    private void firePressListeners(int eventKey, int eventX, int eventY) {
        for (MousePressListener pressListener : buttonPressListeners) {
            pressListener.onMouseButtonPressed(eventKey, eventX, eventY);
        }
    }

    private void fireReleaseListeners(int eventKey, int eventX, int eventY) {
        for (MousePressListener pressListener : buttonPressListeners) {
            pressListener.onMouseButtonReleased(eventKey, eventX, eventY);
        }
    }

    private void fireMoveListeners(int eventX, int eventY) {
        for (MouseMoveListener moveListener : mouseMoveListeners) {
            moveListener.onMouseMoved(eventX, eventY);
        }
    }

    private void fireWheelListeners(int wheel) {
        for (MouseWheelListener wheelListener : mouseWheelListeners) {
            wheelListener.onMouseWheelScrolled(wheel);
        }
    }

    public void addButtonPressListener(MousePressListener buttonPressListener) {
        buttonPressListeners.add(buttonPressListener);
    }

    public void removeButtonPressListener(MousePressListener buttonPressListener) {
        buttonPressListeners.remove(buttonPressListener);
    }

    public void addMoveListener(MouseMoveListener moveListener) {
        mouseMoveListeners.add(moveListener);
    }

    public void removeMoveListener(MouseMoveListener moveListener) {
        mouseMoveListeners.remove(moveListener);
    }

    public void addWheelListener(MouseWheelListener wheelListener) {
        mouseWheelListeners.add(wheelListener);
    }

    public void removeWheelListener(MouseWheelListener wheelListener) {
        mouseWheelListeners.remove(wheelListener);
    }

    public interface MousePressListener extends EventListener {
        public void onMouseButtonPressed(int button, int eventX, int eventY);

        public void onMouseButtonReleased(int button, int eventX, int eventY);
    }

    public interface MouseMoveListener extends EventListener {
        public void onMouseMoved(int eventX, int eventY);
    }

    public interface MouseWheelListener extends EventListener {
        public void onMouseWheelScrolled(int wheel);
    }

    public static synchronized PGEMouseManager getInstance() {
        if (instance == null) {
            instance = new PGEMouseManager();
        }

        return instance;
    }
}
