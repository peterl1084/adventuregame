package fi.peterlehto.pgeng.input;

import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Keyboard;

public class PGEKeyboardManager {

    private static PGEKeyboardManager instance;
    private final Set<KeyPressListener> keyPressListeners;

    private PGEKeyboardManager() {
        keyPressListeners = new HashSet<>();
    }

    public void update() {
        while (Keyboard.next()) {
            int eventKey = Keyboard.getEventKey();
            boolean pressed = Keyboard.getEventKeyState();

            if (pressed) {
                firePressListeners(eventKey);
            } else {
                fireReleaseListeners(eventKey);
            }
        }
    }

    private void firePressListeners(int eventKey) {
        for (KeyPressListener pressListener : keyPressListeners) {
            pressListener.onKeyPressed(eventKey);
        }
    }

    private void fireReleaseListeners(int eventKey) {
        for (KeyPressListener pressListener : keyPressListeners) {
            pressListener.onKeyReleased(eventKey);
        }
    }

    public void addKeyPressListener(KeyPressListener keyPressListener) {
        keyPressListeners.add(keyPressListener);
    }

    public void removeKeyPressListener(KeyPressListener keyPressListener) {
        keyPressListeners.remove(keyPressListener);
    }

    public interface KeyPressListener extends EventListener {
        public void onKeyPressed(int keyCode);

        public void onKeyReleased(int keyCode);
    }

    public static synchronized PGEKeyboardManager getInstance() {
        if (instance == null) {
            instance = new PGEKeyboardManager();
        }

        return instance;
    }
}
