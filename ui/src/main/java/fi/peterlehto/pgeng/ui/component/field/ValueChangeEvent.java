package fi.peterlehto.pgeng.ui.component.field;

public class ValueChangeEvent {

    private final boolean confirmed;

    private final Field<?, ?> source;

    public ValueChangeEvent(boolean confirmed, Field<?, ?> source) {
        this.confirmed = confirmed;
        this.source = source;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public Field<?, ?> getField() {
        return source;
    }
}
