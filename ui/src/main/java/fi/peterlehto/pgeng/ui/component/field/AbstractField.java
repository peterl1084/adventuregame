package fi.peterlehto.pgeng.ui.component.field;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.peterlehto.pgeng.ui.component.AbstractFocusableComponent;
import fi.peterlehto.pgeng.ui.component.field.validation.Validator;

public abstract class AbstractField<VALUE, VALIDATOR extends Validator<? super VALUE>> extends
        AbstractFocusableComponent implements Field<VALUE, VALIDATOR> {

    private VALUE value;

    private final List<VALIDATOR> validators;

    private final Set<ValueChangeListener> valueChangeListeners;

    public AbstractField() {
        validators = new ArrayList<>();
        valueChangeListeners = new HashSet<>();
    }

    @Override
    public VALUE getValue() {
        return value;
    }

    @Override
    public void setValue(VALUE value) {
        setUnconfirmedValue(value);
    }

    protected void setUnconfirmedValue(VALUE value) {
        if (isValid(value)) {
            this.value = value;
            onValueSet(value);
            fireValueChangeListeners(false);
        } else {
            onInvalidValue(value);
        }
    }

    protected void setConfirmedValue(VALUE value) {
        if (isValid(value)) {
            this.value = value;
            onValueSet(value);
            fireValueChangeListeners(true);
        } else {
            onInvalidValue(value);
        }
    }

    protected boolean isValid(VALUE value) {
        for (VALIDATOR validator : validators) {
            if (!validator.validate(value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Invoked when validation of setValue fails.
     * 
     * @param value
     *            value that was attempted to be inputed for this Field.
     */
    protected abstract void onInvalidValue(VALUE value);

    protected abstract void onValueSet(VALUE value);

    private void fireValueChangeListeners(boolean confirmedValue) {
        for (ValueChangeListener listener : valueChangeListeners) {
            listener.onValueChanged(createValueChangeEvent(confirmedValue));
        }
    }

    protected ValueChangeEvent createValueChangeEvent(boolean confirmedValue) {
        return new ValueChangeEvent(confirmedValue, this);
    }

    @Override
    public void addValidator(VALIDATOR validator) {
        this.validators.add(validator);
    }

    @Override
    public void removeValidator(VALIDATOR validator) {
        this.validators.remove(validator);
    }

    @Override
    public void addValueChangeListener(ValueChangeListener valueChangeListener) {
        valueChangeListeners.add(valueChangeListener);
    }

    @Override
    public void removeValueChangeListener(ValueChangeListener valueChangeListener) {
        valueChangeListeners.remove(valueChangeListener);
    }
}
