package fi.peterlehto.pgeng.ui.component;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.canvas.drawable.RectangleDrawable;
import fi.peterlehto.pgeng.display.DisplayProvider;

/**
 * AbstractComponent is abstract and most general implementation of Component
 * interface.
 * 
 * @author Peter
 */
public abstract class AbstractComponent implements Component {
	private HasComponents parent;

	private Integer width;
	private Integer height;

	private boolean fullWidth;
	private boolean fullHeight;

	private String id;
	private RGBColor backgroundColor;

	private String caption;

	@Override
	public void attach(HasComponents parent) {
		this.parent = parent;
	}

	@Override
	public Component getParent() {
		return parent;
	}

	@Override
	public void render() {
		clipScissorBounds();

		if (this instanceof HasProjections) {
			((HasProjections) this).enableProjections();
		}

		if (getBackgroundColor() != null) {
			RectangleDrawable backgroundRectangle = new RectangleDrawable(
					new PositionalRectangle(0, 0, getWidth(), getHeight(),
							Align.TOP_LEFT_POINT), getBackgroundColor());
			backgroundRectangle.draw();
		}

		renderContent();

		if (this instanceof HasProjections) {
			((HasProjections) this).disableProjections();
		}
	}

	/**
	 * Positions OpenGL clipping rectangle on top of this component preventing
	 * the rendering to draw content outside component borders.
	 */
	protected void clipScissorBounds() {
		int scissorX = getX();
		int scissorY = DisplayProvider.getInstance().provideDisplay()
				.getHeight()
				- (getHeight() + getY());
		int scissorWidth = getWidth();
		int scissorHeight = getHeight();

		GL11.glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
	}

	protected abstract void renderContent();

	@Override
	public void setWidth(Integer width) {
		this.width = width;
		this.fullWidth = false;
	}

	@Override
	public boolean hasFixedWidth() {
		return width != null;
	}

	@Override
	public void setFullWidth() {
		this.fullWidth = true;
		this.width = null;
	}

	@Override
	public boolean hasFullWidth() {
		return fullWidth;
	}

	protected Integer getFixedWidth() {
		return width;
	}

	@Override
	public void setHeight(Integer height) {
		this.height = height;
		this.fullHeight = false;
	}

	@Override
	public boolean hasFixedHeight() {
		return height != null;
	}

	@Override
	public void setFullHeight() {
		this.fullHeight = true;
	}

	protected Integer getFixedHeight() {
		return height;
	}

	@Override
	public boolean hasFullHeight() {
		return fullHeight;
	}

	@Override
	public int getWidth() {
		int width = 0;

		int parentAvailableWidth = parent.getAvailableSpaceFor(this).getWidth();

		if (hasFullWidth())
			width = parentAvailableWidth;
		else if (hasFixedWidth())
			width = getFixedWidth();
		else
			width = calculateComponentWidth();

		return Math.min(parentAvailableWidth, width);
	}

	@Override
	public int getHeight() {
		int height = 0;

		int parentAvailableHeight = parent.getAvailableSpaceFor(this)
				.getHeight();

		if (hasFullHeight())
			height = parentAvailableHeight;
		else if (hasFixedHeight())
			height = getFixedHeight();
		else
			height = calculateComponentHeight();

		return Math.min(parentAvailableHeight, height);
	}

	@Override
	public PositionalRectangle getAreaAsRectangle() {
		return new PositionalRectangle(getX(), getY(), getWidth(), getHeight(),
				Align.TOP_LEFT_POINT);
	}

	@Override
	public int getX() {
		return parent.getAvailableSpaceFor(this).getTopLeftCorner().getX();
	}

	@Override
	public int getY() {
		return parent.getAvailableSpaceFor(this).getTopLeftCorner().getY();
	}

	@Override
	public void setBackgroundColor(RGBColor backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	protected RGBColor getBackgroundColor() {
		return backgroundColor;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int getNestingLevel() {
		int nestingLevel = 0;
		Component component = this;

		while ((component = component.getParent()) != null)
			nestingLevel += 1;

		return nestingLevel;
	}

	@Override
	public String getCaption() {
		return caption;
	}

	@Override
	public void setCaption(String caption) {
		this.caption = caption;
	}

	// @Override
	// public String toString() {
	// StringBuilder builder = new StringBuilder();
	// builder.append(getClass().getSimpleName());
	// builder.append(" ");
	// builder.append(getId());
	// builder.append("lvl ");
	// builder.append(getNestingLevel());
	// builder.append(" ");
	// builder.append(getX());
	// builder.append(" ");
	// builder.append(getY());
	// builder.append(" ");
	// builder.append(getWidth());
	// builder.append(" ");
	// builder.append(getHeight());
	//
	// return builder.toString();
	// }

	/**
	 * @return total width of this component calculated by it's content's width.
	 */
	protected abstract int calculateComponentWidth();

	/**
	 * @return total height of this component calculated by it's content's
	 *         height.
	 */
	protected abstract int calculateComponentHeight();
}
