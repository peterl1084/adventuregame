package fi.peterlehto.pgeng.ui.component;

public interface ComponentContainer extends HasComponents {
	void addComponent(Component component);

	void addComponents(Component... components);

	void removeComponent(Component component);
}
