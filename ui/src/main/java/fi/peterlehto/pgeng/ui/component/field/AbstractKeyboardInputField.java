package fi.peterlehto.pgeng.ui.component.field;

import fi.peterlehto.pgeng.ui.component.KeyboardFocusable;
import fi.peterlehto.pgeng.ui.component.field.validation.StringValidator;

public abstract class AbstractKeyboardInputField extends AbstractField<String, StringValidator> implements
        KeyboardFocusable {

    @Override
    public void onKeyboardCharacter(char c) {
        appendToValue(c);
    }

    @Override
    public void onBackspaceKey() {
        StringBuilder valueBuilder = new StringBuilder();
        valueBuilder.append(getValue());

        if (valueBuilder.length() == 0) {
            return;
        }

        valueBuilder.deleteCharAt(valueBuilder.length() - 1);
        setValue(valueBuilder.toString());
    }

    @Override
    public void onSpaceKey() {
        appendToValue(' ');
    }

    @Override
    public void onEnterKey() {
        setConfirmedValue(getValue());
    }

    private void appendToValue(char c) {
        StringBuilder valueBuilder = new StringBuilder();
        valueBuilder.append(getValue() == null ? "" : getValue());
        valueBuilder.append(c);
        setValue(valueBuilder.toString());
    }
}
