package fi.peterlehto.pgeng.ui.component;

import fi.peterlehto.pgeng.canvas.drawable.TextDrawable;

public class Label extends AbstractComponent {

    private TextDrawable drawable;

    public Label(String text) {
        // drawable = new TextDrawable(text, Point.ZERO, RGBAColor.WHITE);
    }

    @Override
    public void renderContent() {
        drawable.draw();
    }

    @Override
    public void setWidth(Integer width) {
        super.setWidth(width);
        drawable.setMaxWidth(width);
    }

    @Override
    protected int calculateComponentWidth() {
        return drawable.getWidth();
    }

    @Override
    protected int calculateComponentHeight() {
        return drawable.getHeight();
    }
}
