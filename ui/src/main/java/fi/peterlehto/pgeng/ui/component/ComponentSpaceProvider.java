package fi.peterlehto.pgeng.ui.component;

import fi.peterlehto.common.point.PositionalRectangle;

public interface ComponentSpaceProvider {
	PositionalRectangle getAvailableSpaceFor(Component component);
}
