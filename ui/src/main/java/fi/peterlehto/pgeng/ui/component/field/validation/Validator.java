package fi.peterlehto.pgeng.ui.component.field.validation;

public interface Validator<VALUE> {
    boolean validate(VALUE value);
}
