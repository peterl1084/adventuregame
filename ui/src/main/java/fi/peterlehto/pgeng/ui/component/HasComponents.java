package fi.peterlehto.pgeng.ui.component;

public interface HasComponents extends Component, Iterable<Component>,
		ComponentSpaceProvider {

}
