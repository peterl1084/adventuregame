package fi.peterlehto.pgeng.ui.component.field;

import java.util.EventListener;

public interface ValueChangeListener extends EventListener {

    public void onValueChanged(ValueChangeEvent event);
}
