package fi.peterlehto.pgeng.ui.component.field.validation;


public interface Validatable<VALUE, VALIDATOR extends Validator<? super VALUE>> {
    void addValidator(VALIDATOR validator);

    void removeValidator(VALIDATOR validator);
}
