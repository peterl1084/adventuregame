package fi.peterlehto.pgeng.ui.component;

import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;

/**
 * Component is the super interface of all components drawable to
 * ComponentCanvas.
 * 
 * @author Peter
 */
public interface Component {

	/**
	 * Renders the component
	 */
	void render();

	/**
	 * Attaches component to the component container
	 * 
	 * @param parent
	 * @param reservedSpace
	 *            space that is reserved for this component
	 */
	void attach(HasComponents parent);

	/**
	 * @return parent component or null if component is hierarchy root
	 */
	Component getParent();

	/**
	 * Sets the background color of the component. If color is set the area of
	 * the component will be colored with given color. Null value disables
	 * background coloring.
	 * 
	 * @param color
	 */
	void setBackgroundColor(RGBColor color);

	/**
	 * Sets full width and height for this component
	 */
	default void setSizeFull() {
		setFullWidth();
		setFullHeight();
	}

	/**
	 * Sets given fixed width for the component. Setting null will set the width
	 * to be calculated by component content.
	 * 
	 * @param width
	 */
	void setWidth(Integer width);

	/**
	 * Sets component to full width mode which will take all the available space
	 * assigned by the parent.
	 */
	void setFullWidth();

	/**
	 * @return true if this component should expand to full width of the space
	 *         reserved for it.
	 */
	boolean hasFullWidth();

	default boolean hasUndefinedWidth() {
		return !hasFixedWidth() && !hasFullWidth();
	}

	/**
	 * @return true if a fixed width has been defined for this component.
	 */
	boolean hasFixedWidth();

	/**
	 * Sets fixed height of the component. If null is passed the height of the
	 * component is determined by its content.
	 * 
	 * @param height
	 */
	void setHeight(Integer height);

	/**
	 * Sets component to full height mode which will take all the available
	 * space assigned by parent.
	 */
	void setFullHeight();

	/**
	 * @return true if this component should expand to full height of the space
	 *         reserved for it.
	 */
	boolean hasFullHeight();

	/**
	 * @return true if a fixed height has been defined for this component.
	 */
	boolean hasFixedHeight();

	default boolean hasUndefinedHeight() {
		return !hasFixedHeight() && !hasFullHeight();
	}

	/**
	 * @return current calculated width of the component.
	 */
	int getWidth();

	/**
	 * @return current calculated height of the component.
	 */
	int getHeight();

	/**
	 * @return current x position of the component in screen coordinates.
	 */
	int getX();

	/**
	 * @return current y position of the component in screen coordinates.
	 */
	int getY();

	/**
	 * @return rectangle that represent the area occupied by the component.
	 *         Rectangle's top left corner is positioned to component's x and y
	 *         coordinates.
	 */
	PositionalRectangle getAreaAsRectangle();

	/**
	 * Sets textual id for the component
	 * 
	 * @param id
	 */
	void setId(String id);

	/**
	 * @return gets component's textual id
	 */
	String getId();

	/**
	 * @return nesting level of this component. The bigger the number the deeper
	 *         the component is in the component hierarchy. Root components have
	 *         nesting level 0.
	 */
	int getNestingLevel();

	void setCaption(String caption);

	String getCaption();
}
