package fi.peterlehto.pgeng.ui.component.field;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.common.point.Rectangle;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.canvas.drawable.TextDrawable;

public class TextField extends AbstractKeyboardInputField {
	private TextDrawable drawable;
	private int minHeight;

	public TextField() {
		drawable = new TextDrawable("", new PositionalRectangle(Point.valueOf(
				2, 0), Rectangle.valueOf(0, 0), Align.CENTER_POINT),
				RGBColor.WHITE);

		setWidth(150);

		setValue("text");
		minHeight = drawable.getHeight();
		setValue(null);
	}

	public TextField(String value) {
		this();
		setValue(value);
	}

	@Override
	public void renderContent() {
		super.renderContent();

		drawable.setMaxWidth(getWidth());
		drawable.draw();
	}

	@Override
	public void setWidth(Integer width) {
		super.setWidth(width);
		drawable.setMaxWidth(width);
	}

	@Override
	protected int calculateComponentWidth() {
		return drawable.getWidth();
	}

	@Override
	protected int calculateComponentHeight() {
		return Math.max(minHeight, drawable.getHeight());
	}

	@Override
	protected void onInvalidValue(String value) {
		drawable.setColor(RGBColor.RED);
	}

	@Override
	protected void onValueSet(String value) {
		drawable.setColor(RGBColor.WHITE);
		drawable.setText(value);
	}
}
