package fi.peterlehto.pgeng.ui.component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.StreamSupport;

import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.common.point.Rectangle;

public class HorizontalLayout extends AbstractOrderedComponentContainer {

	public HorizontalLayout() {
		this(0, 0);
	}

	public HorizontalLayout(int spacingPixels, int marginPixels) {
		super(spacingPixels, marginPixels);
	}

	@Override
	protected int calculateComponentHeight() {
		int highestComponent = StreamSupport.stream(spliterator(), false)
				.mapToInt(c -> c.getHeight()).max().getAsInt();
		return highestComponent + (getMarginPixels() * 2);
	}

	@Override
	protected int calculateComponentWidth() {
		return StreamSupport.stream(spliterator(), false)
				.mapToInt(c -> c.getWidth() + getSpacingPixels()).sum()
				+ (getMarginPixels() * 2) - getSpacingPixels();
	}

	@Override
	protected int calculateXPositionFor(Component component) {
		int componentIndex = indexOf(component);

		if (componentIndex == 0) {
			return getX() + getMarginPixels();
		} else {
			Component previousComponent = getComponentAtIndex(componentIndex - 1);
			return previousComponent.getX() + previousComponent.getWidth()
					+ getSpacingPixels();
		}
	}

	@Override
	protected int calculateYPositionFor(Component component) {
		return getY() + getMarginPixels();
	}

	@Override
	public PositionalRectangle getAvailableSpaceFor(Component component) {
		BigDecimal width = BigDecimal.valueOf(this.getWidth());

		BigDecimal children = BigDecimal.valueOf(getNumberOfChildren());

		int availableWidth = width.divide(children, 0, RoundingMode.HALF_UP)
				.intValue();
		int availableHeight = getHeight();

		return new PositionalRectangle(getPositionFor(component),
				Rectangle.valueOf(availableWidth, availableHeight),
				Align.TOP_LEFT_POINT);
	}
}
