package fi.peterlehto.pgeng.ui.component;

/**
 * KeyboardFocusable is a special type of focusable field that accepts keyboard
 * input
 * 
 * @author Peter
 */
public interface KeyboardFocusable extends Focusable {

    void onKeyboardCharacter(char c);

    void onEnterKey();

    void onBackspaceKey();

    void onSpaceKey();
}
