package fi.peterlehto.pgeng.ui.component;


public interface Scrollable extends Focusable {

    void scroll(int scrollAmount);
}
