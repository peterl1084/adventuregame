package fi.peterlehto.pgeng.ui.component;


public abstract class AbstractOrderedComponentContainer extends
		AbstractComponentContainer {
	private final int spacingPixels;
	private final int marginPixels;

	public AbstractOrderedComponentContainer(int spacingPixels, int marginPixels) {
		this.spacingPixels = spacingPixels;
		this.marginPixels = marginPixels;
	}

	protected int getSpacingPixels() {
		return spacingPixels;
	}

	protected int getMarginPixels() {
		return marginPixels;
	}
}
