package fi.peterlehto.pgeng.ui.component;

import java.util.ArrayList;
import java.util.List;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.common.point.Rectangle;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.canvas.drawable.TextDrawable;

public class Button extends AbstractFocusableComponent {

	private String caption;

	private TextDrawable captionDrawable;

	private final List<ClickListener> clickListeners;

	public Button() {
		captionDrawable = new TextDrawable("", new PositionalRectangle(
				Point.valueOf(10, 10), Rectangle.valueOf(0, 0),
				Align.CENTER_POINT), RGBColor.WHITE);

		clickListeners = new ArrayList<>();
	}

	public Button(String caption) {
		this();

		setCaption(caption);
	}

	public void click() {
		fireClickListeners();
	}

	protected void fireClickListeners() {
		for (ClickListener clickListener : clickListeners) {
			clickListener.onClick(this);
		}
	}

	@Override
	public void onFocus() {
		super.onFocus();
		click();
	}

	@Override
	protected void renderContent() {
		super.renderContent();

		captionDrawable.draw();
	}

	@Override
	protected int calculateComponentWidth() {
		return captionDrawable.getWidth();
	}

	@Override
	protected int calculateComponentHeight() {
		return captionDrawable.getHeight();
	}

	@Override
	public String getCaption() {
		return caption;
	}

	@Override
	public void setCaption(String caption) {
		this.caption = caption;

		captionDrawable.setText(caption);
	}

	public void addClickListener(ClickListener clickListener) {
		this.clickListeners.add(clickListener);
	}

	public void removeClickListener(ClickListener clickListener) {
		this.clickListeners.remove(clickListener);
	}

	public interface ClickListener {
		public void onClick(Button button);
	}
}
