package fi.peterlehto.pgeng.ui.component;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.common.point.Rectangle;
import fi.peterlehto.pgeng.canvas.drawable.RGBAColor;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.canvas.drawable.RectangleDrawable;
import fi.peterlehto.pgeng.canvas.drawable.TextDrawable;

/**
 * TextConsole is a component that can be used to display message. TextConsole
 * supports different message removal policies such as faded removal or timeout.
 * 
 * @author Peter
 */
public class TextConsole extends AbstractFocusableComponent implements
		Scrollable {

	/**
	 * MessageRemovalPolicy defines how messages are removed.
	 */
	public enum MessageRemovalPolicy {
		/**
		 * Messages will not be removed.
		 */
		NONE,

		/**
		 * Message will be removed after timeout.
		 */
		TIMEOUT,

		/**
		 * Message will be faded out after the timeout.
		 */
		TIMEOUT_FADED;
	}

	private static final int DEFAULT_FADE_OUT_TIME_MS = 500;

	private long lastDrawTime;
	private final int messageTimeout;

	private final List<TimedText> texts;

	private final RGBColor textColor;

	private RectangleDrawable scrollBarBackground;
	private RectangleDrawable scrollBar;

	private int scrollPosition;

	private final MessageRemovalPolicy messageRemovalPolicy;

	/**
	 * Creates new TextConsole that preserves all the messages ever added to it.
	 * 
	 * @param color
	 */
	public TextConsole(RGBColor color) {
		this(color, MessageRemovalPolicy.NONE, 0);
	}

	/**
	 * Creates new TextConsole that operates with given message removal policy.
	 * 
	 * @param color
	 * @param messageRemovalPolicy
	 * @param messageTimeout
	 *            timeout in millisecond after which the message removal policy
	 *            will take affect on message.
	 */
	public TextConsole(RGBColor textColor,
			MessageRemovalPolicy messageRemovalPolicy, int messageTimeout) {
		this.texts = new LinkedList<>();

		this.textColor = textColor;
		this.messageRemovalPolicy = messageRemovalPolicy;
		this.messageTimeout = messageTimeout;
	}

	/**
	 * Shows given message in the console
	 * 
	 * @param message
	 */
	public void addMessage(String message) {
		TimedText messageObject = new TimedText(message, messageTimeout);
		texts.add(messageObject);
	}

	@Override
	public void renderContent() {
		super.renderContent();

		lastDrawTime = System.currentTimeMillis();

		Iterator<TimedText> textIterator = texts.iterator();

		scrollBarBackground = new RectangleDrawable(new PositionalRectangle(
				getWidth() - 20, 0, 20, getHeight(), Align.TOP_LEFT_POINT),
				getActiveBackgroundColor());
		scrollBar = new RectangleDrawable(new PositionalRectangle(
				getWidth() - 20, calculateScrollbarPosition(), 20,
				calculateScrollbarHeight(), Align.TOP_LEFT_POINT),
				getActiveBackgroundColor());

		scrollBarBackground.draw();
		scrollBar.draw();

		int yPosition = 0;

		GL11.glTranslatef(0, -scrollPosition, 0);
		while (textIterator.hasNext()) {
			TimedText text = textIterator.next();

			if (yPosition + text.getDrawable().getHeight() >= scrollPosition
					&& yPosition < (scrollPosition + getHeight())) {
				long timeToLive = calculateRemainingTimeToLive(text);
				int alpha = 255;

				switch (messageRemovalPolicy) {
				case NONE: {
					renderText(text, alpha, yPosition);
					break;
				}
				case TIMEOUT: {
					if (timeToLive <= 0) {
						textIterator.remove();
					} else {
						renderText(text, alpha, yPosition);
					}
					break;
				}
				case TIMEOUT_FADED: {
					if (hasFadeOutExpired(timeToLive)) {
						alpha = 0;
						textIterator.remove();
					} else {
						alpha = calculateFadeAlpha(timeToLive);
						renderText(text, alpha, yPosition);
					}
					break;
				}
				}
			}

			yPosition += text.getDrawable().getHeight();
		}

		GL11.glTranslatef(0, scrollPosition, 0);
	}

	private int calculateScrollbarPosition() {
		int height = getHeight();
		int contentHeight = Math.max(height, getContentHeight());

		float relation = (float) scrollPosition / (float) contentHeight;
		int scrollbarHeight = (int) (height * relation);
		return scrollbarHeight;
	}

	private int calculateScrollbarHeight() {
		int height = getHeight();
		int contentHeight = Math.max(height, getContentHeight());

		float relation = (float) height / (float) contentHeight;
		int scrollbarHeight = (int) (height * relation);
		return scrollbarHeight;
	}

	private int getContentHeight() {
		return calculateComponentHeight();
	}

	@Override
	public void scroll(int scrollAmount) {
		scrollPosition += scrollAmount;

		if (scrollPosition < 0) {
			scrollPosition = 0;
		}

		if (scrollPosition + getFixedHeight() >= getContentHeight()) {
			scrollPosition = Math.max(0,
					(getContentHeight() - getFixedHeight()));
		}

		System.out.println(scrollAmount);
	}

	@Override
	protected int calculateComponentWidth() {
		int width = 0;

		for (TimedText text : texts) {
			int drawableWidth = text.getDrawable().getWidth();
			if (width < drawableWidth) {
				width = drawableWidth;
			}
		}

		return width;
	}

	@Override
	protected int calculateComponentHeight() {
		int height = 0;

		for (TimedText text : texts) {
			height += text.getDrawable().getHeight();
		}

		return height;
	}

	private long calculateRemainingTimeToLive(TimedText text) {
		return (text.getCreationTime() + text.getTimeToLive()) - lastDrawTime;
	}

	private int calculateFadeAlpha(long timeToLive) {
		return (int) (255 * (float) (1 - (timeToLive * -1 / (double) DEFAULT_FADE_OUT_TIME_MS)));
	}

	private boolean hasFadeOutExpired(long timeToLive) {
		return timeToLive <= 0
				&& ((timeToLive * -1) > DEFAULT_FADE_OUT_TIME_MS);
	}

	/**
	 * Renders given text with given alpha to the yPosition
	 * 
	 * @param text
	 * @param alpha
	 * @param yPosition
	 */
	private void renderText(TimedText text, int alpha, int yPosition) {
		RGBAColor color = new RGBAColor(this.textColor, alpha);

		TextDrawable textDrawable = text.getDrawable();
		textDrawable.setCenterPoint(Point.valueOf(0, yPosition));
		textDrawable.setColor(color);

		textDrawable.draw();
	}

	private class TimedText {
		private final long creationTime;
		private final int timeToLive;

		private final TextDrawable drawable;

		public TimedText(String text, int timeToLive) {
			this.timeToLive = timeToLive;

			drawable = new TextDrawable(text, new PositionalRectangle(
					Point.valueOf(5, 5), Rectangle.valueOf(0, 0),
					Align.CENTER_POINT), RGBColor.WHITE);
			drawable.setMaxWidth(getWidth());

			this.creationTime = System.currentTimeMillis();
		}

		public long getCreationTime() {
			return creationTime;
		}

		public int getTimeToLive() {
			return timeToLive;
		}

		public TextDrawable getDrawable() {
			return drawable;
		}
	}
}
