package fi.peterlehto.pgeng.ui.component;

public interface HasProjections {

	void enableProjections();

	void disableProjections();
}
