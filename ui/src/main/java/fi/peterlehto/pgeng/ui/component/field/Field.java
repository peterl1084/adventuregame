package fi.peterlehto.pgeng.ui.component.field;

import fi.peterlehto.pgeng.ui.component.Component;
import fi.peterlehto.pgeng.ui.component.Focusable;
import fi.peterlehto.pgeng.ui.component.field.validation.InvalidValueException;
import fi.peterlehto.pgeng.ui.component.field.validation.Validatable;
import fi.peterlehto.pgeng.ui.component.field.validation.Validator;

/**
 * Field is a Component that can have value
 * 
 * @author Peter
 * 
 * @param <VALUE>
 *            type of the value this component can have
 * @param <VALIDATOR>
 *            type of the value validator this component can have
 */
public interface Field<VALUE, VALIDATOR extends Validator<? super VALUE>> extends Component,
        Validatable<VALUE, VALIDATOR>, Focusable {

    /**
     * @return current value of the field
     */
    VALUE getValue();

    /**
     * Sets the current value of the field. Given value will be evaluated with
     * all available field validators and is set only if value is valid.
     * 
     * @param value
     * @throws InvalidValueException
     *             if given value is invalid according to some validator
     */
    void setValue(VALUE value);

    /**
     * Adds value change listener which will be notified when field's value
     * changes
     * 
     * @param valueChangeListener
     */
    void addValueChangeListener(ValueChangeListener valueChangeListener);

    /**
     * Removes value change listener
     * 
     * @param valueChangeListener
     */
    void removeValueChangeListener(ValueChangeListener valueChangeListener);
}
