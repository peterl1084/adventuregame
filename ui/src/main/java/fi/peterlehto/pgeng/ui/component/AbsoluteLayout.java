package fi.peterlehto.pgeng.ui.component;

import java.util.HashMap;
import java.util.Map;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;

public class AbsoluteLayout extends AbstractComponentContainer {
	private final Map<Component, Point> componentPositions;

	public AbsoluteLayout() {
		componentPositions = new HashMap<>();
	}

	@Override
	public void addComponent(Component component) {
		throw new UnsupportedOperationException(
				"Use addComponent(Component, x, y) with AbsoluteLayout");
	}

	public void addComponent(Component component, int x, int y) {
		componentPositions.put(component, new Point(x, y));
		super.addComponent(component);
	}

	@Override
	protected int calculateXPositionFor(Component component) {
		return componentPositions.get(component).getX();
	}

	@Override
	protected int calculateYPositionFor(Component component) {
		return componentPositions.get(component).getY();
	}

	@Override
	protected int calculateComponentWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected int calculateComponentHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PositionalRectangle getAvailableSpaceFor(Component component) {
		return getAreaAsRectangle();
	}
}
