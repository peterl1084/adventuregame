package fi.peterlehto.pgeng.ui.component;

import java.util.Collections;
import java.util.Iterator;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.common.point.Rectangle;

public abstract class AbstractSingleComponentContainer extends
		AbstractComponent implements SingleComponentContainer {

	private Component child;

	@Override
	public PositionalRectangle getAvailableSpaceFor(Component component) {
		return new PositionalRectangle(Point.valueOf(getX(), getY()),
				Rectangle.valueOf(this.getWidth(), this.getHeight()),
				Align.TOP_LEFT_POINT);
	}

	@Override
	public void setContent(Component component) {
		this.child = component;
		component.attach(this);
	}

	@Override
	public Component getContent() {
		return child;
	}

	@Override
	protected void renderContent() {
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		child.render();
		GL11.glPopMatrix();
	}

	protected int getNumberOfChildren() {
		return child != null ? 1 : 0;
	}

	@Override
	public Iterator<Component> iterator() {
		return Collections.singleton(child).iterator();
	}

	@Override
	protected abstract int calculateComponentWidth();

	@Override
	protected abstract int calculateComponentHeight();
}
