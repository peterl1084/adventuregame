package fi.peterlehto.pgeng.ui.component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.StreamSupport;

import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.common.point.Rectangle;

public class VerticalLayout extends AbstractOrderedComponentContainer {

	public VerticalLayout() {
		this(0, 0);
	}

	public VerticalLayout(int spacingPixels, int marginPixels) {
		super(spacingPixels, marginPixels);
	}

	@Override
	protected int calculateComponentWidth() {
		int widestComponent = StreamSupport.stream(spliterator(), false)
				.mapToInt(c -> c.getWidth()).max().getAsInt();
		return widestComponent + (getMarginPixels() * 2);
	}

	@Override
	protected int calculateComponentHeight() {
		return StreamSupport.stream(spliterator(), false)
				.mapToInt(c -> c.getHeight() + getSpacingPixels()).sum()
				+ (getMarginPixels() * 2);
	}

	@Override
	protected int calculateXPositionFor(Component component) {
		return getX() + getMarginPixels();
	}

	@Override
	protected int calculateYPositionFor(Component component) {
		int componentIndex = indexOf(component);

		if (componentIndex == 0) {
			return getY() + getMarginPixels();
		} else {
			Component previousComponent = getComponentAtIndex(componentIndex - 1);
			return previousComponent.getY() + previousComponent.getHeight()
					+ getSpacingPixels();
		}
	}

	@Override
	public PositionalRectangle getAvailableSpaceFor(Component component) {
		BigDecimal height = BigDecimal.valueOf(this.getHeight());

		BigDecimal children = BigDecimal.valueOf(getNumberOfChildren());

		int availableWidth = getWidth();
		int availableHeight = height.divide(children, 0, RoundingMode.HALF_UP)
				.intValue();

		return new PositionalRectangle(getPositionFor(component),
				Rectangle.valueOf(availableWidth, availableHeight),
				Align.TOP_LEFT_POINT);
	}
}
