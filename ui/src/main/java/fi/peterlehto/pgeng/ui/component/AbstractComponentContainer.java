package fi.peterlehto.pgeng.ui.component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.opengl.GL11;

import fi.peterlehto.common.point.Point;

public abstract class AbstractComponentContainer extends AbstractComponent
		implements ComponentContainer {

	private final List<Component> children;

	public AbstractComponentContainer() {
		children = new ArrayList<>();
	}

	@Override
	public void addComponent(Component component) {
		children.add(component);
		component.attach(this);
	}

	@Override
	public void addComponents(Component... components) {
		for (Component component : components) {
			addComponent(component);
		}
	}

	@Override
	public void removeComponent(Component component) {
		children.remove(component);
	}

	protected Point getPositionFor(Component component) {
		return Point.valueOf(calculateXPositionFor(component),
				calculateYPositionFor(component));
	}

	/**
	 * @param component
	 * @return Index of given component or -1 if given component does not exist
	 *         in this container.
	 */
	protected int indexOf(Component component) {
		return children.indexOf(component);
	}

	protected Component getComponentAtIndex(int index) {
		return children.get(index);
	}

	@Override
	protected void renderContent() {
		forEach(c -> {
			GL11.glPushMatrix();
			GL11.glLoadIdentity();
			GL11.glTranslatef(c.getX(), c.getY(), 0);
			c.render();
			GL11.glPopMatrix();
		});
	}

	protected int getNumberOfChildren() {
		return children.size();
	}

	@Override
	public Iterator<Component> iterator() {
		return children.iterator();
	}

	@Override
	protected abstract int calculateComponentWidth();

	@Override
	protected abstract int calculateComponentHeight();

	protected abstract int calculateXPositionFor(Component component);

	protected abstract int calculateYPositionFor(Component component);
}
