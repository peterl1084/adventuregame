package fi.peterlehto.pgeng.ui.component;

public interface SingleComponentContainer extends HasComponents {

	void setContent(Component component);

	Component getContent();
}
