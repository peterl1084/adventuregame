package fi.peterlehto.pgeng.ui.component;

import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.pgeng.canvas.drawable.RGBAColor;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.canvas.drawable.RectangleDrawable;

public abstract class AbstractFocusableComponent extends AbstractComponent implements Focusable {
    private static final RGBColor backgroundColor = RGBAColor.valueOf(200, 200, 200, 64);
    private static final RGBColor focusedColor = RGBAColor.valueOf(200, 150, 150, 92);

    private RGBColor activeBackgroundColor = backgroundColor;

    protected RectangleDrawable background;

    protected AbstractFocusableComponent() {

    }

    @Override
    protected void renderContent() {
        background = new RectangleDrawable(
                new PositionalRectangle(0, 0, getWidth(), getHeight(), Align.TOP_LEFT_POINT), activeBackgroundColor);
        background.draw();
    }

    @Override
    public void onFocus() {
        System.out.println("Focused: " + this);
        activeBackgroundColor = focusedColor;
    }

    @Override
    public void onBlur() {
        activeBackgroundColor = (getBackgroundColor() == null ? backgroundColor : getBackgroundColor());
    }

    protected RGBColor getActiveBackgroundColor() {
        return activeBackgroundColor;
    }
}
