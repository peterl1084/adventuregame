package fi.peterlehto.pgeng.ui.component;


public interface Focusable extends Component {

    /**
     * Called when this component is focused
     */
    void onFocus();

    /**
     * Called when this component is defocused (blurred)
     */
    void onBlur();
}
