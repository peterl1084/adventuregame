package fi.peterlehto.pgeng.ui.component.field.validation;

public interface StringValidator extends Validator<String> {

    @Override
    boolean validate(String stringValue);
}
