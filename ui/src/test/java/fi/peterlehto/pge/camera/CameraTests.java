package fi.peterlehto.pge.camera;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.pgeng.camera.Camera;

public class CameraTests {

	@Test
	public void testCameraLocation_CameraNotMoved_LocationAccordingToViewAreaDimensions() {
		Camera camera = new Camera(1024, 768);

		Assert.assertEquals(Point.valueOf(0, 0), camera.getTopLeftCornerPoint());
		Assert.assertEquals(Point.valueOf(512, 384),
				camera.getCenterPoint());
		Assert.assertEquals(Point.valueOf(1024, 768),
				camera.getBottomRightCornerPoint());

		Assert.assertEquals(Point.valueOf(0, 0), camera.getViewArea()
				.getTopLeftCorner());
		Assert.assertEquals(Point.valueOf(512, 384), camera.getViewArea()
				.getCenterPoint());
		Assert.assertEquals(Point.valueOf(1024, 768), camera.getViewArea()
				.getBottomRightCorner());
	}

	@Test
	public void testCameraLocation_MoveCameraCenterTo1024x768_LocationAccordingToViewAreaDimensions() {
		Camera camera = new Camera(1024, 768);
		camera.setCenterPointTo(Point.valueOf(1024, 768));

		Assert.assertEquals(Point.valueOf(512, 384),
				camera.getTopLeftCornerPoint());
		Assert.assertEquals(Point.valueOf(1024, 768),
				camera.getCenterPoint());
		Assert.assertEquals(Point.valueOf(1536, 1152),
				camera.getBottomRightCornerPoint());

		Assert.assertEquals(Point.valueOf(512, 384), camera.getViewArea()
				.getTopLeftCorner());
		Assert.assertEquals(Point.valueOf(1024, 768), camera.getViewArea()
				.getCenterPoint());
		Assert.assertEquals(Point.valueOf(1536, 1152), camera.getViewArea()
				.getBottomRightCorner());
	}

	@Test
	public void testCameraLocation_CameraZoomedInDoubleButNotMoved_LocationAccordingToViewAreaDimensionsAndZoomFactor() {
		Camera camera = new Camera(1024, 768);
		camera.zoomTo(2.0f);

		Assert.assertEquals(Point.valueOf(256, 192),
				camera.getTopLeftCornerPoint());
		Assert.assertEquals(Point.valueOf(512, 384),
				camera.getCenterPoint());
		Assert.assertEquals(Point.valueOf(768, 576),
				camera.getBottomRightCornerPoint());
	}

	@Test
	public void testCameraLocation_CameraZoomedOutDoubleButNotMoved_LocationAccordingToViewAreaDimensionsAndZoomFactor() {
		Camera camera = new Camera(1024, 768);
		camera.zoomTo(0.5f);

		Assert.assertEquals(Point.valueOf(-512, -384),
				camera.getTopLeftCornerPoint());
		Assert.assertEquals(Point.valueOf(512, 384),
				camera.getCenterPoint());
		Assert.assertEquals(Point.valueOf(1536, 1152),
				camera.getBottomRightCornerPoint());
	}

	@Test
	public void testCameraLocation_CameraZoomedIn100Percent_LocationAccordingToViewAreaDimensionsAndZoomFactor() {
		Camera camera = new Camera(1024, 768);
		camera.zoomTo(0.5f);
		camera.setCenterPointTo(Point.valueOf(10000, 10000));

		Assert.assertEquals(Point.valueOf(8976, 9232),
				camera.getTopLeftCornerPoint());
		Assert.assertEquals(Point.valueOf(10000, 10000),
				camera.getCenterPoint());
		Assert.assertEquals(Point.valueOf(11024, 10768),
				camera.getBottomRightCornerPoint());
	}

}
