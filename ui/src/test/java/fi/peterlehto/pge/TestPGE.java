package fi.peterlehto.pge;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fi.peterlehto.pge.camera.CameraTests;

@RunWith(Suite.class)
@SuiteClasses(CameraTests.class)
public class TestPGE {

}
