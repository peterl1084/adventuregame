package fi.peterlehto.eventbus;

import static org.mockito.internal.verification.VerificationModeFactory.times;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestEventbus {

	@Captor
	private ArgumentCaptor<MyEvent> myEventCaptor;

	@Captor
	private ArgumentCaptor<MyOtherEvent> myOtherEventCaptor;

	@Captor
	private ArgumentCaptor<MySubclassEvent> mySubclassEventCaptor;

	@Mock
	private EventRecipient receiver;

	@Mock
	private EventSubRecipient subReceiver;

	@BeforeClass
	public static void initialize() {
		Eventbus.initialize("fi.peterlehto.eventbus");
	}

	@Before
	public void beforeTest() {

	}

	@After
	public void afterTest() {

	}

	@Test
	public void testEventSending_EventSuccessfullySent_EventSuccessfullyReceived() {
		Eventbus.subscribe(receiver);
		Eventbus.fire(new MyEvent());

		Mockito.verify(receiver, times(1)).onMyEvent(myEventCaptor.capture());
		Mockito.verify(receiver, times(0)).onMyOtherEvent(
				myOtherEventCaptor.capture());
		Eventbus.unsubscribe(receiver);
	}

	@Test
	public void testEventSending_OtherEventSuccessfullySent_OtherEventSuccessfullyReceived() {
		Eventbus.subscribe(receiver);
		Eventbus.fire(new MyOtherEvent());

		Mockito.verify(receiver, times(0)).onMyEvent(myEventCaptor.capture());
		Mockito.verify(receiver, times(1)).onMyOtherEvent(
				myOtherEventCaptor.capture());
		Eventbus.unsubscribe(receiver);
	}

	@Test
	public void testEventSent_RecipientMethodsInClassAndSuperClass_BothRecipientsCalled() {
		Eventbus.subscribe(subReceiver);
		Eventbus.fire(new MyOtherEvent());
		Eventbus.fire(new MySubclassEvent());

		Mockito.verify(subReceiver, times(0))
				.onMyEvent(myEventCaptor.capture());
		Mockito.verify(subReceiver, times(1)).onMyOtherEvent(
				myOtherEventCaptor.capture());
		Mockito.verify(subReceiver, times(1)).onSubClassEvent(
				mySubclassEventCaptor.capture());

		Eventbus.unsubscribe(subReceiver);
	}

	@Test
	public void testReceiverHasTwoMethodsForSameEvent_BothMethodsCalled() {
		Eventbus.subscribe(receiver);
		Eventbus.fire(new MyEvent());

		Mockito.verify(receiver, times(1)).onMyEvent(myEventCaptor.capture());
		Mockito.verify(receiver, times(1)).onMyEventSecond(
				myEventCaptor.capture());

		Eventbus.unsubscribe(receiver);
	}

	@Test
	public void testReceiverNotRegisteredToEventBus_NoEventReceived() {
		Eventbus.fire(new MyEvent());
		Mockito.verify(receiver, times(0)).onMyEvent(myEventCaptor.capture());
	}

	@Test
	public void testReceiverUnsubscribed_NoEventsReceivedAfterUnsubscribing() {
		Eventbus.subscribe(receiver);
		Eventbus.fire(new MyEvent());

		Mockito.verify(receiver, times(1)).onMyEvent(myEventCaptor.capture());

		Eventbus.unsubscribe(receiver);

		Eventbus.fire(new MyOtherEvent());

		Mockito.verify(receiver, times(0)).onMyOtherEvent(
				myOtherEventCaptor.capture());
	}
}
