package fi.peterlehto.eventbus;

public class EventSubRecipient extends EventRecipient {

	@EventBusListener
	protected void onSubClassEvent(MySubclassEvent event) {
		System.out.println("Received event " + event);
	}
}
