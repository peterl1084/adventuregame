package fi.peterlehto.eventbus;

public class EventRecipient {

	@EventBusListener
	protected void onMyEvent(MyEvent event) {
		System.out.println("Received event " + event);
	}

	@EventBusListener
	protected void onMyEventSecond(MyEvent secondEvent) {
		System.out.println("Received second event " + secondEvent);
	}

	@EventBusListener
	protected void onMyOtherEvent(MyOtherEvent event) {
		System.out.println("Received other event " + event);
	}
}
