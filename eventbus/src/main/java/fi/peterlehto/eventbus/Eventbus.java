package fi.peterlehto.eventbus;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;

public class Eventbus {
	private Reflections reflections;
	private Map<Class<? extends EventbusEvent>, Set<Method>> listenerMethods;
	private Set<Object> subscribedObjects;

	private static Eventbus instance;

	private Eventbus(String basePackage) {
		reflections = new Reflections(basePackage,
				new MethodAnnotationsScanner());
		subscribedObjects = new HashSet<>();
		listenerMethods = new HashMap<>();
		instance = this;

		initializeEventbus();
	}

	public static void initialize(String basePackage) {
		if (instance == null) {
			instance = new Eventbus(basePackage);
		} else {
			throw new IllegalStateException("Eventbus is already initialized");
		}
	}

	public static void fire(EventbusEvent event) {
		if (instance == null)
			throw new IllegalStateException("Eventbus not initialized");
		instance.fireEvent(event);
	}

	public static void subscribe(Object subscribee) {
		if (instance == null)
			throw new IllegalStateException("Eventbus not initialized");
		instance.subscribedObjects.add(subscribee);
	}

	public static void unsubscribe(Object subscribee) {
		if (instance == null)
			throw new IllegalStateException("Eventbus not initialized");
		instance.subscribedObjects.remove(subscribee);
	}

	protected void fireEvent(EventbusEvent event) {
		if (!listenerMethods.containsKey(event.getClass())) {
			return;
		}

		for (Method method : listenerMethods.get(event.getClass())) {
			for (Object subscribee : subscribedObjects) {
				if (hasMethod(subscribee.getClass(), method)) {
					try {
						method.invoke(subscribee, event);
					} catch (Exception e) {
						throw new RuntimeException("Failed to invoke method "
								+ method.getDeclaringClass().getCanonicalName()
								+ "." + method.getName() + " on "
								+ subscribee.getClass().getCanonicalName()
								+ " for parameter "
								+ event.getClass().getCanonicalName(), e);
					}
				}
			}
		}
	}

	protected boolean hasMethod(Class<? extends Object> subscribee,
			Method methodToCheck) {
		for (Method method : subscribee.getDeclaredMethods()) {
			if (method.equals(methodToCheck)) {
				getLogger().fine(
						subscribee.getCanonicalName() + " has method "
								+ methodToCheck.getName());
				return true;
			}
		}

		if (subscribee.getSuperclass() != null) {
			getLogger().fine(
					subscribee.getCanonicalName() + " has super class "
							+ subscribee.getSuperclass().getCanonicalName());
			return hasMethod(subscribee.getSuperclass(), methodToCheck);
		}

		getLogger().fine(
				subscribee.getCanonicalName() + " DOES NOT HAVE method "
						+ methodToCheck.getName());
		return false;
	}

	@SuppressWarnings("unchecked")
	private void initializeEventbus() {
		Set<Method> listenerMethods = reflections
				.getMethodsAnnotatedWith(EventBusListener.class);

		for (Method method : listenerMethods) {
			if (method.getParameterCount() != 1) {
				getLogger()
						.warning(
								"Skipping "
										+ method.getDeclaringClass()
												.getCanonicalName()
										+ "."
										+ method.getName()
										+ " as listener method because it does not have a single parameter");
				continue;
			}

			Parameter parameter = method.getParameters()[0];
			if (!EventbusEvent.class.isAssignableFrom(parameter.getType())) {
				getLogger()
						.warning(
								"Skipping "
										+ method.getDeclaringClass()
												.getCanonicalName()
										+ "."
										+ method.getName()
										+ " as the parameter is not assignable from EventbusEvent interface");
				continue;
			}

			if (!this.listenerMethods.containsKey(parameter.getType())) {
				this.listenerMethods.put(
						(Class<? extends EventbusEvent>) parameter.getType(),
						new HashSet<>());
			}

			getLogger().fine(
					"Adding listener method "
							+ method.getDeclaringClass().getCanonicalName()
							+ "." + method.getName() + " for event "
							+ parameter.getType().getSimpleName());
			this.listenerMethods.get(parameter.getType()).add(method);
		}
	}

	private static Logger getLogger() {
		return Logger.getLogger(Eventbus.class.getSimpleName());
	}
}
