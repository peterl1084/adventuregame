package fi.peterlehto.adventure.game.input;

import static org.mockito.internal.verification.VerificationModeFactory.times;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lwjgl.input.Keyboard;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import fi.peterlehto.eventbus.Eventbus;
import fi.peterlehto.model.game.event.DropCurrentWeaponEvent;
import fi.peterlehto.model.game.event.DropItemEvent;
import fi.peterlehto.model.game.event.MoveEvent;
import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.gameitem.character.GameCharacter;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Eventbus.class)
public class TestInputController {

	@Captor
	private ArgumentCaptor<MoveEvent> moveEventCaptor;

	@Captor
	private ArgumentCaptor<DropCurrentWeaponEvent> dropCurrentWeaponEventCaptor;

	@Captor
	private ArgumentCaptor<DropItemEvent> dropItemEventCaptor;

	@Test
	public void testOnKeyPressedUp_MoveEventNorthFired() {
		PowerMockito.mockStatic(Eventbus.class);

		GameCharacter character = new Character();
		InputController controller = new InputController(character);

		controller.onKeyPressed(Keyboard.KEY_UP);

		PowerMockito.verifyStatic(times(1));
		Eventbus.fire(moveEventCaptor.capture());

		Assert.assertEquals(MoveDirection.NORTH, moveEventCaptor.getValue()
				.getDirection());
	}

	@Test
	public void testOnKeyPressedDown_MoveEventSouthFired() {
		PowerMockito.mockStatic(Eventbus.class);

		GameCharacter character = new Character();
		InputController controller = new InputController(character);

		controller.onKeyPressed(Keyboard.KEY_DOWN);

		PowerMockito.verifyStatic(times(1));
		Eventbus.fire(moveEventCaptor.capture());

		Assert.assertEquals(MoveDirection.SOUTH, moveEventCaptor.getValue()
				.getDirection());
	}

	@Test
	public void testOnKeyPressedRight_MoveEventEastFired() {
		PowerMockito.mockStatic(Eventbus.class);

		GameCharacter character = new Character();
		InputController controller = new InputController(character);

		controller.onKeyPressed(Keyboard.KEY_RIGHT);

		PowerMockito.verifyStatic(times(1));
		Eventbus.fire(moveEventCaptor.capture());

		Assert.assertEquals(MoveDirection.EAST, moveEventCaptor.getValue()
				.getDirection());
	}

	@Test
	public void testOnKeyPressedLeft_MoveEventWestFired() {
		PowerMockito.mockStatic(Eventbus.class);

		GameCharacter character = new Character();
		InputController controller = new InputController(character);

		controller.onKeyPressed(Keyboard.KEY_LEFT);

		PowerMockito.verifyStatic(times(1));
		Eventbus.fire(moveEventCaptor.capture());

		Assert.assertEquals(MoveDirection.WEST, moveEventCaptor.getValue()
				.getDirection());
	}
}
