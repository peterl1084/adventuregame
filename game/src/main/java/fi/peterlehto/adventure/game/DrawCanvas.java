package fi.peterlehto.adventure.game;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;
import fi.peterlehto.model.gameitem.GameItem;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.map.GameItemMap;
import fi.peterlehto.pgeng.camera.Camera;
import fi.peterlehto.pgeng.canvas.AbstractCanvas;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.canvas.drawable.SpriteDrawable;
import fi.peterlehto.pgeng.display.PGESpritePool;

public class DrawCanvas extends AbstractCanvas {

	private GameItemMap map;

	public DrawCanvas(GameItemMap map) {
		this.map = map;
		Camera camera = new Camera();
		camera.setCenterPointTo(Point.valueOf(2000, 1500));
		camera.zoomTo(0.5f);
		apply(camera);
	}

	private void draw(GameItem i, Point position) {
		SpriteDrawable drawable = new SpriteDrawable(new PositionalRectangle(
				position.getX() * 64, position.getY() * 64, 64, 64,
				Align.TOP_LEFT_POINT), getFacing(i), PGESpritePool
				.getInstance().getSprite(i.itemId()), RGBColor.WHITE);

		drawable.draw();
	}

	private float getFacing(GameItem i) {
		if (i instanceof GameCharacter) {
			return ((GameCharacter) i).getFacing().getDirection();
		}

		return 0;
	}

	@Override
	protected void renderContent() {
		map.forEach(i -> draw(i, map.whereIs(i).get()));
	}

	@Override
	protected int calculateComponentWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected int calculateComponentHeight() {
		// TODO Auto-generated method stub
		return 0;
	}
}
