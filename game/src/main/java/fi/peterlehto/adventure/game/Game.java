package fi.peterlehto.adventure.game;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import fi.peterlehto.adventure.game.input.InputController;
import fi.peterlehto.common.point.Point;
import fi.peterlehto.eventbus.Eventbus;
import fi.peterlehto.model.game.AIPlayer;
import fi.peterlehto.model.game.HumanPlayer;
import fi.peterlehto.model.game.Player;
import fi.peterlehto.model.game.PlayerTurnManager;
import fi.peterlehto.model.game.world.GameWorld;
import fi.peterlehto.model.gameitem.character.Character;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.inventory.Water;
import fi.peterlehto.model.gameitem.inventory.Weapon;
import fi.peterlehto.model.gameitem.tile.Wall;
import fi.peterlehto.model.map.GameItemMap;
import fi.peterlehto.model.map.builder.MapBuilder;
import fi.peterlehto.pgeng.canvas.drawable.RGBColor;
import fi.peterlehto.pgeng.display.DefaultPGEDisplay;
import fi.peterlehto.pgeng.display.PGEDisplay;
import fi.peterlehto.pgeng.display.PGESpritePool;

public class Game {

	static {
		try {
			LogManager.getLogManager().readConfiguration(
					Thread.currentThread().getContextClassLoader()
							.getResourceAsStream("logging.properties"));
		} catch (Exception e) {
			throw new RuntimeException(
					"Failed to read logging configuration file");
		}
	}

	private GameWorld world;
	private GameItemMap map;
	private PlayerTurnManager turnManager;

	private PGEDisplay display;

	private Player humanPlayer;
	private Player AIPlayer;

	public Game() throws IOException {
		Eventbus.initialize("fi.peterlehto");
		humanPlayer = new HumanPlayer();
		AIPlayer = new AIPlayer();

		turnManager = new PlayerTurnManager(humanPlayer, AIPlayer);

		MapBuilder builder = new MapBuilder();
		map = builder.width(100).height(100)
				.fill(Point.ZERO, Point.valueOf(10, 0), Wall.class)
				.fill(Point.valueOf(0, 1), Point.valueOf(0, 10), Wall.class)
				.fill(Point.valueOf(5, 0), Point.valueOf(5, 5), Wall.class)
				.build();

		world = new GameWorld(map);
		Eventbus.subscribe(world);

		GameCharacter character = new Character();
		InputController controller = new InputController(character);
		controller.activate();

		world.addGameItem(character, Point.valueOf(6, 6));
		world.addGameItem(new Weapon("Sword"), Point.valueOf(7, 7));
		world.addGameItem(new Water(), Point.valueOf(6, 6));

		display = DefaultPGEDisplay.init(1024, 768, false);

		loadSprite("floor_wood");
		loadSprite("floor_tile");
		loadSprite("wall_bottom_intersection_up");
		loadSprite("wall_bottom_left");
		loadSprite("wall_bottom_right");
		loadSprite("wall_horizontal");
		loadSprite("wall_vertical");
		loadSprite("wall_left_intersection_right");
		loadSprite("wall_right_intersection_left");
		loadSprite("wall_top_intersection_down");
		loadSprite("wall_top_left");
		loadSprite("wall_top_right");
		loadSprite("character_body");
		loadSprite("gun1");
		loadSprite("gun2");
		loadSprite("drop");

		GameUI ui = new GameUI(map);
		display.setContent(ui);
		display.setBackgroundColor(RGBColor.BLUE);

		display.start();
	}

	private void loadSprite(String id) throws IOException {
		getLogger().fine("Loading sprinte with id " + id);
		PGESpritePool.getInstance().loadSprite(
				id,
				ImageIO.read(Thread.currentThread().getContextClassLoader()
						.getResource("graphics/tiles/" + id + ".png")));
	}

	public static void main(String... args) throws IOException {
		Game game = new Game();
	}

	private Logger getLogger() {
		return Logger.getLogger(Game.class.getSimpleName());
	}
}
