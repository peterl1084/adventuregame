package fi.peterlehto.adventure.game.input;

import org.lwjgl.input.Keyboard;

import fi.peterlehto.eventbus.Eventbus;
import fi.peterlehto.eventbus.EventbusEvent;
import fi.peterlehto.model.game.event.DropCurrentWeaponEvent;
import fi.peterlehto.model.game.event.MoveEvent;
import fi.peterlehto.model.game.event.PickupItemsEvent;
import fi.peterlehto.model.game.world.MoveDirection;
import fi.peterlehto.model.gameitem.character.GameCharacter;
import fi.peterlehto.model.gameitem.inventory.InventoryItem;
import fi.peterlehto.pgeng.input.PGEKeyboardManager;
import fi.peterlehto.pgeng.input.PGEKeyboardManager.KeyPressListener;

public class InputController implements KeyPressListener {

	private final GameCharacter characterToControl;

	public InputController(GameCharacter characterToControl) {
		this.characterToControl = characterToControl;
	}

	public void activate() {
		PGEKeyboardManager.getInstance().addKeyPressListener(this);
	}

	protected void fireEvent(EventbusEvent event) {
		Eventbus.fire(event);
	}

	@Override
	public void onKeyReleased(int keyCode) {

	}

	@Override
	public void onKeyPressed(int keyCode) {
		switch (keyCode) {
		case Keyboard.KEY_UP:
			fireEvent(new MoveEvent(characterToControl, MoveDirection.NORTH));
			break;
		case Keyboard.KEY_RIGHT:
			fireEvent(new MoveEvent(characterToControl, MoveDirection.EAST));
			break;
		case Keyboard.KEY_DOWN:
			fireEvent(new MoveEvent(characterToControl, MoveDirection.SOUTH));
			break;
		case Keyboard.KEY_LEFT:
			fireEvent(new MoveEvent(characterToControl, MoveDirection.WEST));
			break;
		case Keyboard.KEY_SPACE:
			fireEvent(new PickupItemsEvent(characterToControl,
					InventoryItem.class));
			break;
		case Keyboard.KEY_D:
			fireEvent(new DropCurrentWeaponEvent(characterToControl));
			break;
		}
	}
}
