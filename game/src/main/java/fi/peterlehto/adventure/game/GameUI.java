package fi.peterlehto.adventure.game;

import fi.peterlehto.eventbus.EventBusListener;
import fi.peterlehto.eventbus.Eventbus;
import fi.peterlehto.model.game.event.MoveEvent;
import fi.peterlehto.model.map.GameItemMap;
import fi.peterlehto.pgeng.UI;
import fi.peterlehto.pgeng.canvas.drawable.RGBAColor;
import fi.peterlehto.pgeng.ui.component.Button;
import fi.peterlehto.pgeng.ui.component.HorizontalLayout;
import fi.peterlehto.pgeng.ui.component.TextConsole;
import fi.peterlehto.pgeng.ui.component.TextConsole.MessageRemovalPolicy;
import fi.peterlehto.pgeng.ui.component.VerticalLayout;
import fi.peterlehto.pgeng.ui.component.field.TextField;

public class GameUI extends UI {
	private DrawCanvas canvas;
	private VerticalLayout rootLayout;
	private GameItemMap map;
	private TextConsole console;
	private TextField inputField;

	public GameUI(GameItemMap map) {
		this.map = map;
		Eventbus.subscribe(this);

		rootLayout = new VerticalLayout(10, 0);
		rootLayout.setSizeFull();
		rootLayout.setBackgroundColor(RGBAColor.valueOf(255, 255, 255, 128));

		canvas = new DrawCanvas(map);
		canvas.setSizeFull();

		console = new TextConsole(RGBAColor.GREEN,
				MessageRemovalPolicy.TIMEOUT_FADED, 2500);
		console.setSizeFull();

		HorizontalLayout layout = new HorizontalLayout(10, 0);
		layout.setBackgroundColor(RGBAColor.valueOf(255, 0, 0, 128));
		layout.setFullWidth();
		layout.setHeight(35);

		inputField = new TextField();
		inputField.setFullWidth();

		Button button = new Button("Enter");

		layout.addComponents(inputField, button);

		setContent(rootLayout);

		rootLayout.addComponents(canvas, console, layout);
	}

	@EventBusListener
	public void onMoveEvent(MoveEvent event) {
		console.addMessage("Character moved to "
				+ map.whereIs(event.getCharacter()).get());
	}
}
