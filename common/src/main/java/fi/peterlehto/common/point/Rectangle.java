package fi.peterlehto.common.point;


public class Rectangle {
	private final int width;
	private final int height;

	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public Rectangle(Rectangle rectangle) {
		this(rectangle.width, rectangle.height);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean containsPoint(Point point) {
		return point.getX() >= 0 && point.getX() <= width && point.getY() >= 0
				&& point.getY() <= height;
	}

	public static Rectangle valueOf(int width, int height) {
		return new Rectangle(width, height);
	}
}
