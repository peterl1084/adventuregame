package fi.peterlehto.common.point;

public class Vector {

    private final float x;
    private final float y;

    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getMagnitude() {
        return (float) Math.sqrt(x * x + y * y);
    }

    public float getDegreeAngle() {
        float degrees = (float) Math.toDegrees(Math.atan2(y, x));
        return degrees;
    }

    public static Vector asScaled(Vector vector, float scale) {
        float x = vector.x * scale;
        float y = vector.y * scale;

        return new Vector(x, y);
    }

    public static Vector asNegatedVector(Vector vector) {
        float x = vector.x * -1;
        float y = vector.y * -1;

        return new Vector(x, y);
    }

    public static Vector asVector(Vector... vectors) {
        float x = 0;
        float y = 0;

        for (Vector vector : vectors) {
            x += vector.x;
            y += vector.y;
        }

        return new Vector(x, y);
    }

    public static Vector asBetweenPoints(Point a, Point b) {
        float deltaX = b.getX() - a.getX();
        float deltaY = b.getY() - a.getY();

        return Vector.asVector((float) Math.toDegrees(Math.atan2(deltaY, deltaX)));
    }

    public static Vector asUnitVector(Vector vector) {
        double magnitude = vector.getMagnitude();

        float unitX = (float) (vector.x / magnitude);
        float unitY = (float) (vector.y / magnitude);

        return new Vector(unitX, unitY);
    }

    public static Vector asVector(float degreeAngle) {
        float x = (float) Math.cos(Math.toRadians(degreeAngle));
        float y = (float) Math.sin(Math.toRadians(degreeAngle));

        return new Vector(x, y);
    }

    public static Vector delta(Vector vector, float degrees, float min, float max) {
        float current = vector.getDegreeAngle();
        float target = current + degrees;

        if (target > max) {
            target = max;
        }

        if (target < min) {
            target = min;
        }

        return Vector.asVector(target);
    }

    @Override
    public String toString() {
        return x + "\t\t" + y + " :\t\t" + getDegreeAngle() + " deg\t\t" + getMagnitude();
    }
}
