package fi.peterlehto.common.point;

import java.util.Objects;

/**
 * 2d point
 */
public class Point {
	public static final Point ZERO = new Point(0, 0);

	private final int x;
	private final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point(Point point) {
		this(point.x, point.y);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public static Point valueOf(int x, int y) {
		return new Point(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj instanceof Point) {
			return this.x == ((Point) obj).x && this.y == ((Point) obj).y;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public String toString() {
		return x + ":" + y;
	}
}
