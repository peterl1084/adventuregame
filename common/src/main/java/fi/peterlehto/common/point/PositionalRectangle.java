package fi.peterlehto.common.point;


/**
 * PositionalRectangle is a rectangle with defined center point in space.
 * 
 * @author Peter
 */
public class PositionalRectangle extends Rectangle {

	public enum Align {
		TOP_LEFT_POINT,
		CENTER_POINT;
	}

	private final Point centerPoint;

	private final Align sizing;

	public PositionalRectangle(int pointX, int pointY, int width, int height,
			Align sizing) {
		this(Point.valueOf(pointX, pointY), width, height, sizing);
	}

	public PositionalRectangle(Point point, int width, int height, Align sizing) {
		this(point, Rectangle.valueOf(width, height), sizing);
	}

	public PositionalRectangle(Point point, Rectangle rectangle, Align sizing) {
		super(rectangle);

		if (Align.TOP_LEFT_POINT.equals(sizing)) {
			this.centerPoint = Point.valueOf(
					point.getX() + Math.round(rectangle.getWidth() / 2f),
					point.getY() + Math.round(rectangle.getHeight() / 2f));
		} else {
			this.centerPoint = new Point(point);
		}

		this.sizing = sizing;
	}

	public PositionalRectangle(PositionalRectangle other) {
		super(Rectangle.valueOf(other.getWidth(), other.getHeight()));

		this.centerPoint = other.getCenterPoint();
		this.sizing = other.sizing;
	}

	public Point getTopLeftCorner() {
		return Point.valueOf(
				(centerPoint.getX() - Math.round(getWidth() / 2f)),
				(centerPoint.getY() - Math.round(getHeight() / 2f)));
	}

	public Point getTopRightCorner() {
		return Point.valueOf(
				(centerPoint.getX() + Math.round(getWidth() / 2f)),
				(centerPoint.getY() - Math.round(getHeight() / 2f)));
	}

	public Point getBottomLeftCorner() {
		return Point.valueOf(centerPoint.getX() - Math.round(getWidth() / 2f),
				centerPoint.getY() + Math.round(getHeight() / 2f));
	}

	public Point getBottomRightCorner() {
		return Point.valueOf(
				(centerPoint.getX() + Math.round(getWidth() / 2f)),
				(centerPoint.getY() + Math.round(getHeight() / 2f)));
	}

	public Point getCenterPoint() {
		return new Point(centerPoint);
	}

	@Override
	public boolean containsPoint(Point point) {
		boolean a1 = this.getTopLeftCorner().getX() <= point.getX();
		boolean a2 = this.getTopRightCorner().getX() >= point.getX();
		boolean a3 = this.getTopLeftCorner().getY() <= point.getY();
		boolean a4 = this.getBottomLeftCorner().getY() >= point.getY();

		return a1 && a2 && a3 && a4;
	}

	public boolean isOverlapping(PositionalRectangle other) {
		boolean a1 = this.getTopLeftCorner().getX() < other.getTopRightCorner()
				.getX();
		boolean a2 = this.getTopRightCorner().getX() > other.getTopLeftCorner()
				.getX();
		boolean a3 = this.getTopLeftCorner().getY() < other
				.getBottomLeftCorner().getY();
		boolean a4 = this.getBottomLeftCorner().getY() > other
				.getTopLeftCorner().getY();

		return a1 && a2 && a3 && a4;
	}

	protected Align getSizing() {
		return sizing;
	}

	@Override
	public String toString() {
		return "x: " + centerPoint.getX() + ", y: " + centerPoint.getY()
				+ " - " + getWidth() + ", " + getHeight();
	}

	public Align getAlign() {
		return sizing;
	}

	public PositionalRectangle offSet(Point offset) {
		if (Align.TOP_LEFT_POINT.equals(getAlign())) {
			return new PositionalRectangle(getTopLeftCorner().getX()
					+ offset.getX(), getTopLeftCorner().getY() + offset.getY(),
					getWidth(), getHeight(), Align.TOP_LEFT_POINT);
		} else {
			return new PositionalRectangle(getCenterPoint().getX()
					+ offset.getX(), getCenterPoint().getY() + offset.getY(),
					getWidth(), getHeight(), Align.CENTER_POINT);
		}
	}

	public PositionalRectangle scale(float factor) {
		Rectangle rectangle = Rectangle.valueOf(
				Math.round(getWidth() * factor),
				Math.round(getHeight() * factor));

		if (Align.TOP_LEFT_POINT.equals(getAlign())) {
			return new PositionalRectangle(getTopLeftCorner(), rectangle,
					Align.TOP_LEFT_POINT);
		}

		return new PositionalRectangle(getCenterPoint(), rectangle,
				Align.CENTER_POINT);
	}
}
