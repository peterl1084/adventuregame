package fi.peterlehto.common.point;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;

public class PointTests {

	@Test
	public void testPointEquality_SamePoints_PointsEqual() {
		Point a = new Point(1, 1);
		Point b = new Point(1, 1);

		Assert.assertEquals(a, b);
		Assert.assertEquals(b, a);
	}

	@Test
	public void testPointInequality_DifferentPoints_PointsNotEqual() {
		Point a = new Point(1, 1);
		Point b = new Point(2, 2);

		Assert.assertFalse(a.equals(b));
		Assert.assertFalse(b.equals(a));
	}

	@Test
	public void testSamePointsHaveSameHashCode() {
		Point a = new Point(1, 1);
		Point b = new Point(1, 1);

		Assert.assertEquals(a.hashCode(), b.hashCode());
	}

	@Test
	public void testDifferentPointsHaveDifferentHashCode() {
		Point a = new Point(1, 1);
		Point b = new Point(2, 2);

		Assert.assertFalse(a.hashCode() == b.hashCode());
	}

	@Test
	public void testPointHashing_PointInHashSet_PointFound() {
		Set<Point> pointSet = new HashSet<>();

		for (int i = 0; i < 100; i++) {
			pointSet.add(new Point(i, 0));
		}

		for (int i = 0; i < 100; i++) {
			Assert.assertTrue(pointSet.contains(new Point(i, 0)));
		}

		for (int i = 0; i < 100; i++) {
			int value = i; // effectively final
			Assert.assertEquals(new Point(i, 0),
					pointSet.stream().filter(p -> p.getX() == value)
							.findFirst().get());
		}

		Assert.assertEquals(100, pointSet.size());
		Assert.assertFalse(pointSet.add(new Point(50, 0)));
		Assert.assertEquals(100, pointSet.size());
	}
}
