package fi.peterlehto.common.point;

import org.junit.Assert;
import org.junit.Test;

import fi.peterlehto.common.point.Point;
import fi.peterlehto.common.point.PositionalRectangle;
import fi.peterlehto.common.point.PositionalRectangle.Align;

public class PositionalRectangleTests {

	@Test
	public void testRectanglePointsOnCenterPointAlign() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 100, 100,
				Align.CENTER_POINT);

		// Center
		Assert.assertEquals(0, a.getCenterPoint().getX());
		Assert.assertEquals(0, a.getCenterPoint().getY());

		// Top left
		Assert.assertEquals(-50, a.getTopLeftCorner().getX());
		Assert.assertEquals(-50, a.getTopLeftCorner().getY());

		// Top right
		Assert.assertEquals(50, a.getTopRightCorner().getX());
		Assert.assertEquals(-50, a.getTopRightCorner().getY());

		// Bottom right
		Assert.assertEquals(50, a.getBottomRightCorner().getX());
		Assert.assertEquals(50, a.getBottomRightCorner().getY());

		// Bottom left
		Assert.assertEquals(-50, a.getBottomLeftCorner().getX());
		Assert.assertEquals(50, a.getBottomLeftCorner().getY());

		PositionalRectangle b = new PositionalRectangle(50, 50, 100, 100,
				Align.CENTER_POINT);

		// Center
		Assert.assertEquals(50, b.getCenterPoint().getX());
		Assert.assertEquals(50, b.getCenterPoint().getY());

		// Top left
		Assert.assertEquals(0, b.getTopLeftCorner().getX());
		Assert.assertEquals(0, b.getTopLeftCorner().getY());

		// Top right
		Assert.assertEquals(100, b.getTopRightCorner().getX());
		Assert.assertEquals(0, b.getTopRightCorner().getY());

		// Bottom right
		Assert.assertEquals(100, b.getBottomRightCorner().getX());
		Assert.assertEquals(100, b.getBottomRightCorner().getY());

		// Bottom left
		Assert.assertEquals(0, b.getBottomLeftCorner().getX());
		Assert.assertEquals(100, b.getBottomLeftCorner().getY());
	}

	@Test
	public void testRectanglePointsOnTopLeftPointAlign() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 100, 100,
				Align.TOP_LEFT_POINT);

		// Center
		Assert.assertEquals(50, a.getCenterPoint().getX());
		Assert.assertEquals(50, a.getCenterPoint().getY());

		// Top left
		Assert.assertEquals(0, a.getTopLeftCorner().getX());
		Assert.assertEquals(0, a.getTopLeftCorner().getY());

		// Top right
		Assert.assertEquals(100, a.getTopRightCorner().getX());
		Assert.assertEquals(0, a.getTopRightCorner().getY());

		// Bottom right
		Assert.assertEquals(100, a.getBottomRightCorner().getX());
		Assert.assertEquals(100, a.getBottomRightCorner().getY());

		// Bottom left
		Assert.assertEquals(0, a.getBottomLeftCorner().getX());
		Assert.assertEquals(100, a.getBottomLeftCorner().getY());

		PositionalRectangle b = new PositionalRectangle(50, 50, 100, 100,
				Align.TOP_LEFT_POINT);

		// Center
		Assert.assertEquals(100, b.getCenterPoint().getX());
		Assert.assertEquals(100, b.getCenterPoint().getY());

		// Top left
		Assert.assertEquals(50, b.getTopLeftCorner().getX());
		Assert.assertEquals(50, b.getTopLeftCorner().getY());

		// Top right
		Assert.assertEquals(150, b.getTopRightCorner().getX());
		Assert.assertEquals(50, b.getTopRightCorner().getY());

		// Bottom right
		Assert.assertEquals(150, b.getBottomRightCorner().getX());
		Assert.assertEquals(150, b.getBottomRightCorner().getY());

		// Bottom left
		Assert.assertEquals(50, b.getBottomLeftCorner().getX());
		Assert.assertEquals(150, b.getBottomLeftCorner().getY());
	}

	@Test
	public void testRectangeAOverlapsRectangleBTopLeftAlignedTopLeftCorner() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 100, 100,
				Align.TOP_LEFT_POINT);
		PositionalRectangle b = new PositionalRectangle(0, 0, 50, 50,
				Align.TOP_LEFT_POINT);

		Assert.assertTrue(a.isOverlapping(b));
		Assert.assertTrue(b.isOverlapping(a));
	}

	@Test
	public void testRectangeAOverlapsRectangleBTopLeftAlignedTopRightCorner() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 100, 100,
				Align.TOP_LEFT_POINT);
		PositionalRectangle b = new PositionalRectangle(50, 0, 50, 50,
				Align.TOP_LEFT_POINT);

		Assert.assertEquals(50, a.getCenterPoint().getX());
		Assert.assertEquals(50, a.getCenterPoint().getY());

		Assert.assertEquals(75, b.getCenterPoint().getX());
		Assert.assertEquals(25, b.getCenterPoint().getY());

		Assert.assertTrue(a.isOverlapping(b));
		Assert.assertTrue(b.isOverlapping(a));
	}

	@Test
	public void testRectangeAOverlapsRectangleBTopLeftAlignedBottomRightCorner() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 100, 100,
				Align.TOP_LEFT_POINT);
		PositionalRectangle b = new PositionalRectangle(50, 50, 50, 50,
				Align.TOP_LEFT_POINT);

		Assert.assertTrue(a.isOverlapping(b));
		Assert.assertTrue(b.isOverlapping(a));
	}

	@Test
	public void testRectangeAOverlapsRectangleBTopLeftAlignedBottomLeftCorner() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 100, 100,
				Align.TOP_LEFT_POINT);
		PositionalRectangle b = new PositionalRectangle(0, 50, 50, 50,
				Align.TOP_LEFT_POINT);

		Assert.assertTrue(a.isOverlapping(b));
		Assert.assertTrue(b.isOverlapping(a));
	}

	@Test
	public void testTopLeftAlignedRectangleOverlapsMultipleCenterAlignedSmallerRectangles() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 1024, 768,
				Align.TOP_LEFT_POINT);

		PositionalRectangle b = new PositionalRectangle(100, 100, 25, 25,
				Align.CENTER_POINT);
		PositionalRectangle c = new PositionalRectangle(750, 100, 25, 25,
				Align.CENTER_POINT);
		PositionalRectangle d = new PositionalRectangle(700, 600, 25, 25,
				Align.CENTER_POINT);
		PositionalRectangle e = new PositionalRectangle(100, 600, 25, 25,
				Align.CENTER_POINT);

		Assert.assertTrue(a.isOverlapping(b));
		Assert.assertTrue(b.isOverlapping(a));

		Assert.assertTrue(a.isOverlapping(c));
		Assert.assertTrue(c.isOverlapping(a));

		Assert.assertTrue(a.isOverlapping(d));
		Assert.assertTrue(d.isOverlapping(a));

		Assert.assertTrue(a.isOverlapping(e));
		Assert.assertTrue(e.isOverlapping(a));
	}

	@Test
	public void testTopLeftAlignedRectangleDoesNotOverlapMultipleCenterAlignedSmallerRectangles() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 1024, 768,
				Align.TOP_LEFT_POINT);

		PositionalRectangle b = new PositionalRectangle(-100, -100, 25, 25,
				Align.CENTER_POINT);
		PositionalRectangle c = new PositionalRectangle(1200, 100, 25, 25,
				Align.CENTER_POINT);
		PositionalRectangle d = new PositionalRectangle(1200, 600, 25, 25,
				Align.CENTER_POINT);
		PositionalRectangle e = new PositionalRectangle(100, 900, 25, 25,
				Align.CENTER_POINT);

		Assert.assertFalse(a.isOverlapping(b));
		Assert.assertFalse(b.isOverlapping(a));

		Assert.assertFalse(a.isOverlapping(c));
		Assert.assertFalse(c.isOverlapping(a));

		Assert.assertFalse(a.isOverlapping(d));
		Assert.assertFalse(d.isOverlapping(a));

		Assert.assertFalse(a.isOverlapping(e));
		Assert.assertFalse(e.isOverlapping(a));
	}

	@Test
	public void rectangleContainsPoint() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 1024, 768,
				Align.TOP_LEFT_POINT);

		Assert.assertTrue(a.containsPoint(Point.valueOf(0, 0)));
		Assert.assertTrue(a.containsPoint(Point.valueOf(1024, 0)));
		Assert.assertTrue(a.containsPoint(Point.valueOf(1024, 768)));
		Assert.assertTrue(a.containsPoint(Point.valueOf(0, 768)));

		Assert.assertTrue(a.containsPoint(Point.valueOf(500, 500)));
	}

	@Test
	public void rectangleDoesNotContainPoint() {
		PositionalRectangle a = new PositionalRectangle(0, 0, 1024, 768,
				Align.TOP_LEFT_POINT);

		Assert.assertFalse(a.containsPoint(Point.valueOf(-1, -1)));
		Assert.assertFalse(a.containsPoint(Point.valueOf(1025, -1)));
		Assert.assertFalse(a.containsPoint(Point.valueOf(1025, 769)));
		Assert.assertFalse(a.containsPoint(Point.valueOf(-1, 769)));
	}

	@Test
	public void testPositionalRectangleCopy() {
		PositionalRectangle a = new PositionalRectangle(50, 50, 100, 100,
				Align.TOP_LEFT_POINT);
		PositionalRectangle b = new PositionalRectangle(a);

		Assert.assertEquals(a.getCenterPoint().getX(), b.getCenterPoint()
				.getX());
		Assert.assertEquals(a.getCenterPoint().getY(), b.getCenterPoint()
				.getY());

		Assert.assertEquals(a.getTopLeftCorner().getX(), b.getTopLeftCorner()
				.getX());
		Assert.assertEquals(a.getTopLeftCorner().getY(), b.getTopLeftCorner()
				.getY());

		Assert.assertEquals(a.getTopRightCorner().getX(), b.getTopRightCorner()
				.getX());
		Assert.assertEquals(a.getTopRightCorner().getY(), b.getTopRightCorner()
				.getY());

		Assert.assertEquals(a.getBottomLeftCorner().getX(), b
				.getBottomLeftCorner().getX());
		Assert.assertEquals(a.getBottomLeftCorner().getY(), b
				.getBottomLeftCorner().getY());

		Assert.assertEquals(a.getBottomRightCorner().getX(), b
				.getBottomRightCorner().getX());
		Assert.assertEquals(a.getBottomRightCorner().getY(), b
				.getBottomRightCorner().getY());
	}
}
