package fi.peterlehto.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fi.peterlehto.common.point.PointTests;
import fi.peterlehto.common.point.PositionalRectangleTests;

@RunWith(Suite.class)
@SuiteClasses({ PointTests.class, PositionalRectangleTests.class })
public class TestCommon {

}
